@extends('Front::layout')

@section('content')
    <div class="">
        <div class="fof">
            <div class="container  error-inner wow flipInX">
                <h1>404, Page not found.</h1>
                <p class="text-center">The Page you are looking for doesn't exist or an other error occurred.</p>
                <a class="btn btn-info" href="{{ Request::root() }}">GO HOMEPAGE</a>
            </div>
        </div>
    </div>
@stop