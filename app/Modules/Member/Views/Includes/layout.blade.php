@include('App::header')
@include('Member::sidebar')
@include('Member::navbar')
<div class="right_col" role="main">
  <div class="">
    @yield('content')
  </div>
</div>
@include('App::footer')