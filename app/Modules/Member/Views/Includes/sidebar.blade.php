<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ Request::root() }}" class="site_title"><i class="fa fa-leaf"></i> <span>Member Panel</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="{{ URL::asset(Auth::user()->image) }}" alt="" class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{ Auth::user()->member_id }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->
    <br />
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Dashboard</h3>
        <ul class="nav side-menu">
          <li>
            <a><i class="fa fa-sitemap"></i> Pass-up <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ Request::root() }}/member/pass-up/list">List</a></li>
              <li><a href="{{ Request::root() }}/member/pass-up/chart">Chart</a></li>
            </ul>
          </li>
          <li><a href="{{ Request::root() }}/member/earnings"><i class="fa fa-money"></i>Earnings</a></li>
          <li><a href="{{ Request::root() }}/member/purchase-orders"><i class="fa fa-shopping-cart"></i>Purchase Orders</a></li>
        </ul>
        </ul>
      </div>
    </div>
  </div>
</div>