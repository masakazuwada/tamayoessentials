@extends('Member::layout')

@section('styles')
  <link rel="stylesheet" href="{{ URL::asset('assets/member/css/chart.css') }}">
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3>Unilevel Genealogy</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>
            <span id="back"></span> Passup Chart
          </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="body-tree">
            <div class="scoll-tree">
              <div id="chart"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/member/js/chart.js') }}"></script>
@stop