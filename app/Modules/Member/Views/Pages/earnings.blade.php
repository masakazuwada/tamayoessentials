@extends('Member::layout')

@section('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/responsive.bootstrap.min.css') }}">
@stop

@section('content')
<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Summary</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="summary">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="tile-stats text-center">
              <div class="count">₱{{ Earning::getTotalEarned(['field' => 'amount_balance', 'status = 1 AND amount_balance > 0']) }}</div>
              <h3 class="green">Amount Earned</h3>
              <p>Total Amount Earned: ₱{{ Earning::getTotalEarned(['field' => 'amount']) }}</p>
              <hr>
              <a href="javascript:void(0)" class="btn btn-success" data-bind="earnings_payout" data-method="payout" data-type="1" data-payout_available="{{ Earning::getTotalEarned(['field' => 'amount_balance', 'status = 1 AND amount_balance > 0']) }}">Request Payout</a>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="tile-stats text-center">
              <div class="count">{{ Earning::getTotalEarned(['field' => 'points_balance', 'status = 1 AND points_balance > 0']) }}</div>
              <h3 class="orange">Points Earned</h3>
              <p>Total Points Earned: {{ Earning::getTotalEarned(['field' => 'points']) }}</p>
              <hr>
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div>
                    <?php $laptopPercentage = (Earning::getTotalEarned(['field' => 'points']) / Setting::getValue('laptop_total_points')) * 100; ?>
                    <label for=""><i class="fa fa-laptop"></i> Laptop</label> <span>= {{ App::money(Setting::getValue('laptop_total_points')) }} points</span>
                    <div class="progress progress-striped" style="height: 10px;">
                      <div class="progress-bar progress-bar-info" data-transitiongoal="{{ $laptopPercentage }}" style="width: {{ $laptopPercentage }}%;" aria-valuenow="{{ $laptopPercentage }}"></div>
                    </div>
                  </div>
                  <div>
                    <?php $carPercentage = (Earning::getTotalEarned(['field' => 'points']) / Setting::getValue('car_total_points')) * 100; ?>
                    <label for=""><i class="fa fa-car"></i> Car</label> <span>= {{ App::money(Setting::getValue('car_total_points')) }} points</span>
                    <div class="progress progress-striped" style="height: 10px;">
                      <div class="progress-bar progress-bar-danger" data-transitiongoal="{{ $carPercentage }}" style="width: {{ $carPercentage }}%;" aria-valuenow="{{ $carPercentage }}"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Transactions</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="earnings-tab" role="tab" data-toggle="tab" aria-expanded="true">Earnings</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="payouts-tab" data-toggle="tab" aria-expanded="false">Payouts</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="earnings-tab">
              <div class="x_content">
                <table class="table table-bordered table-hover" id="earnings" width="100%">
                  <thead>
                    <tr>
                      <th class="text-center">Date</th>
                      <th class="text-center">Invited By</th>
                      <th class="text-center">Amount</th>
                      <th class="text-center">Points Earned </th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="payouts-tab">
              <table class="table table-bordered table-hover" id="payouts" width="100%">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Remarks</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
<script src="{{ URL::asset('assets/plugins/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/js/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/member/js/earnings.js') }}"></script>
@stop