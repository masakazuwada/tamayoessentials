@extends('Member::layout')

@section('styles')
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/responsive.bootstrap.min.css') }}">
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3>Purchase Orders</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>List</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-bordered table-hover" id="purchase_orders" width="100%">
            <thead>
              <th>Date</th>
              <th>Products</th>
              <th>Customer</th>
              <th>Total</th>
              <th>Details</th>
              <th>Action</th>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/member/js/purchase_orders.js') }}"></script>
@stop