<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Payouts</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <form method="POST" id="form">
          {!! csrf_field() !!}
          <input type="hidden" name="type">
          <div class="form-group">
            <label for=""><span id="type_label"></span></label>
            <input type="number" class="form-control" name="amount" min="1" placeholder="">
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-success">Request Payout</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
