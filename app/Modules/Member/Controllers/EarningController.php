<?php 
namespace App\Modules\Member\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Html;
use App\Modules\App\Models\App;
use App\Modules\Member\Models\Earning;

class EarningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewList()
    {
        return view('Member::earnings');
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Earning::get();
        }
        return json_encode(['data' => $data]);
    }

    public function checkIfHasBalance(Request $request)
    {
        if ($request->ajax()) {
            if (Earning::getTotalEarned(['field' => $request->field]) >= $request->amount) {
                return 'true';
            }
        }
        return 'false';
    }
}
