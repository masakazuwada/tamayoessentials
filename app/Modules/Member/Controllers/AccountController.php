<?php
namespace App\Modules\Member\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Member\Models\Account;
use App\Modules\Member\Models\Member;
use App\Modules\App\Models\App;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function account()
    {
        $account = Member::get(auth()->user()->member_id);
        return view('Member::account', array('account' => $account));
    }
    
    public function updateDetails(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
             if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/admin/images/users/',
                    'dir' => '/assets/admin/images/users/'));
            }
            if (Account::updateDetails($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function updatePassword(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if ($request->ajax()) {
            if (Account::updatePassword($request->all())) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}