<?php 
namespace App\Modules\Member\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Html;
use App\Modules\Member\Models\Member;
use App\Modules\Member\Models\Earning;
use App\Modules\Admin\Models\User;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\App;
use App\Mail\EmailVerification;
use App\User AS AppUser;
use Mail;
use Session;
use Auth;
use Redirect;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'register', 'verify', 'getForDropdown', 'sendEmailVerification']]);
    }

    public function login(Request $request)
    {
        if (Auth::check() || Auth::viaRemember()) {
            return redirect::to('/member/earnings');
        }
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'email' => 'required',
                'password' => 'required',
                ]);

            $userdata = array(
                'password' => $request->password,
                'verified' => 1,
                'status' => 1
                );

            if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $userdata['email'] = $request->email;
            } else {
                $userdata['member_id'] = $request->email;
            }

            if (Auth::validate($userdata)) {
                if (Auth::attempt($userdata, $request->remember)) {
                    Person::setSession(Auth::user()->person_id);
                    Session::put('guard', 'web');
                    return redirect()->intended('member/earnings');
                }
            }  else {
                Session::flash('error', 'Incorrect Email or Password'); 
            }
            return redirect::to('/member');
        }
        return view('Member::login');
    }

    public function viewList()
    {
        $data = [
        'data_bind' => 'member_add_edit',
        ];
        return view('Member::members', $data);
    }

    public function viewChart()
    {
        return view('Member::chart');
    }

    public function createChartByMember(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $levelOne = (isset($request->member_id) ? $request->member_id : auth()->user()->member_id);
            $obj = new \StdClass;
            $obj->level_one = Member::get($levelOne);
            $obj->level_one->members = Member::getBySponsor($levelOne);
            foreach ($obj->level_one->members as $key => $level) {
                $level->members = Member::getBySponsor($level->member_id);
            }
            $data['chart'] = $obj;
        }
        return json_encode(['data' => $data]);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Member::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->user_id) && $request->ajax()) {
            $data = User::get($id);
        }
        return json_encode($data);
    }

    public function getForDropdown(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Member::getForDropdown();
        }
        return json_encode($data);
    }


    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/member/images/members/',
                    'dir' => '/assets/member/images/members/'));
            }
            if (User::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function register(Request $request)
    {
        $data = array();
        $data['status'] = false;
        
        if (($ajaxData = $request->all()) && $request->ajax()) {

            if ($emailData = Member::register($ajaxData)) {
                $data['status'] = true;
                $data['email_data'] = $emailData;
                // Session::flash('status', 'Successfully Registered! Please check your email for verification.');
            } else {
                // Session::flash('error', 'Error, try again.');
            }
        }
        return json_encode($data);
    }

    public function verify(Request $request)
    {
        $earningFilters = array(
            'applicant' => App::getFieldValue(array('table' => 'te_members', 'pkey' => 'token', 'field' => 'member_id', 'val' => $request->token)),
            'sponsor' => App::getFieldValue(array('table' => 'te_members', 'pkey' => 'token', 'field' => 'sponsor', 'val' => $request->token)));
        if (Earning::insert($earningFilters) && Member::verify($request->token)) {
            Session::flash('status', 'Account Activated. You can now login to the system.');
        } else {
            Session::flash('error', 'Error Occured!');
        }
        
        return redirect('/member');
    }

    public function sendEmailVerification(Request $request)
    {
        $data = array();
        $data['status'] = false;
        $email = new EmailVerification(new AppUser(['token' => $request->token, 'name' => $request->name]));
        if (Mail::to($request->email)->send($email)) {
            $data['status'] = true;
        }
        return json_encode($data);
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect::to('/member');
    }
}
