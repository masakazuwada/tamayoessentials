<?php
namespace App\Modules\Member\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Member\Models\Payout;

class PayoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Payout::get(Auth()->user()->member_id);
        }
        return json_encode(['data' => $data]);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Payout::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getTotalPendingByMember(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Payout::getTotalPendingByMember(auth()->user()->member_id, $request->type) < 1) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->payout_id) && $request->ajax()) {
            if (Payout::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}
