<?php
namespace App\Modules\Member\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Member\Models\PurchaseOrder;


class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manage(Request $request)
    {
        return view('Member::purchase_orders');
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = PurchaseOrder::get(Auth()->user()->member_id);
        }
        return json_encode(['data' => $data]);
    }

    public function getLines(Request $request)
    {
        $data = array();
        if (($id = $request->id) && $request->ajax()) {
            $data = PurchaseOrder::getLines($id);
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->purchase_order_id) && $request->ajax()) {
            $data = PurchaseOrder::get($id);
            $data->purchase_order_lines = PurchaseOrder::getLinesForDetails($id);
        }
        return json_encode($data);
    }
}
