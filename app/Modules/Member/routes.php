<?php

use Illuminate\Http\Request;

Route::group(array('namespace' => 'App\Modules\Member\Controllers'), function() {
    Route::group(['middleware' => ['web'], 'prefix' => 'member'], function() {

        // Member
        Route::get('/', ['as'=> 'login' , 'uses' => 'MemberController@login' ]);
        Route::post('/login', 'MemberController@login');
        Route::get('/logout', 'MemberController@logout');
        Route::post('/register', 'MemberController@register');
        Route::get('/get-for-dropdown', 'MemberController@getForDropdown');
        Route::get('/registration/verify', 'MemberController@verify');
        Route::post('/send-email-verification', 'MemberController@sendEmailVerification');

        // Earnings
        Route::get('/earnings', 'EarningController@viewList');
        Route::get('/earning/get', 'EarningController@get');
        Route::get('/earning/check-if-has-balance', 'EarningController@checkIfHasBalance');

        // Pass-up Members
        Route::get('/pass-up/list', 'MemberController@viewList');
        Route::get('/pass-up/get', 'MemberController@get');

        // Pass-up Chart
        Route::get('/pass-up/chart', 'MemberController@viewChart');
        Route::get('/pass-up/create-chart-by-member', 'MemberController@createChartByMember');

        // Purchase Order
        Route::get('/purchase-orders', 'PurchaseOrderController@manage');
        Route::get('/purchase-order/get', 'PurchaseOrderController@get');
        Route::get('/purchase-order/get-details', 'PurchaseOrderController@getDetails');
        Route::get('/purchase-order/get-lines', 'PurchaseOrderController@getLines');

        // Payout
        Route::get('/payout/get', 'PayoutController@get');
        Route::get('/payout/get-total-pending-by-member', 'PayoutController@getTotalPendingByMember');
        Route::post('/payout/save', 'PayoutController@save');
        Route::get('/payout/remove', 'PayoutController@remove');

        // Account
        Route::get('/account', 'AccountController@account');
        Route::post('/account/update-details', 'AccountController@updateDetails');
        Route::post('/account/update-password', 'AccountController@updatePassword');
    
    });
});
