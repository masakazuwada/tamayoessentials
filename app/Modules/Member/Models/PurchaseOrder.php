<?php

namespace App\Modules\Member\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;

class PurchaseOrder extends Model
{
    const TABLE = 'te_purchase_orders';
    const PRIMARY_KEY = 'purchase_order_id';

    public static function get($customer = null)
    {
        $params = array();
        $sql = 'SELECT tepo.purchase_order_id, GROUP_CONCAT(tepro.product SEPARATOR "<br>") AS products, tepo.total, tepo.customer, tepo.date_purchased, tepo.details, tepo.timestamp, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS encoder ';
        $sql .= 'FROM te_purchase_orders tepo ';
        $sql .= 'LEFT JOIN te_purchase_order_lines tepol ON tepol.purchase_order_id = tepo.purchase_order_id ';
        $sql .= 'LEFT JOIN te_products tepro ON tepro.product_id = tepol.product_id ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tepo.encoder ';
        $sql .= 'WHERE tepo.customer = ? ';
        $sql .= 'GROUP BY tepo.purchase_order_id ';
        $sql .= 'ORDER BY tepo.date_purchased DESC, tepo.timestamp DESC';
        
        $params[] = $customer;

        return DB::select($sql, $params);
    }

    public static function getLines($purchaseOrderId)
    {
        $sql = 'SELECT tepol.purchase_order_line_id, tepro.product, tepro.image, tepol.quantity, tepol.discount, tepol.price, tepol.sub_total ';
        $sql .= 'FROM te_purchase_order_lines tepol ';
        $sql .= 'LEFT JOIN te_products tepro ON tepro.product_id = tepol.product_id ';
        $sql .= 'WHERE tepol.purchase_order_id = ? ';
        $sql .= 'GROUP BY tepol.purchase_order_line_id ';
        $sql .= 'ORDER BY tepol.purchase_order_line_id ASC ';

        return DB::select($sql, array($purchaseOrderId));
    }

    public static function getLinesForDetails($id)
    {
        $sql = 'SELECT tepol.purchase_order_line_id, tepol.product_id, tepol.purchase_order_id, tepol.quantity, tepol.discount, tepol.price ';
        $sql .= 'FROM te_purchase_order_lines tepol ';
        $sql .= 'LEFT JOIN te_purchase_orders tepo ON tepo.purchase_order_id = tepol.purchase_order_id ';
        $sql .= 'WHERE tepol.purchase_order_id = ? ';
        $sql .= 'ORDER BY tepol.purchase_order_line_id ';
        return DB::select($sql, array($id));
    }
}