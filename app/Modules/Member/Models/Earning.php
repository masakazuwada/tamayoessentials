<?php

namespace App\Modules\Member\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\Admin\Models\Setting;
use App\Modules\Member\Models\Member;

class Earning extends Model
{
    const TABLE = 'te_earnings';
    const PRIMARY_KEY = 'earning_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tee.date_earned, tee.amount, tee.points, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS invited_by ';
        $sql .= 'FROM te_earnings tee ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tee.invited_by ';
        // If it has an id
        if (!is_null($id)) {
            $sql .= 'WHERE tee.earned_id = ? ';
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'WHERE tee.member_id = ? ';
        $sql .= 'ORDER BY tee.date_earned DESC ';
        return DB::select($sql, array(auth()->user()->member_id));
    }

    public static function getBalancesByMember($data)
    {
        $sql = "SELECT earning_id, {$data['field']} ";
        $sql .= 'FROM te_earnings ';
        $sql .= "WHERE member_id = ? AND {$data['field']} > 0 ";
        $sql .= "ORDER BY {$data['field']} ASC ";
        return DB::select($sql, array($data['member']));
    }

    public static function insert($data)
    {
        $amountEarner = $data['sponsor'];
        $upline = null;
        
        $pointsEarner = $data['sponsor'];
        
        if (Member::getInviteCount($data['sponsor'], ($data['applicant'])) < 1) {
            $upline = App::getFieldValue(array(
                'table' => 'te_members',
                'pkey' => 'member_id',
                'field' => 'sponsor',
                'val' => $data['sponsor']));
            $amountEarner = $upline;
        }

        if ($amountEarner) {
            $earnings = array(
                'member_id' => $amountEarner,
                'date_earned' => date('Y-m-d'),
                'amount' => $amount = Setting::getValue('qualifying_sale_rate'),
                'amount_balance' => $amount,
                'invited_by' => $upline ? $upline : $amountEarner,
                'applicant' => $data['applicant']);

            DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $earnings));
        }

        if ($pointsEarner) {
            $earnings = array(
                'member_id' => $pointsEarner,
                'date_earned' => date('Y-m-d'),
                'points' => $points = Setting::getValue('qualifying_sale_points'),
                'points_balance' => $points,
                'invited_by' => $upline ? $upline : $pointsEarner,
                'applicant' => $data['applicant']);

            DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $earnings));
        }


        return true;
    }

    public static function removeByApplicant($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE applicant = ?";
        return DB::delete($sql, array($id));
    }

    public static function getTotalEarned($data, $table = self::TABLE)
    {
        $params = array();
        $sql = "SELECT COALESCE(SUM({$data['field']}), 0) as total FROM {$table} ";
        $sql .= 'WHERE member_id = ? ';
        $params[] = auth()->user()->member_id;
        if (isset($data['where']) && $data['where']) {
            $sql .= 'AND ? ';
            $params[] = $data['where'];
        }
        return DB::select($sql, $params)[0]->total;
    }
}