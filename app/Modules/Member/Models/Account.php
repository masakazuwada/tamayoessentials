<?php
namespace App\Modules\member\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\App\Models\Person;
use DB;
use Hash;

class Account extends Model
{
    const TABLE = 'te_members';
    const PRIMARY_KEY = 'member_id';

    public static function updateDetails($data)
    {
        $userId = $data['member_id'];
        $person = array(
            'person_id' => $userId,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birthdate' => $data['birthdate'],
            'contact_no' => $data['contact_no'],
            'gender' => $data['gender']);
        DB::insert(App::getInsertUpdateSQL(Person::TABLE, Person::PRIMARY_KEY, $person));

        $user = array(
            'member_id' => $userId,
            'image' => $data['image']);
        DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $user));
    
        return true;
    }

    public static function updatePassword($data)
    {
        $user = array(
            'member_id' => $data['member_id'],
            'password' => Hash::make($data['password']));
        return DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $user));
    }
}