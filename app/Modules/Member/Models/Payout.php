<?php

namespace App\Modules\Member\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\Member\Models\Earning;

class Payout extends Model
{
    const TABLE = 'te_payouts';
    const PRIMARY_KEY = 'payout_id';

    public static function get($member)
    {
        $sql = 'SELECT tepy.payout_id, tepy.member_id, tepy.amount, tepy.type, tepy.request_date, tepy.status, tepy.reviewed_on, tepy.remarks, tepy.timestamp,  ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS reviewed_by ';
        $sql .= 'FROM te_payouts tepy ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tepy.reviewer ';
        $sql .= 'WHERE tepy.member_id = ? ';
        $sql .= 'ORDER BY status, tepy.payout_id DESC';
        return DB::select($sql, array($member));
    }

    public static function getTotalPendingByMember($member, $type)
    {
        $sql = 'SELECT count(payout_id) AS count FROM te_payouts WHERE status = 0 AND member_id = ? AND type = ?';
        return DB::select($sql, array($member, $type))[0]->count;
    }

    public static function insertUpdate($data)
    {
        $payoutData = array(
            'member_id' => Auth()->user()->member_id,
            'amount' => $data['amount'],
            'type' => $data['type'],
            'request_date' => date('Y-m-d'),
            'status' => false);

        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $payoutData);
        return DB::insert($sql);
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }
}