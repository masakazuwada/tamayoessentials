<?php

namespace App\Modules\Member\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\Address;
use App\Modules\Admin\Models\Setting;
use Session;
use Hash;
use Mail;

class Member extends Model
{
    const TABLE = 'te_members';
    const PRIMARY_KEY = 'member_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tem.member_id, tep.first_name, tep.last_name, tep.birthdate, tep.contact_no, tep.gender, tem.image, tem.email, tem.status, tem.status, tem.date_registered, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS full_name ';
        $sql .= 'FROM te_members tem ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tem.person_id ';
        // If it has an id
        if (!is_null($id)) {
            $sql .= 'WHERE tem.member_id = ? ';
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'WHERE tem.sponsor = ? ';
        $sql .= 'ORDER BY tem.member_id ';
        return DB::select($sql, array(auth()->user()->member_id));
    }

    public static function getBySponsor($sponsor)
    {
        $sql = 'SELECT tem.member_id, tem.image, tem.email, tem.status, tem.status, tem.date_registered, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS full_name ';
        $sql .= 'FROM te_members tem ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tem.person_id ';
        $sql .= 'WHERE tem.sponsor = ? ';
        $sql .= 'ORDER BY tem.member_id ';
        return DB::select($sql, array($sponsor));
    }

    public static function getInviteCount($sponsor = null, $memberId = null)
    {
        $params = array();
        $sql = 'SELECT count(member_id) AS count ';
        $sql .= 'FROM te_members ';
        $sql .= 'WHERE sponsor = ? ';
        $params[] = $sponsor;
        if ($memberId) {
            $sql .= 'AND member_id != ? ';
            $params[] = $memberId;
        }

        return DB::select($sql, $params)[0]->count;
    }

    public static function register($data)
    {
        $memberId = Setting::getValue('id_prefix') . str_pad(Sequence::getNextSequence('MBR'), 5, '0', STR_PAD_LEFT);
        $person = array(
            'person_id' => $memberId,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'address_id' => $data['address_id'],
            'birthdate' => $data['birthdate'],
            'contact_no' => $data['contact_no'],
            'gender' => $data['gender']);

        $member = array(
            'member_id' => $memberId,
            'person_id' => $memberId,
            'email' => $data['email'],
            'image' => $data['image'],
            'registration_code' => $data['code'],
            'sponsor' => $data['sponsor'],
            'password' => Hash::make($data['password']),
            'date_registered' => date('Y-m-d'),
            'token' => $token = str_random(15));

        $address = array(
            'address_id' => $data['address_id'],
            'line1' => $data['line1'],
            'country_id' => $data['country_id'],
            'region_id' => $data['region_id'],
            'city_id' => $data['city_id']);

        $code = array(
            'used_by' => $memberId,
            'code' => (string) $data['code']);


        DB::insert(App::getInsertUpdateSQL(Person::TABLE, Person::PRIMARY_KEY, $person));
        DB::insert(App::getInsertUpdateSQL(Address::TABLE, Address::PRIMARY_KEY, $address));
        DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $member, true));
        DB::update('UPDATE te_codes SET used_by = ? WHERE code = ?', array($memberId, $data['code']));

        $email = array('email' => $data['email'], 'token' => $token, 'name' => $data['first_name']);

        return $email;
    }
    
    public static function verify($token, $table = self::TABLE)
    {
        return DB::update("UPDATE {$table} SET verified = 1, token = null WHERE token = ?", array($token));
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }

    public static function getForDropdown()
    {
        $sql = 'SELECT member_id ';
        $sql .= 'FROM te_members ';
        $sql .= 'ORDER BY member_id ';
        return DB::select($sql);
    }
}