<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Control Panel - Tamayo Essentials</title>
    
    <link rel="shortcut icon" href="{{ URL::asset('assets/app/images/logo/logo2.png') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/bootstrap/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/font-awesome/css/font-awesome.css') }}" />
    @yield('styles')
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/app/css/app.css') }}">
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

