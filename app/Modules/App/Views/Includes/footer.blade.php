
        <!-- Large Modal Format -->
        <div class="modal fade" id="large" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>

        <div class="modal modal-fullscreen" id="fullscreen" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>

        <!-- Medium Modal Format -->

        <div class="modal fade" id="medium" tabindex="-1"  role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>

        <!-- Medium_XS Modal Format -->
        <div class="modal fade" id="medium-xs" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>

        <!-- Small Modal Format -->
        <div class="modal fade" id="small" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>

        <!-- Small Modal Format -->
        <div class="modal fade" id="confirmation" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- Ajax Modal Content Here -->
                </div>
            </div>
        </div>
      </div>
{{--       <footer>
        <div class="pull-right">
            Tamayo Essentials - Administration
        </div>
        <div class="clearfix"></div>
    </footer> --}}
    </div>
    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!}
    </script>
    <script src="{{ URL::asset('assets/plugins/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/toastr/toastr-options.js') }}"></script>
    <script src="{{ URL::asset('assets/app/js/app.js') }}"></script>
    <script src="{{ URL::asset('assets/app/js/helper.js') }}"></script>
    @yield('scripts')
  </body>
</html>