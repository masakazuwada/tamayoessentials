<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">
      <i class="fa fa-question-circle fa-fw fg-info"></i>
      <span id="title"></span>
    </h4>
  </div>
  <div class="modal-body form-horizontal">
    <div class="text-left">
      Are you sure about this?
    </div>
    <div class="modal-custom-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">No</button>
      <button type="submit" class="btn btn-primary btn-sm" id="yconfirmation">Confirm</button>
    </div>
  </div>
</div>