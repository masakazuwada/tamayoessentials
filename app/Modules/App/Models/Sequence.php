<?php

namespace App\Modules\App\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;

class Sequence extends Model
{
    const SEQ_GLOBAL = 'GBL';
    const SEQ_MEMBER = 'MBR';
    const SEQ_USER = 'USR';

    public static function getNextSequence($id = self::SEQ_GLOBAL)
    {
        static $retries = 0;
        $sequence = null;
        DB::beginTransaction();

        try {
            DB::update("UPDATE app_sequences SET sequence = sequence + 1 WHERE sequence_id = ?", [$id]);
            $result = DB::select("SELECT sequence FROM app_sequences WHERE sequence_id = ?", array($id));

            if (isset($result[0])) {
                $sequence = $result[0]->sequence;
            }
            else {
                DB::insert("INSERT INTO app_sequences (sequence_id, sequence) VALUES(?, 1000)", array($id));
                $sequence = self::getNextSequence($id);
            }
            DB::commit();
            $retries = 0;
        }
        catch (Exception $e) {
            DB::rollback();
            $retries++;
            if ($retries < 3)
                $sequence = self::getNextSequence($id);
            throw $e;
        }

        return $sequence;
    }

    public static function decrementSequence($id = self::SEQ_GLOBAL)
    {
        return DB::update("UPDATE app_sequences SET sequence = sequence - 1 WHERE sequence_id = ?", [$id]);
    }
}