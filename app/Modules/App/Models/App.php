<?php

namespace App\Modules\App\Models;

use App\Modules\App\Models\Sequence;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Admin\Models\Content;
use DB;
use Exception;
use Hash;
use Request;

class App extends Model
{
    public static function getFieldValue($data)
    {
        $sql = "SELECT {$data['field']} FROM {$data['table']} WHERE `{$data['pkey']}` = ? ";
        $result = DB::select($sql, array($data['val']));
        if (isset($result[0])) {
            $field = $data['field'];
            return $result[0]->$field;
        }
        return NULL;
    }

    public static function getInsertUpdateSQL($table, $pkey, $data, $ai = false)
    {
        $sql = null;
        if ($data) {
            
            if (!isset($data[$pkey]) && $ai == false) {
                $data[$pkey] = Sequence::getNextSequence();
            }
            foreach ($data as $key => $value) {
                // if (!is_null($value) && $key != '_token') {
                if ($key != '_token') {
                    $strVal = '"' . $value . '"';
                    $values[] = "`{$key}` = $strVal";
                }
            }
            $setValues = implode(', ', $values);

            $sql = "INSERT INTO {$table} SET ";
            $sql .= $setValues . " ON DUPLICATE KEY UPDATE {$setValues}";
        }
        return $sql;
    }

    public static function checkAvailability($data)
    {
        $sql = "SELECT {$data['key_name']} AS count FROM {$data['table']} WHERE {$data['key_name']} = ? ";
        $result = DB::select($sql, array($data[$data['key_name']]));
        return count($result) ? FALSE : TRUE;
    }

    public static function checkAvailabilityByExpiration($data)
    {
        $sql = "SELECT {$data['key_name']} AS count FROM {$data['table']} WHERE code = ?  ";
        $sql .= "AND (expiry_date > NOW() ";
        $sql .= "OR used_by = '')";
        $result = DB::select($sql, array($data['code']));

        return count($result) ? TRUE : FALSE;
    }

    public static function checkAvailabilityWithField($data)
    {
        $sql = "SELECT {$data['key_name']} AS count FROM {$data['table']} WHERE {$data['key_name']} = ? ";
        $result = DB::select($sql, array($data[$data['field']]));
        return count($result) ? TRUE : FALSE;
    }

    public static function getThis($pageId, $table, $field)
    {
        $sql = "SELECT {$field} FROM {$table} WHERE page_id = ?";
        $result = DB::select($sql, array($pageId));
        if (isset($result[0])) {
            return $result[0]->$field;
        }
        return null;
    }

    public static function getData($table, $pkey, $id)
    {
        $sql = "SELECT * FROM {$table} WHERE `{$pkey}` = ?";
        return DB::select($sql, array($id));
    }

    public static function getDataByType($table, $pkey, $id, $type = Content::TYPE_CONTENT)
    {
        $sql = "SELECT * FROM {$table} WHERE {$pkey} = ? AND type = ?";
        return DB::select($sql, array($id, $type));
    }


    public static function processImageUpload($requestImage, $data)
    {
        $imagePath = base_path() . $data['rdir'];
        $image = str_shuffle(md5($requestImage->getClientOriginalName())) . '.'
            . $requestImage->getClientOriginalExtension();
        $requestImage->move($imagePath, $image);
        return $data['dir'] . $image;;
    }

    public static function getYoutubeId($url)
    {
        if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
          $value = $id[1];
        } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
          $value = $id[1];
        } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
          $value = $id[1];
        } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
          $value = $id[1];
        }
        else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
            $value = $id[1];
        } else {   
            return null;
        }
        return $value;
    }

    public static function strRandom($length = 15)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length * 2);
            if ($bytes === false) {
                return 'Unable to generate random string.';
            }
            return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
        }
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    public static function money($value)
    {
        $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $value)), 0);
        return $value < 0 ? "({$formatted})" : "{$formatted}";
    }
}