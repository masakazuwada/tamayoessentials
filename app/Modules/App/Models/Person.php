<?php

namespace App\Modules\App\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\Sequence;
use Hash;
use Session;

class Person extends Model
{
    const TABLE = 'app_persons';
    const PRIMARY_KEY = 'person_id';

    public static function setSession($personId)
    {
        $person = self::get($personId);
        if (!empty($person)) {
            Session::put('gender', $person->gender);
            Session::put('user', $person->first_name);
        }
    }

    public static function get($id = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        // If it has an id
        if (!is_null($id)) {
            $sql .= "WHERE {$pkey} = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
            return false;
        }
        $sql .= 'ORDER BY person_id ';
        return DB::select($sql);
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }
}