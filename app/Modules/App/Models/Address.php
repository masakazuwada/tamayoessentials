<?php

namespace App\Modules\App\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\Sequence;

class Address extends Model
{
    const TABLE = 'app_addresses';
    const PRIMARY_KEY = 'address_id';

    public static function get($id = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        // If it has an id
        if (!is_null($id)) {
            $sql .= "WHERE {$pkey} = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY address_id ';
        return DB::select($sql);
    }

    public static function getCountries()
    {
        $result = DB::select("SELECT * FROM app_countries ORDER BY name ASC"); 
        if(!empty($result)) {
            return $result;
        }
        
        return FALSE;
    }

    public static function getRegionsByCountry($countryId)
    {
        return DB::select("SELECT * FROM app_regions WHERE country_id = ? AND name != ''", array($countryId));

    }

    public static function getCitiesByRegion($regionId)
    {
        return DB::select("SELECT * FROM app_cities WHERE region_id = ? AND name != ''", array($regionId));

    }
}