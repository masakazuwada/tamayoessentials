<?php

use Illuminate\Http\Request;

Route::group(array('namespace' => 'App\Modules\App\Controllers'), function() {
    Route::group(['middleware' => ['web']], function() {
        Route::any('/modal/{module}/{modalFileName}', 'AppController@viewModal');
        Route::any('/app/ajax-check-availability', 'AppController@checkAvailability');
        Route::any('/app/ajax-check-availability-by-expiration', 'AppController@checkAvailabilityByExpiration');
        Route::any('/app/ajax-check-availability-with-field', 'AppController@checkAvailabilityWithField');
        Route::any('/app/ajax-check-hash-if-equal', 'AppController@checkHashIfEqual');
        
        Route::get('/app/get-data', 'AppController@getData');

        Route::get('/address/get-countries', 'AddressController@getCountries');
        Route::get('/address/get-regions-by-country', 'AddressController@getRegionsByCountry');
        Route::get('/address/get-cities-by-region', 'AddressController@getCitiesByRegion');
    });
});
