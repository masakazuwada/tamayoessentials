<?php

namespace App\Modules\App\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\App\Models\Address;

class AddressController extends Controller
{
    public function getCountries(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Address::getCountries();
        }
        return json_encode($data);
    }

    public function getRegionsByCountry(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Address::getRegionsByCountry($request->country_id);
        }
        return json_encode($data);
    }

    public function getCitiesByRegion(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Address::getCitiesByRegion($request->region_id);
        }
        return json_encode($data);
    }
}