<?php

namespace App\Modules\App\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\App\Models\App;
use Hash;

class AppController extends Controller
{
    public function viewModal($modules, $modalFileName)
    {
        return view(ucwords($modules) . '::' . $modalFileName);
    }

    public function checkAvailability(Request $request)
    {
        if ($request->ajax()) {
            if (App::checkAvailability($request->all())) {
                return 'true';
            }
        }
        return 'false';
    }
    
    public function checkAvailabilityByExpiration(Request $request)
    {
        if ($request->ajax()) {
            if (App::checkAvailabilityByExpiration($request->all())) {
                return 'true';
            }
        }
        return 'false';
    }
    
    public function checkAvailabilityWithField(Request $request)
    {
        $field = $request->field;
        if ($request->ajax()) {
            if ($request->$field == NULL || App::checkAvailabilityWithField($request->all())) {
                return 'true';
            }
        }
        return 'false';
    }

    public function checkHashIfEqual(Request $request)
    {
        if ($request->ajax()) {
            if (Hash::check($request->current_password, 
                    (isset(Auth()->guard('admin')->user()->password) ? Auth()->guard('admin')->user()->password : Auth()->user()->password))) {
                return 'true';
            }
        }
        return 'false';
    }

    public function getData(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = App::getData($request->table, $request->pkey, $request->id);
        }
        return json_encode($data);
    }
}