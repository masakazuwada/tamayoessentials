<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ Request::root() }}" class="site_title"><i class="fa fa-leaf"></i> <span>TE Admin</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="{{ Auth::guard('admin')->user()->image }}" alt="" class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{ Session::get('user') }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->
    <br />
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Dashboard</h3>
        <ul class="nav side-menu">
          <li>
            <a><i class="fa fa-exchange"></i> Transactions <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ Request::root() }}/administration/transactions/products">Products</a></li>
              <li><a href="{{ Request::root() }}/administration/transactions/purchase-orders">Purchase Orders</a></li>
              <li>
                <a href="{{ Request::root() }}/administration/transactions/payouts">
                Payouts
                @if (Payout::getTotalPending())
                  <span class="badge bg-orange pull-right">{{ Payout::getTotalPending() }}</span>
                @endif
                </a>
              </li>
            </ul>
          </li>
          <li><a href="{{ Request::root() }}/administration/users"><i class="fa fa-users"></i> Users</a></li>
          <li><a href="{{ Request::root() }}/administration/members"><i class="fa fa-universal-access"></i> Members</a></li>
          <li><a href="{{ Request::root() }}/administration/codes"><i class="fa fa-envelope"></i> Invitation Codes</a></li>
          <li>
            <a><i class="fa fa-file-text"></i> Site Pages <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ Request::root() }}/administration/site-pages"><i class="fa fa-edit"></i>Manage</a></li>
              @foreach (Site::buildNavigation(array('manageable' => true)) as $page)
                <li>
                  <a href="{{ Request::root() }}/administration/site-pages/{{ $page->segment }}">{{ $page->name }}</a>
                </li>
              @endforeach
            </ul>
          </li>

          <li>
            <a><i class="fa fa-cogs"></i> Settings <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ Request::root() }}/administration/settings/company">Company</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>