@include('App::header')
@include('Admin::sidebar')
@include('Admin::navbar')
<div class="right_col" role="main">
  <div class="">
    @yield('content')
  </div>
</div>
@include('App::footer')