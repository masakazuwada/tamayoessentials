<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
            <img src="{{ URL::asset(Auth::guard('admin')->user()->image)  }}" alt="">
            {{ Session::get('user') }}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="{{ Request::root() }}/administration/account" data-url="settings" data-method="view" data-id="{{ Auth::guard('admin')->user()->user_id }}"> <i class="fa fa-cog pull-right"></i> My Account</a></li>
            </li>
            <li><a href="{{ Request::root() }}/administration/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->