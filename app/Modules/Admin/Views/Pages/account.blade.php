@extends('Admin::layout')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="page-title">
      <div class="title_left">
        <h3>Account Settings</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="account_tab" role="tab" data-toggle="tab" data-tab="account" aria-expanded="true"><i class="fa fa-user"></i> Account</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="password_tab" data-toggle="tab" data-tab="password" aria-expanded="false"><i class="fa fa-lock"></i> Password</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="account_tab">
              <div class="x_content">
                <form method="POST" id="account_form">
                  {!! csrf_field() !!}
                  <input type="hidden" name="user_id" value="{{ $account->user_id }}">
                  <div class="form-group">
                    <div class="photo-upload text-center">
                      <input type="hidden" name="image" id="image" value="/assets/app/images/user-male.png">
                      <input id="photo" type="file" name="image" class="photo">
                      <label for="photo">
                        <div class="image rou">
                          <img class="round" src="{{ URL::asset($account->image) }}" title="Upload Image" id="photo_preview">
                          <div class="after">
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Product Image"></i>
                          </div>
                        </div>
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control input-sm" name="email" readonly="true" title="Not Editable" value="{{ $account->email }}">
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{ $account->first_name }}">
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{ $account->last_name }}">
                      </div>
                    </div>
                  </div>
                  <div class="pull-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="password_tab">
              <form method="POST" id="password_form">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" value="{{ $account->user_id }}">
                <div class="form-group">
                  <label>Current Password</label>
                  <input type="password" class="form-control input-sm" name="current_password" placeholder="Current Password" id="current_password">
                </div>
                <div class="form-group">
                  <label>New Password</label>
                  <input type="password" class="form-control input-sm" name="password" placeholder="New Password" id="password">
                </div>
                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control input-sm" name="confirm_password" placeholder="Confirm Password" id="confirm_password">
                </div>
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/validation/additional-methods.js') }}"></script>
  <script src="{{ URL::asset('assets/admin/js/accounts.js') }}"></script>
@stop