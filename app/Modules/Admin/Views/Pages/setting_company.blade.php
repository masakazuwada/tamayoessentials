@extends('Admin::layout')

@section('styles')
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/responsive.bootstrap.min.css') }}">
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="page-title">
        <div class="title_left">
          <h3>Settings</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>Company</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form action="" class="">
            {!! csrf_field() !!}
            <input type="hidden" name="setting_id">
            <input type="hidden" name="type" value="{{ Setting::TYPE_COMPANY }}">
            <div class="form-group">
              <label for="">Item</label>
              <select name="key" id="items" class="form-control"></select>
            </div>
            <div class="form-group">
              <label for="">Value</label>
              <input type="text" class="form-control" name="value" placeholder="Value">
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea name="description" id="" rows="3" class="form-control" placeholder="Description"></textarea>
            </div>
            <div class="form-group pull-right">
              <button type="submit" class="btn btn-primary">Set</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="x_panel">
        <div class="x_title">
          <h2>List</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-bordered table-hover" id="c_settings" width="100%">
            <thead>
              <tr>
                <th class="text-center">Item</th>
                <th class="text-center">Value</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/admin/js/c_settings.js') }}"></script>
@stop