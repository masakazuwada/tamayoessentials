@extends('Admin::layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/css/wysihtml5/bootstrap3-wysihtml5.min.css') }}"></link>
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3><a target="_blank" href="{{ Request::root() }}/contacts">Contacts</a></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <form method="POST" id="content_form">
          {!! csrf_field() !!}
          <div class="x_title">
            <h2>Content</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
              <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('contacts') }}">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="address_content_id" value="{{ (isset($content['TYPE_ADDRESS']) ? $content['TYPE_ADDRESS']['content_id'] : "") }}">
                      <label for="">Address</label>
                      <textarea name="address" id="" cols="30" rows="5" class="form-control textarea" placeholder="Address">
                        {!! (isset($content['TYPE_ADDRESS']) ? $content['TYPE_ADDRESS']['description'] : "") !!}
                      </textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="business_content_id" value="{{ (isset($content['TYPE_BUSINESS_HOURS']) ? $content['TYPE_BUSINESS_HOURS']['content_id'] : "") }}">
                      <label for="">Business Hours</label>
                      <textarea name="business_hours" id="" cols="30" rows="5" class="form-control textarea" placeholder="Business Hours">
                        {!! (isset($content['TYPE_BUSINESS_HOURS']) ? $content['TYPE_BUSINESS_HOURS']['description'] : "") !!}
                      </textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="contact_content_id" value="{{ (isset($content['TYPE_CONTACT_NUMBERS']) ? $content['TYPE_CONTACT_NUMBERS']['content_id'] : "") }}">
                      <label for="">Contact Numbers</label>
                      <textarea name="contact_nos" id="" cols="30" rows="5" class="form-control textarea" placeholder="Contact Numbers">
                        {!! (isset($content['TYPE_CONTACT_NUMBERS']) ? $content['TYPE_CONTACT_NUMBERS']['description'] : "") !!}
                      </textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="hidden" name="pri_email_content_id" value="{{ (isset($content['TYPE_PRIMARY_EMAIL']) ? $content['TYPE_PRIMARY_EMAIL']['content_id'] : "") }}">
                      <label for="">Primary E-mail</label>
                      <input type="text" class="form-control" name="pri_email" placeholder="E-mail Address" value="{{ (isset($content['TYPE_PRIMARY_EMAIL']) ? $content['TYPE_PRIMARY_EMAIL']['description'] : "") }}" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="hidden" name="sup_email_content_id" value="{{ (isset($content['TYPE_SUPPORT_EMAIL']) ? $content['TYPE_SUPPORT_EMAIL']['content_id'] : "") }}">
                      <label for="">Support E-mail</label>
                      <input type="text" class="form-control" name="sup_email" placeholder="E-mail Address" value="{{ (isset($content['TYPE_SUPPORT_EMAIL']) ? $content['TYPE_SUPPORT_EMAIL']['description'] : "") }}" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="twitter_content_id" value="{{ (isset($content['TYPE_TWITTER']) ? $content['TYPE_TWITTER']['content_id'] : "") }}">
                      <label for="">Twitter</label>
                      <input type="text" class="form-control" name="twitter" placeholder="Twitter" value="{{ (isset($content['TYPE_TWITTER']) ? $content['TYPE_TWITTER']['description'] : "") }}" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="facebook_email_content_id" value="{{ (isset($content['TYPE_FACEBOOK']) ? $content['TYPE_FACEBOOK']['content_id'] : "") }}">
                      <label for="">Facebook</label>
                      <input type="text" class="form-control" name="facebook" placeholder="Facebook" value="{{ (isset($content['TYPE_FACEBOOK']) ? $content['TYPE_FACEBOOK']['description'] : "") }}" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="hidden" name="youtube_email_content_id" value="{{ (isset($content['TYPE_YOUTUBE']) ? $content['TYPE_YOUTUBE']['content_id'] : "") }}">
                      <label for="">Youtube</label>
                      <input type="text" class="form-control" name="youtube" placeholder="Youtube" value="{{ (isset($content['TYPE_YOUTUBE']) ? $content['TYPE_YOUTUBE']['description'] : "") }}" />
                  </div>
                </div>
              </div>
              <div class="form-group pull-right">
                <button type="submit" class="btn btn-success" value="content_submit_btn">Save Changes</button>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/wysihtml5x-toolbar.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/handlebars.runtime.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/bootstrap3-wysihtml5.min.js') }}"></script>
  <script>
    $('.textarea').wysihtml5({
      toolbar: {
        "font-styles": true,
        "emphasis": true,
        "lists": true,
        "html": true,
        "link": false,
        "image": false,
        "color": false,
        "blockquote": false,
        "size": 'sm'
      },
    });
  </script>
  <script src="{{ URL::asset('assets/admin/js/contacts.js') }}"></script>
@stop