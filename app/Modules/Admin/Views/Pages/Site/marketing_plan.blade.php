@extends('Admin::layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/css/wysihtml5/bootstrap3-wysihtml5.min.css') }}"></link>
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3><a target="_blank" href="{{ Request::root() }}/marketing-plan">Marketing Plan</a></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <form method="POST" id="slider_form">
          {!! csrf_field() !!}
          <div class="x_title">
            <h2>Carousel Slide <a href="javascript:void(0)" id="add_more_content" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add More</a></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('marketing-plan') }}">
            <input type="hidden" name="slider_id" value="{{ (isset($slider['slider']) ? $slider['slider']->slider_id  : '') }}">
            <label for="">Carousel Data</label>
            @if (isset($slider['slider_data']) && !empty($slider['slider_data']))
              @foreach ($slider['slider_data'] as $key => $sd)
                <div class="form-group">
                  <input type="hidden" name="slider_data_id[]" value="{{ $sd->slider_data_id }}">
                  <div class="row">
                    <div class="pull-right">
                      <a href="javascript:void(0)" class="btn btn-danger delete_btn {{ $key < 1 ? 'disabled' : '' }}" data-id="{{ isset($sd->slider_data_id) ? $sd->slider_data_id : '' }}"><i class="fa fa-close"></i></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="">Image</label>
                      <div class="photo-upload text-center">
                        <input type="hidden" name="image[]" id="image" value="{{ (isset($sd->image) ? $sd->image : '/assets/app/images/no_image.jpg') }}">
                        <input id="content_photo_{{ $key }}" type="file" name="image[]" class="photo">
                        <label for="content_photo_{{ $key }}">
                          <div class="image rou">
                            <img class="round" src="{{ (isset($sd->image) ? URL::asset($sd->image) : URL::asset('assets/app/images/no_image.jpg')) }}" title="Upload Image" id="photo_preview">
                            <div class="after">
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                            </div>
                          </div>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <label for="">Description</label>
                      <textarea name="description[]" id="" cols="30" rows="5" class="form-control first_textarea" placeholder="Description">{!! (isset($sd->description) ? $sd->description : '') !!}</textarea>
                    </div>
                  </div>
                </div>
              @endforeach
            @else
            <div class="form-group">
              <input type="hidden" name="slider_data_id[]" value="">
              <div class="row">
                <div class="pull-right">
                <a href="javascript:void(0)" class="btn btn-danger delete_btn disabled" data-id=""><i class="fa fa-close"></i></a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="">Image</label>
                  <div class="photo-upload text-center">
                    <input type="hidden" name="image[]" id="image">
                    <input id="content_photo" type="file" name="image[]" class="photo">
                    <label for="content_photo">
                      <div class="image rou">
                        <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                        <div class="after">
                          <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                        </div>
                      </div>
                    </label>
                  </div>
                </div>
                <div class="col-md-8">
                  <label for="">Description</label>
                  <textarea name="description[]" id="" cols="30" rows="5" class="form-control first_textarea" placeholder="Description"></textarea>
                </div>
              </div>
            </div>
            @endif
            <div id="slider_file_last_row"></div>
            <div class="form-group">
              <label for="">Details</label>
              <textarea name="details" id="" class="form-control first_textarea" cols="30" rows="7" placeholder="Details">{{ ($slider['slider'] ? $slider['slider']->details : "") }}</textarea>
            </div>
            <div class="form-group pull-right">
              <button type="submit" class="btn btn-success" value="slider_submit_btn">Save Changes</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/wysihtml5x-toolbar.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/handlebars.runtime.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/bootstrap3-wysihtml5.min.js') }}"></script>
  <script>
    $('.first_textarea').wysihtml5({
      toolbar: {
        "font-styles": true,
        "emphasis": true,
        "lists": true,
        "html": true,
        "link": false,
        "image": false,
        "color": false,
        "blockquote": false,
        "size": 'sm'
      },
    });
  </script>
  <script src="{{ URL::asset('assets/admin/js/marketing_plan.js') }}"></script>
@stop