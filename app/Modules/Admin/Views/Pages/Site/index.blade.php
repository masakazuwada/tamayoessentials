@extends('Admin::layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/css/wysihtml5/bootstrap3-wysihtml5.min.css') }}"></link>
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3><a target="_blank" href="{{ Request::root() }}/home">Home</a></h3>
    </div>
  </div>
  <div class="row">
    {{-- Slider --}}
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Slider <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <form id="slider_form" method="POST">
            {!! csrf_field() !!}
            <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('home') }}">
            <input type="hidden" name="slider_id" value="{{ (isset($slider['slider']->slider_id) ? $slider['slider']->slider_id : '')  }}">
            <div class="form-group">
              <label for="">Slider Backgroup Image</label>
              <div class="photo-upload">
                <input type="hidden" name="slider_bg_image" id="image" value="{{ (isset($slider['slider']->background_image) ? $slider['slider']->background_image : '/assets/app/images/no_image.jpg') }}">
                <input id="photo" type="file" name="slider_bg_image" class="photo">
                <label for="photo">
                  <div class="image rect">
                    <img class="rectangle img-responsive" src="{{ (isset($slider['slider']->background_image) ? URL::asset($slider['slider']->background_image) : URL::asset('assets/app/images/no_image.jpg')) }}" title="Upload Image" id="photo_preview">
                    <div class="after">
                      <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                    </div>
                  </div>
                </label>
              </div>
            </div>
            <label>
              Slider Data
              <a href="javascript:void(0)" id="add_more_data" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add More</a>
            </label>
            @if (isset($slider['slider_data']))
              @foreach ($slider['slider_data'] as $key => $sd)
                <div class="form-group slider-data">
                  <div class="row">
                    <div class="pull-right">
                      <a href="javascript:void(0)" class="btn btn-danger delete_btn {{ $key < 1 ? 'disabled' : '' }}" data-id="{{ isset($sd->slider_data_id) ? $sd->slider_data_id : '' }}" style="top: 0"><i class="fa fa-close"></i></a>
                    </div>
                  </div>
                  <div class="row">
                    <input type="hidden" name="slider_data_id[]" value="{{ (isset($sd->slider_data_id) ? $sd->slider_data_id : '') }}">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="">Image</label>
                        <div class="photo-upload text-center">
                          <input type="hidden" name="slider_data_image[{{ $key }}]" value="{{ (isset($sd->image) ? $sd->image : '/assets/app/images/no_image.jpg') }}">
                          <input id="photo_{{ $key }}" type="file" name="slider_data_image[{{ $key }}]" class="photo">
                          <label for="photo_{{ $key }}">
                            <div class="image rou">
                              <img class="round" src="{{ (isset($sd->image) ? URL::asset($sd->image) : URL::asset(URL::asset('assets/app/images/no_image.jpg'))) }}" title="Upload Image" id="photo_preview">
                              <div class="after">
                              <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                              </div>
                            </div>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <label for="">Title</label>
                      <textarea name="slider_data_title[]" class="form-control" cols="30" rows="5" placeholder="Title">{!! (isset($sd->title) ? $sd->title : '') !!}</textarea>
                    </div>
                    <div class="col-md-4">
                      <label for="">Description</label>
                      <textarea name="slider_data_desc[]" id="" cols="30" rows="5" class="form-control" placeholder="Description">{!! (isset($sd->description) ? $sd->description : '') !!}</textarea>
                    </div>
                  </div>
                </div>
              @endforeach
            @endif
            <div id="slider_data_last_row"></div>
            <div class="form-group pull-right">
              <button type="submit" class="btn btn-success" value="slider_submit_btn">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    {{-- Company --}}
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Company <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <form method="POST" id="company_form">
            {!! csrf_field() !!}
            <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('home') }}">
            <input type="hidden" name="company_id" value="{{ (isset($company->company_id) ? $company->company_id : '')  }}">
            <div class="form-group">
              <label for="">Header Text</label>
              <input type="text" class="form-control" name="header" placeholder="Header Text" value="{{ (isset($company->header) ? $company->header : '') }}">
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="">Mission</label>
                  <textarea name="mission" id="" cols="30" rows="5" class="form-control" placeholder="Mission">{!! (isset($company->mission) ? $company->mission : '') !!}</textarea>
                </div>
                <div class="col-md-4">
                  <label for="">Vision</label>
                  <textarea name="vision" id="" cols="30" rows="5" class="form-control" placeholder="Vision">{!! (isset($company->vision) ? $company->vision : '') !!}</textarea>
                </div>
                <div class="col-md-4">
                  <label for="">Core Values</label>
                  <textarea name="core_values" id="" cols="30" rows="5" class="form-control" placeholder="Core Values">{!! (isset($company->core_values) ? $company->core_values : '') !!}</textarea>
                </div>
              </div>
            </div>
            <div class="form-group pull-right">
              <button type="submit" class="btn btn-success" value="company_submit_btn">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    {{-- Content --}}
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
          <form method="POST" id="content_form">
            {!! csrf_field() !!}
            <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('home') }}">
            <div class="x_title">
              <h2>Content <a href="javascript:void(0)" id="add_more_content" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add More</a></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              @if (isset($contents))
                @foreach ($contents as $key => $content)
                  <div class="form-group">
                    <input type="hidden" name="content_id[]" value="{{ (isset($content->content_id) ? $content->content_id : '')  }}">
                    <div class="row">
                      <div class="pull-right">
                        <a href="javascript:void(0)" class="btn btn-danger delete_btn {{ $key < 1 ? 'disabled' : '' }}" data-id="{{ isset($content->content_id) ? $content->content_id : '' }}"><i class="fa fa-close"></i></a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <label for="">Image</label>
                        <div class="photo-upload text-center">
                          <input type="hidden" name="image[]" id="image" value="{{ (isset($content->image) ? $content->image : '/assets/app/images/no_image.jpg') }}">
                          <input id="content_photo_{{ $key }}" type="file" name="image[]" class="photo">
                          <label for="content_photo_{{ $key }}">
                            <div class="image rou">
                              <img class="round" src="{{ (isset($content->image) ? URL::asset($content->image) : URL::asset('assets/app/images/no_image.jpg')) }}" title="Upload Image" id="photo_preview">
                              <div class="after">
                              <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                              </div>
                            </div>
                          </label>
                        </div>
                      </div>
                      <div class="col-md-8">
                        <label for="">Description</label>
                        <textarea name="description[]" id="" cols="30" rows="5" class="form-control textarea" placeholder="Description">{!! (isset($content->description) ? $content->description : '') !!}</textarea>
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
              <div id="content_last_row"></div>

              <div class="form-group pull-right">
                <button type="submit" class="btn btn-success" value="content_submit_btn">Save Changes</button>
              </div>
            </div>
          </form>
      </div>
    </div>
    {{-- Pass-up Marketting Plan --}}
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
          <form method="POST" id="passup_form">
            {!! csrf_field() !!}
            <input type="hidden" name="page_id" value="{{ Site::getIdBySegment('home') }}">
            <div class="x_title">
              <h2>Pass-up Marketting Plan</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="form-group">
                <div class="row">
                  @if (isset($passup) && !empty($passup))
                    @foreach ($passup as $key => $passup)
                      <div class="col-md-4">
                        <div class="photo-upload text-center">
                          <input type="hidden" name="content_id[]" value="{{ $passup->content_id }}">
                          <input type="hidden" name="image[]" id="image" value="{{ (isset($passup->image) ? $passup->image : '/assets/app/images/no_image.jpg') }}">
                          <input id="content_photo_passup{{ $key }}" type="file" name="image[]" class="photo">
                          <label for="content_photo_passup{{ $key }}">
                            <div class="image rou">
                              <img class="round" src="{{ (isset($passup->image) ? URL::asset($passup->image) : URL::asset('assets/app/images/no_image.jpg')) }}" title="Upload Image" id="photo_preview">
                              <div class="after">
                              <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                              </div>
                            </div>
                          </label>
                        </div>
                      </div>
                    @endforeach
                  @else
                    <div class="col-md-4">
                      <input type="hidden" name="content_id[]" value="">
                      <div class="photo-upload text-center">
                        <input type="hidden" name="image[]" id="image" value="">
                        <input id="content_photo_passup1" type="file" name="image[]" class="photo">
                        <label for="content_photo_passup1">
                          <div class="image rou">
                            <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                            <div class="after">
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                            </div>
                          </div>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <input type="hidden" name="content_id[]" value="">
                      <div class="photo-upload text-center">
                        <input type="hidden" name="image[]" id="image" value="">
                        <input id="content_photo_passup2" type="file" name="image[]" class="photo">
                        <label for="content_photo_passup2">
                          <div class="image rou">
                            <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                            <div class="after">
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                            </div>
                          </div>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <input type="hidden" name="content_id[]" value="">
                      <div class="photo-upload text-center">
                        <input type="hidden" name="image[]" id="image" value="">
                        <input id="content_photo_passup3" type="file" name="image[]" class="photo">
                        <label for="content_photo_passup3">
                          <div class="image rou">
                            <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                            <div class="after">
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i>
                            </div>
                          </div>
                        </label>
                      </div>
                    </div>
                  @endif
                </div>
              </div>
              <div class="form-group pull-right">
                <button type="submit" class="btn btn-success" value="passup_submit_btn">Save Changes</button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/wysihtml5x-toolbar.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/handlebars.runtime.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/wysihtml5/bootstrap3-wysihtml5.min.js') }}"></script>
  <script src="{{ URL::asset('assets/admin/js/index.js') }}"></script>
  <script>
    $('.textarea').wysihtml5({
      toolbar: {
        "font-styles": true,
        "emphasis": true,
        "lists": true,
        "html": true,
        "link": false,
        "image": false,
        "color": false,
        "blockquote": false,
        "size": 'sm'
      },
    });
    jQuery('.wysihtml5').wysihtml5({
      "events": {
        "load": function () {
          jQuery('.wysihtml5').addClass('nicehide');
        }
      }
    });
  </script>
@stop