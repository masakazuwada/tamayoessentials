@extends('Admin::layout')

@section('styles')
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/datatables/responsive.bootstrap.min.css') }}">
@stop

@section('content')
  <div class="page-title">
    <div class="title_left">
      <h3><a target="_blank" href="{{ Request::root() }}/products">Products</a></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>
            List
            <a href="javascript:void(0)" data-bind="{{ isset($data_bind) ? $data_bind : '' }}" data-method="insert" class="btn btn-primary btn-xs" title="New">
              <i class="fa fa-plus"></i>
              New
            </a>
          </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-bordered table-hover" id="products" width="100%">
            <thead>
              <tr>
                <th class="text-center" data-priority="1">CODE</th>
                <th class="text-center"><i class="fa fa-image"></i></th>
                <th class="text-center" data-priority="2">Product</th>
                {{-- <th class="text-center" data-priority="3">Price List</th> --}}
                <th class="text-center" data-priority="3">Original Price</th>
                <th class="text-center" data-priority="4">Selling Price</th>
                <th class="text-center">Description</th>
                <th class="text-center">Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/plugins/js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/datatables/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/admin/js/products.js') }}"></script>
@stop