<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title" id="myModalLabel"><span id="title">Approve Payout</span></h4>
  </div>
  <div class="modal-body form-horizontal">
    <div class="text-left">
      <form method="POST">
        {!! csrf_field() !!}
        <input type="hidden" name="payout_id">
        <input type="hidden" name="status">
        <div class="form-group">
          <label>Remarks</label>
          <textarea class="form-control input-sm" name="remarks" id="" cols="3" rows="3" placeholder="Remarks"></textarea>
        </div>
        <div class="modal-foot">
          <button type="submit" class="btn btn-success">Approve</button>
        </div>
          <p><small><i><b>Note:</b> This action cannot be undone.</i></small></p>
      </form>
    </div>
  </div>
</div>