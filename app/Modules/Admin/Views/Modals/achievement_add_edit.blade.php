<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Achievement</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="form-group">
                  <div class="photo-upload text-center">
                    <label for="photo">
                      <div class="image rou">
                        <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                        <div class="after">
                        <i class="fa fa-plus-circle" id="upload_icon" title="Upload Product Image"></i>
                        </div>
                      </div>
                    </label>
                    <input type="hidden" name="image" id="image" value="/assets/app/images/no_image.jpg">
                    <input id="photo" type="file" name="image">
                  </div>
              </div>
            </div>
          </div>
          <input type="hidden" name="achievement_id">
          <div class="form-group">
            <label>Achievement</label>
            <input type="text" class="form-control input-sm" name="achievement" placeholder="Achievement">
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea class="form-control input-sm" name="details" id="" cols="3" rows="3" placeholder="Details"></textarea>
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
