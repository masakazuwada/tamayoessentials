<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="outline:none">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
    <h4 class="modal-title">
      Purchase Order
      <a href="javascript:void(0)" class="btn btn-warning btn-xs add_button" style="line-height: 1.5" title="Add More" id="add_order_line">
        <i class="fa fa-plus"></i>
        Add More
      </a>
    </h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          <input type="hidden" name="purchase_order_id">
          <div class="row visible-lg">
            <div class="col-md-5">
              <label>Product Name (Code)</label>
            </div>
            <div class="col-md-2">
              <label>Quantity</label>
            </div>
            <div class="col-md-2">
              <label>% Discount</label>
            </div>
            <div class="col-md-2">
              <label>Price</label>
            </div>
            <div class="col-md-1 hidden-xs">
              <i class="fa fa-cogs"></i>
            </div>
          </div>
          <div id="purchase_order_line"></div>
          <div class="row">
            <div class="col-md-5 col-xs-12">
              <div class="form-group">
                <label>Customer Type</label>
                <div style="border: 1px solid #ccc; padding: 4px;">
                  <label class="radio-inline">
                    <input type="radio" name="customer_type" value="0" id="walk_in"> Walk-in
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="customer_type" value="1" id="pass_up_member"> Pass-up member
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-7 col-xs-12">
              <div class="form-group">
                <label for="">Customer</label>
                <span id="customer">
                    <input type="text" class="form-control input-sm" name="customer" placeholder="Customer" id="wi_customer">
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Date Purchased</label>
            <input type="date" class="form-control input-sm" name="date_purchased" placeholder="Date Purchased" value="{{ date('Y-m-d') }}">
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea class="form-control input-sm" name="details" id="" cols="3" rows="3" placeholder="Details"></textarea>
          </div>
          <div class="modal-foot">
            <button type="button" class="btn btn-default hidden" data-dismiss="modal" tabindex="-1">Cancel</button>
            <button type="submit" class="btn btn-primary">Save Changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
