<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Codes</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <form method="POST">
          {!! csrf_field() !!}
          <div class="form-group">
            <label>No. to generate</label>
            <input type="number" class="form-control" name="no_to_generate" placeholder="No. to generate" value="10">
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
