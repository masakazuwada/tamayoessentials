<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Page</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          <input type="hidden" name="page_id">
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control input-sm" name="name" placeholder="Name">
          </div>
          <div class="form-group">
            <label>Parent</label>
            <select name="parent" class="form-control" id="parents"></select>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
              <option value="1">Active</option>
              <option value="0">Inactive</option>
            </select>
          </div>
          <div class="form-group">
            <label>Summary</label>
            <textarea class="form-control" name="summary" id="" cols="30" rows="3" placeholder="Summary"></textarea>
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
