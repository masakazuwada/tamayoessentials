<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Product</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          <input type="hidden" name="product_id">
          <div class="form-group">
            <div class="photo-upload text-center">
              <label for="photo">
                <div class="image rou">
                  <img class="round" src="{{ URL::asset('assets/app/images/no_image.jpg') }}" title="Upload Image" id="photo_preview">
                  <div class="after">
                    <i class="fa fa-plus-circle" id="upload_icon" title="Upload Product Image"></i>
                  </div>
                </div>
              </label>
              <input type="hidden" name="image" id="image" value="/assets/app/images/no_image.jpg">
              <input id="photo" type="file" name="image">
            </div>
          </div>
          <div class="form-group">
            <label>Code</label>
            <input type="text" class="form-control input-sm" name="code" placeholder="Code" value="{{ 'TE-' . implode("-", str_split(strtoupper(substr(md5(uniqid()), 4, 9)), 3)) }}">
          </div>
          <div class="row">
            <div class="col-md-10 col-xs-8">
              <div class="form-group">
                <label>Product</label>
                <input type="text" class="form-control input-sm" name="product" placeholder="Product">
              </div>
            </div>
            <div class="col-md-2 col-xs-4">
              <div class="form-group">
                <label>Advertised</label>
                <div>
                  <label for="advertised">
                    <input type="checkbox" name="advertised" value="1" id="advertised"> Yes
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6">
              <div class="form-group">
                <label>Original Price</label>
                <input type="number" class="form-control input-sm" name="original_price" placeholder="Original Price">
              </div>
            </div>
            <div class="col-md-6 col-xs-6">
              <div class="form-group">
                <label>Selling Price</label>
                <input type="number" class="form-control input-sm" name="selling_price" placeholder="Selling Price">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea class="form-control input-sm" name="description" id="" cols="3" rows="3" placeholder="Description"></textarea>
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
