<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Member</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          <input type="hidden" name="member_id">
          <div class="form-group">
            <div class="photo-upload text-center">
              <label for="photo">
                <div class="image rou">
                  <img class="round" src="{{ URL::asset('assets/app/images/user-male.png') }}" title="Upload Image" id="photo_preview">
                  <div class="after">
                    <i class="fa fa-plus-circle" id="upload_icon" title="Upload Product Image"></i>
                  </div>
                </div>
              </label>
              <input type="hidden" name="image" id="image" value="/assets/app/images/user-male.png">
              <input id="photo" type="file" name="image">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name">
              </div>
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control input-sm" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label>Password</label> <span class="hide" style="font-size: 70%; font-style: italic; color: red">(Empty = No password changes)</span>
            <input id="password_input" type="password" class="form-control input-sm" name="password" placeholder="Password">
            <div>
              <label>
                <input id="show_password" type="checkbox" /> Show password
              </label>
            </div>
          </div>
          <div class="form-group sponsor">
            <label for="">Sponsor</label>
            <select name="sponsor" id="sponsor" class="form-control input-sm"></select>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6">
              <div class="form-group">
                <label>Date Registered</label>
                <input type="date" class="form-control input-sm" name="date_registered" placeholder="Date Registered">
              </div>
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control input-sm">
                  <option value="">Select Status</option>
                  <option value="1">Active</option>
                  <option value="0">Inactive</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-xs-12">
              <div class="form-group">
                <label>Birthdate</label>
                <input type="date" class="form-control input-sm" name="birthdate" placeholder="Birthdate">
              </div>
            </div>
            <div class="col-md-5 col-xs-12">
              <div class="form-group">
                <label>Contact #</label>
                <input type="text" class="form-control input-sm" name="contact_no" placeholder="Contact No.">
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="form-group">
                <label>Gender</label>
                <select name="gender" class="form-control input-sm">
                  <option value="">Select Gender</option>
                  <option value="1">Male</option>
                  <option value="2">Female</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-xs-12">
              <div class="form-group">
                <input type="hidden" name="address_id">
                <label for="">Country</label>
                <select name="country_id" id="country_id" class="form-control input-sm">
                  <option value="">Select Country</option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-xs-6">
              <div class="form-group">
                <label for="">Region</label>
                <select name="region_id" class="form-control input-sm">
                  <option value="">Select Region</option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-xs-6">
              <div class="form-group">
                <label for="">City</label>
                <select name="city_id" class="form-control input-sm">
                  <option value="">Select City</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="">Address</label>
            <input type="text" class="form-control input-sm" name="line1" placeholder="Address">
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
