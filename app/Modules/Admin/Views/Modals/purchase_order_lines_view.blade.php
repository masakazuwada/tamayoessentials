<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="outline:none">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Purchase Order Lines</h4>
  </div>
  <div class="modal-body">
    <table class="table table-bordered table-hover" id="purchase_order_lines" width="100%">
      <thead>
        <tr>
          <th class="text-center" data-priority="6"><i class="fa fa-image"></i></th>
          <th class="text-center" data-priority="1">Product</th>
          <th class="text-center" data-priority="2">Quantity</th>
          <th class="text-center" data-priority="4">Discount</th>
          <th class="text-center" data-priority="5">Price</th>
          <th class="text-center" data-priority="3">Subtotal</th>
        </tr>
      </thead>
    </table>
  </div>
</div>