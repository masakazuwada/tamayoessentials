<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Gallery</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <form method="POST">
          {!! csrf_field() !!}
          <input type="hidden" name="gallery_id">
          <input type="hidden" name="type" value="2">
          <div class="form-group">
            <label for="">Youtube URL</label>
            <input type="text" class="form-control" name="content" placeholder="Youtube URL">
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea class="form-control input-sm" name="details" id="" cols="3" rows="3" placeholder="Details"></textarea>
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
