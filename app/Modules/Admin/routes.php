<?php

use Illuminate\Http\Request;

Route::group(array('namespace' => 'App\Modules\Admin\Controllers'), function() {
    Route::group(['middleware' => ['web'], 'prefix' => 'administration'], function() {

        // Administration
        Route::get('/', ['as'=> 'login' , 'uses' => 'AdminController@login' ]);
        Route::post('/login', 'AdminController@login');
        Route::get('/logout', 'AdminController@logout');

        // Site Pages
        Route::get('/site-pages', 'SiteController@viewList');
        Route::get('/site-pages/get', 'SiteController@get');
        Route::get('/site-pages/get-details', 'SiteController@getDetails');
        Route::get('/site-pages/get-levels-for-dropdown', 'SiteController@getLevelsForDropdown');
        Route::post('/site-pages/save', 'SiteController@save');
        Route::get('/site-pages/reorder', 'SiteController@reorder');
        
        // Home
        Route::get('/site-pages/home', 'IndexController@manage');
        Route::post('/site-pages/home/save-slider', 'IndexController@saveSlider');
        Route::post('/site-pages/home/save-company', 'IndexController@saveCompany');
        Route::post('/site-pages/home/save-content', 'IndexController@saveContent');
        Route::post('/site-pages/home/save-passup', 'IndexController@savePassup');
        Route::get('/site-pages/home/remove-slider', 'IndexController@removeSlider');
        Route::get('/site-pages/home/remove-content', 'IndexController@removeContent');

        // Services
        Route::get('/site-pages/services', 'ServiceController@manage');
        Route::post('/site-pages/services/save-content', 'ServiceController@saveContent');
        Route::get('/site-pages/services/remove-line', 'ServiceController@removeLine');
        Route::post('/site-pages/services/save-slider', 'ServiceController@saveSlider');
        Route::get('/site-pages/services/remove-slider', 'ServiceController@removeSlider');

        // About
        Route::get('/site-pages/about-us', 'AboutController@manage');
        Route::post('/site-pages/about-us/save-slider', 'AboutController@saveSlider');
        Route::get('/site-pages/about-us/remove-slider', 'AboutController@removeSlider');
        Route::post('/site-pages/about-us/save-team', 'AboutController@saveTeam');
        Route::get('/site-pages/about-us/remove-team', 'AboutController@removeTeam');
        
        // Contacts
        Route::get('/site-pages/contacts', 'ContactController@manage');
        Route::post('/site-pages/contacts/save-content', 'ContactController@saveContent');

        // Achievements
        Route::get('/site-pages/achievements', 'AchievementController@manage');
        Route::get('/site-pages/achievements/get', 'AchievementController@get');
        Route::post('/site-pages/achievements/save', 'AchievementController@save');
        Route::get('/site-pages/achievements/get-details', 'AchievementController@getDetails');
        Route::get('/site-pages/achievements/remove', 'AchievementController@remove');

        // Gallery
        Route::get('/site-pages/gallery', 'GalleryController@manage');
        Route::get('/site-pages/gallery/get', 'GalleryController@get');
        Route::post('/site-pages/gallery/save', 'GalleryController@save');
        Route::get('/site-pages/gallery/get-details', 'GalleryController@getDetails');
        Route::get('/site-pages/gallery/remove', 'GalleryController@remove');

        // Marketting Plan
        Route::get('/site-pages/marketing-plan', 'MarketingPlanController@manage');
        Route::post('/site-pages/marketing-plan/save-slider', 'MarketingPlanController@saveSlider');
        Route::get('/site-pages/marketing-plan/remove-slider', 'MarketingPlanController@removeSlider');

        // Users
        Route::get('/users', 'UserController@viewList');
        Route::get('/user/get', 'UserController@get');
        Route::get('/user/get-details', 'UserController@getDetails');
        Route::get('/user/remove', 'UserController@remove');
        Route::post('/user/save', 'UserController@save');
        Route::get('/user/reset-password', 'UserController@resetPassword');

        // Members
        Route::get('/members', 'MemberController@viewList');
        Route::get('/member/get', 'MemberController@get');
        Route::get('/member/get-details', 'MemberController@getDetails');
        Route::get('/member/get-for-dropdown', 'MemberController@getForDropdown');
        Route::get('/member/remove', 'MemberController@remove');
        Route::post('/member/save', 'MemberController@save');
        Route::get('/member/reset-password', 'MemberController@resetPassword');

        // Codes
        Route::get('/codes', 'CodeController@viewList');
        Route::get('/code/get', 'CodeController@get');
        Route::post('/code/save', 'CodeController@save');
        Route::get('/code/void', 'CodeController@void');

        // Products
        Route::get('/transactions/products', 'ProductController@manage');
        Route::get('/transactions/product/get', 'ProductController@get');
        Route::post('/transactions/product/save', 'ProductController@save');
        Route::get('/transactions/product/get-details', 'ProductController@getDetails');
        Route::get('/transactions/product/get-details-for-select', 'ProductController@getDetailsForSelect');
        Route::get('/transactions/product/remove', 'ProductController@remove');
        Route::get('/transactions/product/get-for-dropdown', 'ProductController@getForDropdown');

        // Purchase Orders
        Route::get('/transactions/purchase-orders', 'PurchaseOrderController@manage');
        Route::get('/transactions/purchase-order/get', 'PurchaseOrderController@get');
        Route::get('/transactions/purchase-order/get-details', 'PurchaseOrderController@getDetails');
        Route::get('/transactions/purchase-order/get-lines', 'PurchaseOrderController@getLines');
        Route::post('/transactions/purchase-order/save', 'PurchaseOrderController@save');
        Route::get('/transactions/purchase-order/remove', 'PurchaseOrderController@remove');

        // Payouts
        Route::get('/transactions/payouts', 'PayoutController@manage');
        Route::get('/transactions/payout/get', 'PayoutController@get');
        Route::post('/transactions/payout/save', 'PayoutController@save');
        
        // Settings
        Route::get('/settings/company', 'SettingController@company');
        Route::get('/settings/get-company', 'SettingController@getCompany');
        Route::get('/settings/get-company-items-for-dropdown', 'SettingController@getCompanyItemsForDropdown');
        Route::post('/settings/save', 'SettingController@save');

        // Account
        Route::get('/account', 'AccountController@account');
        Route::post('/account/update-details', 'AccountController@updateDetails');
        Route::post('/account/update-password', 'AccountController@updatePassword');

    });
});
