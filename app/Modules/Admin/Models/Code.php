<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;

class Code extends Model
{
    const TABLE = 'te_codes';
    const PRIMARY_KEY = 'code_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tc.code_id, tc.code, tu.email AS used_by, ';
        $sql .= 'CASE ';
        $sql .= 'WHEN tc.status = 0 THEN "void" ';
        $sql .= 'WHEN tc.used_by THEN "not_available" ';
        $sql .= 'WHEN tc.expiry_date > NOW() THEN tc.expiry_date ';
        $sql .= 'ELSE "expired" END AS status ';
        $sql .= 'FROM te_codes tc ';
        $sql .= 'LEFT JOIN te_users tu ON tu.user_id = tc.used_by ';
        $sql .= 'ORDER BY tc.status DESC, expiry_date DESC ';
        return DB::select($sql);
    }

    public static function insertUpdate($data)
    {
        for ($i=0; $i < $data['no_to_generate']; $i++) { 
            $code = array(
                'code' => strtoupper(App::strRandom(10)),
                'expiry_date' => date('Y-m-d H:i:s', time() + 86400));
            DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $code));
        }
        return true;
    }

    public static function void($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "UPDATE {$table} SET status = 0 WHERE code_id =? ";
        return DB::delete($sql, array($id));
    }
}