<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\Admin\Models\Site;

class Slider extends Model
{
    const TABLE = 'site_sliders';
    const PRIMARY_KEY = 'slider_id';

    public static function get($pageId)
    {
        $sql = 'SELECT * FROM site_sliders WHERE page_id = ?';
        $result = DB::select($sql, array($pageId));
        if (isset($result[0])) {
            return $result[0];
        }
        return null;
    }

    public static function getData($sliderId)
    {
        $sql = 'SELECT * FROM site_slider_datas WHERE slider_id = ?';
        return DB::select($sql, array($sliderId));
    }

    public static function insertUpdate($data)
    {
        $data['slider']['page_id'] = ($data['slider']['page_id'] ? $data['slider']['page_id'] : Sequence::getNextSequence());
        $data['slider']['slider_id'] = ($data['slider']['slider_id'] ? $data['slider']['slider_id'] : Sequence::getNextSequence());
        $sliderSQL = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data['slider']);
        DB::insert($sliderSQL);

        foreach ($data['slider_data'] as $key => $sd) {
            $sd['slider_id'] = $data['slider']['slider_id'];
            
            $sliderDataSQL = App::getInsertUpdateSQL('site_slider_datas', 'slider_data_id', $sd);
            DB::insert($sliderDataSQL);
        }
        return true;
    }

    public static function remove($id)
    {
        $sql = "DELETE FROM site_slider_datas WHERE slider_data_id = ?";
        return DB::delete($sql, array($id));
    }
}