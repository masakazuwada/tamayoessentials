<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\Admin\Models\Site;

class Content extends Model
{
    const TABLE = 'site_contents';
    const PRIMARY_KEY = 'content_id';

    const TYPE_CONTENT = 'TYPE_CONTENT';
    const TYPE_ADDRESS = 'TYPE_ADDRESS';
    const TYPE_PRIMARY_EMAIL = 'TYPE_PRIMARY_EMAIL';
    const TYPE_SUPPORT_EMAIL = 'TYPE_SUPPORT_EMAIL';
    const TYPE_CONTACT_NUMBERS = 'TYPE_CONTACT_NUMBERS';
    const TYPE_BUSINESS_HOURS = 'TYPE_BUSINESS_HOURS';
    const TYPE_TWITTER = 'TYPE_TWITTER';
    const TYPE_FACEBOOK = 'TYPE_FACEBOOK';
    const TYPE_YOUTUBE = 'TYPE_YOUTUBE';
    const TYPE_PASSUP = 'TYPE_PASSUP';

    public static function get($pageId, $type = self::TYPE_CONTENT)
    {
        $params = array();
        $sql = 'SELECT * FROM site_contents WHERE page_id = ? ';
        $params[] = $pageId;
        if ($type) {
            $sql .= 'AND type = ?';
            $params[] = $type;
        }
        $result = DB::select($sql, $params);
        return $result;
    }

    public static function getContentByType($type)
    {
        $sql = 'SELECT * FROM site_contents WHERE type = ?';
        $result = DB::select($sql, array($type));
        return $result;
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    public static function insertUpdateMultiple($data)
    {
        foreach ($data as $item) {
            $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $item);
            DB::insert($sql);
        }
        return true;
    }

    public static function removeLine($id)
    {
        $table = self::TABLE;
        $sql = "DELETE FROM {$table} WHERE content_id = ?";
        return DB::delete($sql, array($id));
    }
}