<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;

class Achievement extends Model
{
    const TABLE = 'te_achievements';
    const PRIMARY_KEY = 'achievement_id';

    public static function get($id = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        // If it has an id
        if (!is_null($id)) {
            $sql .= "WHERE {$pkey} = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY date_achieve ';
        return DB::select($sql);
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }
}