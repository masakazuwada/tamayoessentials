<?php

namespace App\Modules\Admin\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\App\Models\Person;
use DB;
use Hash;

class Account extends Model
{
    const TABLE = 'te_users';
    const PRIMARY_KEY = 'user_id';

    public static function updateDetails($data)
    {
        $userId = $data['user_id'];
        $person = array(
            'person_id' => $userId,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name']);
        DB::insert(App::getInsertUpdateSQL(Person::TABLE, Person::PRIMARY_KEY, $person));

        $user = array(
            'user_id' => $userId,
            'image' => $data['image']);
        DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $user));
    
        return true;
    }

    public static function updatePassword($data)
    {
        $user = array(
            'user_id' => $data['user_id'],
            'password' => Hash::make($data['password']));
        return DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $user));
    }
}