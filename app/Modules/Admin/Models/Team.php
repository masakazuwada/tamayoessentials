<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\Admin\Models\Site;

class Team extends Model
{
    const TABLE = 'site_teams';
    const PRIMARY_KEY = 'team_id';

    public static function get($pageId, $table = self::TABLE)
    {
        $sql = "SELECT * FROM {$table} WHERE page_id = ?";
        $result = DB::select($sql, array($pageId));
        return $result;
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    public static function insertUpdateMultiple($data)
    {
        foreach ($data as $item) {
            $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $item);
            DB::insert($sql);
        }
        return true;
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }
}