<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\Address;
use App\Modules\Admin\Models\User;
use App\Modules\Admin\Models\Setting;
use App\Mail\EmailVerification;
use App\User AS AppUser;
use Session;
use Hash;
use Mail;

class Member extends Model
{
    const TABLE = 'te_members';
    const PRIMARY_KEY = 'member_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tem.member_id, tem.image, tem.email, tem.status, tem.sponsor, tem.date_registered, tep.birthdate, tep.contact_no, tep.gender, tep.address_id, tead.country_id, tead.region_id, tead.city_id, tead.line1, tep.first_name, tep.last_name, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS full_name, ';
        $sql .= 'CONCAT(teps.first_name, " ", teps.last_name) AS sponsor_full_name, ';
        $sql .= 'CONCAT(tead.line1, "<br>", tere.name, ", ", teci.name, " ", teco.name) AS address ';
        $sql .= 'FROM te_members tem ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tem.person_id ';
        $sql .= 'LEFT JOIN app_persons teps ON teps.person_id = tem.sponsor ';
        $sql .= 'LEFT JOIN app_addresses tead ON tead.address_id = tep.address_id ';
        $sql .= 'LEFT JOIN app_countries teco ON teco.id = tead.country_id ';
        $sql .= 'LEFT JOIN app_regions tere ON tere.id = tead.region_id ';
        $sql .= 'LEFT JOIN app_cities teci ON teci.id = tead.city_id ';
        // If it has an id
        if (!is_null($id)) {
            $sql .= 'WHERE tem.member_id = ? ';
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY tem.member_id ';
        return DB::select($sql);
    }

    public static function getForDropdown($data)
    {
        $sql = 'SELECT tem.member_id, CONCAT(tep.first_name, " ", tep.last_name) AS full_name ';
        $sql .= 'FROM te_members tem ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tem.person_id ';
        return DB::select($sql);
    }

    public static function insertUpdate($data)
    {
        $memberId = $data['member_id'];
        if (is_null($data['member_id'])) {
            $memberId = Setting::getValue('id_prefix') . str_pad(Sequence::getNextSequence('MBR'), 5, '0', STR_PAD_LEFT);
        }

        $addressId = $data['address_id'];
        if (is_null($data['address_id'])) {
            $addressId = Sequence::getNextSequence();
        }
        $person = array(
            'person_id' => $memberId,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'address_id' => $addressId,
            'birthdate' => $data['birthdate'],
            'contact_no' => $data['contact_no'],
            'gender' => $data['gender']);
        DB::insert(App::getInsertUpdateSQL(Person::TABLE, Person::PRIMARY_KEY, $person));


        $member = array(
            'member_id' => $memberId,
            'person_id' => $memberId,
            'email' => $data['email'],
            'image' => $data['image'],
            'date_registered' => $data['date_registered'],
            'verified' => true,
            'status' => $data['status']);
        
        if (is_null($data['member_id'])) {
            $member['sponsor'] = $data['sponsor'];
        }
        if (!($memberId && is_null($data['password']))) {
            $member['password'] = Hash::make($data['password']);
        }
        DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $member));

        $address = array(
            'address_id' => $addressId,
            'line1' => $data['line1'],
            'country_id' => $data['country_id'],
            'region_id' => $data['region_id'],
            'city_id' => $data['city_id']);
        DB::insert(App::getInsertUpdateSQL(Address::TABLE, Address::PRIMARY_KEY, $address));

        return $memberId;
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }

    public static function resetPassword($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "UPDATE {$table} SET password = ? WHERE {$pkey} = ?";
        return DB::update($sql, array(Hash::make(Setting::getValue('default_password')), $id));
    }
}