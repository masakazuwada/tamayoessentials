<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;

class Site extends Model
{
    const TABLE = 'site_pages';
    const PRIMARY_KEY = 'page_id';

    const MAIN = 0;


    public static function get($id = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        // If it has an id
        if (!is_null($id)) {
            $sql .= "WHERE {$pkey} = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY order_no, parent ';
        return DB::select($sql);
    }

    public static function buildNavigation($data = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        $sql .= 'WHERE status = ? ';
        if (!is_null($data)) {
            foreach ($data as $key => $value) {
                $strVal = '"' . $value . '"';
                $values[] = "`{$key}` = $strVal";
            }
            $sql .= 'AND ' . implode(', ', $values);
        }
        $sql .= ' ORDER BY order_no ';

        return DB::select($sql, array(true));
    }

    public static function getIdBySegment($segment)
    {
        $sql = 'SELECT page_id FROM site_pages WHERE segment = ?';
        $result = DB::select($sql, array($segment));
        if (isset($result[0])) {
            return $result[0]->page_id;
        }
        return false;
    }

    public static function getLevelsForDropdown($level = null)
    {
        $sql = 'SELECT page_id, name FROM site_pages ';
        $sql .= 'WHERE parent >= ? ';
        $sql .= 'ORDER BY name';
        return DB::select($sql, array($level));
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    // public static function reorder($data)
    // {
    //     $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
    //     return DB::insert($sql);
    // }
}