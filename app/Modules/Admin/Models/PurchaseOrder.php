<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;

class PurchaseOrder extends Model
{
    const TABLE = 'te_purchase_orders';
    const PRIMARY_KEY = 'purchase_order_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tepo.purchase_order_id, GROUP_CONCAT(tepro.product SEPARATOR "<br>") AS products, tepo.total, tepo.customer_type, tepo.customer, tepo.date_purchased, tepo.details, tepo.timestamp, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS encoder ';
        $sql .= 'FROM te_purchase_orders tepo ';
        $sql .= 'LEFT JOIN te_purchase_order_lines tepol ON tepol.purchase_order_id = tepo.purchase_order_id ';
        $sql .= 'LEFT JOIN te_products tepro ON tepro.product_id = tepol.product_id ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tepo.encoder ';
        // If it has an id
        if (!is_null($id)) {
            $sql .= 'WHERE tepo.purchase_order_id = ? ';
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'GROUP BY tepo.purchase_order_id ';
        $sql .= 'ORDER BY tepo.date_purchased DESC, tepo.timestamp DESC';
        return DB::select($sql);
    }

    public static function getLines($purchaseOrderId)
    {
        $sql = 'SELECT tepol.purchase_order_line_id, tepro.product, tepro.image, tepol.quantity, tepol.discount, tepol.price, tepol.sub_total ';
        $sql .= 'FROM te_purchase_order_lines tepol ';
        $sql .= 'LEFT JOIN te_products tepro ON tepro.product_id = tepol.product_id ';
        $sql .= 'WHERE tepol.purchase_order_id = ? ';
        $sql .= 'GROUP BY tepol.purchase_order_line_id ';
        $sql .= 'ORDER BY tepol.purchase_order_line_id ASC ';

        return DB::select($sql, array($purchaseOrderId));
    }

    public static function getLinesForDetails($id)
    {
        $sql = 'SELECT tepol.purchase_order_line_id, tepol.product_id, tepol.purchase_order_id, tepol.quantity, tepol.discount, tepol.price ';
        $sql .= 'FROM te_purchase_order_lines tepol ';
        $sql .= 'LEFT JOIN te_purchase_orders tepo ON tepo.purchase_order_id = tepol.purchase_order_id ';
        $sql .= 'WHERE tepol.purchase_order_id = ? ';
        $sql .= 'ORDER BY tepol.purchase_order_line_id ';
        return DB::select($sql, array($id));
    }

    public static function insertUpdate($data)
    {
        $purchaseOrderData = array();
        $total = 0;
        $purchaseOrderId = (isset($data['purchase_order_id']) ? $data['purchase_order_id'] : Sequence::getNextSequence());

        if (isset($data['purchase_order_id']) && $data['purchase_order_id']) {
            self::removeLine($data['product'], $purchaseOrderId);
        }

        if ($data['product'] && $data['quantity'] && $data['discount'] && $data['price']) {

            foreach ($data['product'] as $key => $product) {
                $collectionLineSQL = '';
                $strVal = '';
                $values = array();

                $subTotal = $data['price'][$key] * $data['quantity'][$key];
                $collectionLineSQL = 'REPLACE INTO te_purchase_order_lines SET ';
                $purchaseOrderLine = [
                    'purchase_order_line_id' => (isset($data['purchase_order_line_id'][$key]) ? $data['purchase_order_line_id'][$key] : Sequence::getNextSequence()),
                    'purchase_order_id' => $purchaseOrderId,
                    'product_id' => $product,
                    'quantity' => $data['quantity'][$key],
                    'discount' => $data['discount'][$key],
                    'price' => $data['price'][$key],
                    'sub_total' => floatval($subTotal - ($subTotal * ($data['discount'][$key]/ 100)))
                ];

                foreach ($purchaseOrderLine as $key2 => $value) {
                    if ($value && $key2 != '_token') {
                        $strVal = '"' . $value . '"';
                        $values[] = "`{$key2}` = $strVal";
                    }
                }
                $collectionLineSQL .= implode(', ', $values);
                DB::insert($collectionLineSQL);

                $total += $purchaseOrderLine['sub_total'];
            }

            $purchaseOrderData = [
                'purchase_order_id' => $purchaseOrderId,
                'customer_type' => $data['customer_type'],
                'customer' => $data['customer'],
                'total' => $total,
                'date_purchased' => $data['date_purchased'],
                'details' => $data['details']];

            if (!$data['purchase_order_id']) {
                $purchaseOrderData['encoder'] = Auth()->guard('admin')->id();
            }

            DB::beginTransaction();
            try {
                DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $purchaseOrderData));
                DB::commit();
                $result = true;
            }
            catch (Exception $e) {
                DB::rollback();
                throw ($e);
                $result = false;
            }
        }

        return true;
    }

    public static function remove($id)
    {
        DB::beginTransaction();
        try {
            DB::delete('DELETE FROM te_purchase_orders WHERE purchase_order_id = ?', array($id));
            DB::delete('DELETE FROM te_purchase_order_lines WHERE purchase_order_id = ?', array($id));
            DB::commit();
            $result = true;
        }
        catch (Exception $e) {
            DB::rollback();
            throw ($e);
            $result = false;
        }
        return $result;
    }

    public static function removeLine($collectionLines, $purchaseOrderId)
    {
        $sql = 'DELETE FROM te_purchase_order_lines ';
        $sql .= 'WHERE purchase_order_id = ? AND purchase_order_line_id NOT IN ("'. implode('", "', $collectionLines) .' ")';
        return DB::delete($sql, array($purchaseOrderId));
    }

    public static function getPendingCollectionAmount($table = self::TABLE)
    {
        $sql = "SELECT COALESCE(SUM(total), 0) AS total_pending FROM {$table} WHERE status = 0";
        $result = DB::select($sql);
        if (isset($result[0])) {
            return $result[0]->total_pending;
        }
        return 0;
    }
}