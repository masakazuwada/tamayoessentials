<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\Address;
use App\Modules\Admin\Models\Setting;
use App\Mail\EmailVerification;
use Session;
use Hash;
use Mail;
use App\User as AppUser;


class User extends Model
{
    const TABLE = 'te_users';
    const PRIMARY_KEY = 'user_id';

    const TYPE_ADMIN = 1;
    const TYPE_USER = 2;

    public static function get($id = null)
    {
        $sql = 'SELECT teu.user_id, teu.image, teu.email, tep.first_name, tep.last_name, teu.type, teu.status, ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS full_name ';
        $sql .= 'FROM te_users teu ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = teu.person_id ';
        // If it has an id
        if (!is_null($id)) {
            $sql .= 'WHERE teu.user_id = ? ';
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY teu.user_id ';
        return DB::select($sql);
    }

    public static function insertUpdate($data)
    {

        $userId = $data['user_id'];
        if (is_null($data['user_id'])) {
            $userId = Sequence::getNextSequence();
        }
        $person = array(
            'person_id' => $userId,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name']);
        DB::insert(App::getInsertUpdateSQL(Person::TABLE, Person::PRIMARY_KEY, $person));

        $user = array(
            'user_id' => $userId,
            'person_id' => $userId,
            'image' => $data['image'],
            'type' => $data['type'],
            'status' => $data['status'],
            'email' => $data['email']);

        if (!($userId && is_null($data['password']))) {
            $user['password'] = Hash::make($data['password']);
        }
        DB::insert(App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $user));
    
        return true;
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }

    public static function resetPassword($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "UPDATE {$table} SET password = ? WHERE {$pkey} = ?";
        return DB::update($sql, array(Hash::make(Setting::getValue('default_password')), $id));
    }
}