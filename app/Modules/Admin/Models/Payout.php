<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\Member\Models\Earning;

class Payout extends Model
{
    const TABLE = 'te_payouts';
    const PRIMARY_KEY = 'payout_id';

    public static function get($id = null)
    {
        $sql = 'SELECT tepy.payout_id, tepy.member_id, tepy.amount, tepy.type, tepy.request_date, tepy.status, tepy.reviewed_on, tepy.remarks, tepy.timestamp,  ';
        $sql .= 'CONCAT(tep.first_name, " ", tep.last_name) AS member, ';
        $sql .= 'CONCAT(tepr.first_name, " ", tepr.last_name) AS reviewed_by ';
        $sql .= 'FROM te_payouts tepy ';
        $sql .= 'LEFT JOIN app_persons tep ON tep.person_id = tepy.member_id ';
        $sql .= 'LEFT JOIN app_persons tepr ON tepr.person_id = tepy.reviewer ';
        if (!is_null($id)) {
            $sql .= "WHERE payout_id = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'GROUP BY payout_id ';
        $sql .= 'ORDER BY status, tepy.payout_id DESC';
        return DB::select($sql);
    }

    public static function getTotalPending()
    {
        $sql = 'SELECT count(payout_id) AS count FROM te_payouts WHERE status = 0';
        return DB::select($sql)[0]->count;
    }

    public static function insertUpdate($data)
    {
        if ($data['status'] == 1) {
            $payoutData = Payout::get($data['payout_id']);
            $payoutAmount = $payoutData->amount;
            $type = $payoutData->type == 1 ? 'amount_balance' : 'points_balance';
            $earnings = Earning::getBalancesByMember(['field' => $type, 'member' => $payoutData->member_id]);
            if (!empty($earnings)) {
                foreach ($earnings as $earning) {
                    if ($payoutAmount < 1) { break; }
                    if ($earning->$type >= $payoutAmount) {
                        DB::update("UPDATE te_earnings SET {$type} = {$type} - {$payoutAmount} WHERE earning_id = {$earning->earning_id}");
                        $payoutAmount -= $payoutAmount;
                        break;
                    } else if ($earning->$type < $payoutAmount) {
                        DB::update("UPDATE te_earnings SET {$type} = {$type} - {$earning->$type} WHERE earning_id = {$earning->earning_id}");
                        $payoutAmount -= $earning->$type;
                    }
                }
            }
        }
        $data['reviewer'] = Auth()->guard('admin')->user()->user_id;
        $data['reviewed_on'] = date('Y-m-d H:i:s');

        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }
}