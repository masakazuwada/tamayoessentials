<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\Admin\Models\Site;

class Setting extends Model
{
    const TABLE = 'te_settings';
    const PRIMARY_KEY = 'setting_id';

    const TYPE_COMPANY = 1;

    public static function get($type)
    {
        $sql = 'SELECT * FROM te_settings WHERE type = ?';
        return DB::select($sql, array($type));
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    public static function getValue($key)
    {
        $sql = 'SELECT value FROM te_settings WHERE `key` = ?';
        $result = DB::select($sql, array($key));
        if (isset($result[0])) {
            return $result[0]->value;
        }
        return false;
    }
}