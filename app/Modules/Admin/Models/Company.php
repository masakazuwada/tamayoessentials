<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use App\Modules\Admin\Models\Site;

class Company extends Model
{
    const TABLE = 'site_companies';
    const PRIMARY_KEY = 'company_id';

    public static function get($pageId)
    {
        $sql = 'SELECT * FROM site_companies WHERE page_id = ?';
        $result = DB::select($sql, array($pageId));
        if (isset($result[0])) {
            return $result[0];
        }
        return null;
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }
}