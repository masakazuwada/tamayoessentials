<?php

namespace App\Modules\Admin\Models;
use DB;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;

class Product extends Model
{
    const TABLE = 'te_products';
    const PRIMARY_KEY = 'product_id';

    public static function get($id = null, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "SELECT * FROM {$table} ";
        // If it has an id
        if (!is_null($id)) {
            $sql .= "WHERE {$pkey} = ? ";
            $result = DB::select($sql, array($id));
            if (isset($result[0])) {
                return $result[0];
            }
        }
        $sql .= 'ORDER BY product ';
        return DB::select($sql);
    }

    public static function getForFront($table = self::TABLE)
    {
        $sql = "SELECT * FROM {$table} ";
        $sql .= "WHERE advertised = 1 ";
        $sql .= 'ORDER BY product ';
        return DB::select($sql);
    }


    public static function getForDropdown($data)
    {
        $sql = 'SELECT product_id, code, product FROM te_products ORDER BY product';
        return DB::select($sql);
    }

    public static function getDetailsForSelect($id)
    {
        $sql = 'SELECT product_id, selling_price FROM te_products ';
        $sql .= 'WHERE product_id = ?';
        $result = DB::select($sql, array($id));
        if (isset($result[0])) {
            return $result[0];
        }
        return false;
    }

    public static function insertUpdate($data)
    {
        $sql = App::getInsertUpdateSQL(self::TABLE, self::PRIMARY_KEY, $data);
        return DB::insert($sql);
    }

    public static function remove($id, $table = self::TABLE, $pkey = self::PRIMARY_KEY)
    {
        $sql = "DELETE FROM {$table} WHERE {$pkey} = ?";
        return DB::delete($sql, array($id));
    }
}