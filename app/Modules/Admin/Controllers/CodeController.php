<?php 
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Html;
use App\Modules\App\Models\App;
use App\Modules\Admin\Models\Code;
use Session;

class CodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['login', 'register', 'verify']]);
    }

    public function viewList()
    {
        $data = [
            'data_bind' => 'code_generate',
        ];
        return view('Admin::codes', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Code::get();
        }
        return json_encode(['data' => $data]);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Code::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function void(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->code_id) && $request->ajax()) {
            if (Code::void($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}
