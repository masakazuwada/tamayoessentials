<?php 
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\User;
use Illuminate\Http\Request;
use Illuminate\Html;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\App;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['register', 'verify']]);
    }

    public function viewList()
    {
        $data = [
            'data_bind' => 'user_add_edit',
        ];
        return view('Admin::users', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = User::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->user_id) && $request->ajax()) {
            $data = User::get($id);
        }
        return json_encode($data);
    }


    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/admin/images/users/',
                    'dir' => '/assets/admin/images/users/'));
            }
            if (User::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->user_id) && $request->ajax()) {
            if (User::remove($id) && Person::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function resetPassword(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->user_id) && $request->ajax()) {
            if (User::resetPassword($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}
