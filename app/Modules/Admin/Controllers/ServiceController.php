<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;
use App\Modules\Admin\Models\Slider;
use App\Modules\Admin\Models\Company;
use App\Modules\Admin\Models\Content;


class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function manage(Request $request)
    {
        $sliderData = array(
            'slider' => $slider = Slider::get(Site::getIdBySegment('services')),
            'slider_data' => (isset($slider) ? Slider::getData($slider->slider_id) : ''));

        $contentData = Content::get(Site::getIdBySegment('services'));
        return view('Admin::services', array('slider' => $sliderData, 'contents' => $contentData));
    }

    public function saveContent(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            $contents = array();
            
            foreach ($request->content_id as $key => $ci) {
                $contentData = array();
                $contentData['page_id'] = $request->page_id;
                $contentData['content_id'] = $ci;
                if (isset($request->file('image')[$key])) {
                    $contentData['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/services/data/',
                        'dir' => '/assets/front/images/services/data/'));
                }
                $contentData['description']= $request->description[$key];
                $contents[] = $contentData;
            }

            if (Content::insertUpdateMultiple($contents)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function saveSlider(Request $request)
    {
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            
            $slider = array(
                'slider_id' => $request->slider_id,
                'page_id' => $request->page_id,
                'details' => $request->details,
            );

            $sliderDatas = array();
            
            foreach ($request->slider_data_id as $key => $sdi) {
                $sliderData = array();
                $sliderData['slider_data_id'] = $sdi;
                if (isset($request->file('image')[$key])) {
                    $sliderData['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/services/slider/',
                        'dir' => '/assets/front/images/services/slider/'));
                }
                $sliderData['description']= $request->description[$key];
                $sliderDatas[] = $sliderData;
            }

            $ajaxData = array(
                'slider' => $slider,
                'slider_data' => $sliderDatas
            );

            if (Slider::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeSlider(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->slider_data_id) && $request->ajax()) {

            if (Slider::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeLine(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->content_id) && $request->ajax()) {

            if (Content::removeLine($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}