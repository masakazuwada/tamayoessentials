<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Setting;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function company()
    {
        return view('Admin::setting_company');
    }
    
    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if ($request->ajax()) {
            if (Setting::insertUpdate($request->all())) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getCompanyItemsForDropdown(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Setting::get(Setting::TYPE_COMPANY);
        }
        return json_encode($data);
    }

    public function getCompany(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Setting::get(Setting::TYPE_COMPANY);
        }
        return json_encode(['data' => $data]);
    }
}