<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;
use App\Modules\Admin\Models\Slider;
use App\Modules\Admin\Models\Company;
use App\Modules\Admin\Models\Content;


class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function manage(Request $request)
    {
        $sliderData = array(
            'slider' => $slider = Slider::get(Site::getIdBySegment('home')),
            'slider_data' => Slider::getData($slider->slider_id));
        $companyData = Company::get(Site::getIdBySegment('home'));
        $contentData = Content::get(Site::getIdBySegment('home'));
        $passupData = Content::get(Site::getIdBySegment('home'), Content::TYPE_PASSUP);

        return view('Admin::index', array('slider' => $sliderData, 'company' => $companyData, 'contents' => $contentData, 'passup' => $passupData));
    }

    public function saveSlider(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if ($request->ajax()) {
            if ($requestImg = $request->file('slider_bg_image')) {
                $sliderBgImage = App::processImageUpload($requestImg, array(
                    'name' => 'slider_bg_image',
                    'rdir' => '/public/assets/front/images/home/slider/',
                    'dir' => '/assets/front/images/home/slider/'));
            }

            $slider = array(
                'slider_id' => $request->slider_id,
                'page_id' => $request->page_id,
            );

            if (isset($sliderBgImage)) {
                $slider['background_image'] = $sliderBgImage;
            }

            $sliderDatas = array();
            
            foreach ($request->slider_data_id as $key => $sdi) {
                $sliderData = array();
                $sliderData['slider_data_id'] = $sdi;
                if (isset($request->file('slider_data_image')[$key])) {
                    $sliderData['image'] = App::processImageUpload($request->file('slider_data_image')[$key], array(
                        'name' => 'slider_data_image',
                        'name' => 'slider_data_image',
                        'rdir' => '/public/assets/front/images/home/slider/data/',
                        'dir' => '/assets/front/images/home/slider/data/'));
                }
                $sliderData['description']= $request->slider_data_desc[$key];
                $sliderData['title']= $request->slider_data_title[$key];
                $sliderDatas[] = $sliderData;
            }
            $ajaxData = array(
                'slider' => $slider,
                'slider_data' => $sliderDatas);

            if (Slider::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function saveCompany(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Company::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function saveContent(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            $contents = array();

            foreach ($request->content_id as $key => $ci) {
                $contentData = array();
                $contentData['page_id'] = $request->page_id;
                $contentData['content_id'] = $ci;
                if (isset($request->file('image')[$key])) {
                    $contentData['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/home/content/',
                        'dir' => '/assets/front/images/home/content/'));
                }
                $contentData['description']= $request->description[$key];
                $contents[] = $contentData;
            }

            if (Content::insertUpdateMultiple($contents)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function savePassup(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            $contents = array();

            foreach ($request->content_id as $key => $ci) {
                $contentData = array();
                $contentData['page_id'] = $request->page_id;
                $contentData['content_id'] = $ci;
                $contentData['type'] = Content::TYPE_PASSUP;
                if (isset($request->file('image')[$key])) {
                    $contentData['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/home/content/',
                        'dir' => '/assets/front/images/home/content/'));
                }
                $contents[] = $contentData;
            }

            if (Content::insertUpdateMultiple($contents)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeSlider(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->slider_data_id) && $request->ajax()) {

            if (Slider::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeContent(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->content_id) && $request->ajax()) {

            if (Content::removeLine($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}