<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Payout;


class PayoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function manage(Request $request)
    {
        return view('Admin::payouts');
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Payout::get();
        }
        return json_encode(['data' => $data]);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Payout::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}
