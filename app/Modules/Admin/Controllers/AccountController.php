<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Account;
use App\Modules\Admin\Models\User;
use App\Modules\App\Models\App;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function account()
    {
        $account = User::get(auth()->guard('admin')->user()->user_id);
        return view('Admin::account', array('account' => $account));
    }
    
    public function updateDetails(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
             if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/admin/images/users/',
                    'dir' => '/assets/admin/images/users/'));
            }
            if (Account::updateDetails($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function updatePassword(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if ($request->ajax()) {
            if (Account::updatePassword($request->all())) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}