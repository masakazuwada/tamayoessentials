<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;
use App\Modules\Admin\Models\Slider;
use App\Modules\Admin\Models\Content;
use App\Modules\Admin\Models\Product;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getDetails']]);
    }

    public function manage(Request $request)
    {
        $data = [
            'data_bind' => 'product_add_edit',
        ];
        return view('Admin::products', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Product::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getForDropdown(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Product::getForDropdown($request->all());
        }
        return json_encode($data);
    }

    public function getDetailsForSelect(Request $request)
    {
        $data = array();
        if (($id = $request->product_id) && $request->ajax()) {
            $data = Product::getDetailsForSelect($id);
        }
        return json_encode($data);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/front/images/products/',
                    'dir' => '/assets/front/images/products/'));
            }
            $ajaxData['code'] = strtoupper($ajaxData['code']);
            $ajaxData['advertised'] = $request->advertised ? 1 : 0;
            if (Product::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->product_id) && $request->ajax()) {
            $data = Product::get($id);
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->product_id) && $request->ajax()) {
            if (Product::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

}
