<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;
use App\Modules\Admin\Models\Slider;
use App\Modules\Admin\Models\Company;
use App\Modules\Admin\Models\Content;


class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function manage(Request $request)
    {
        $contentData = Content::get(Site::getIdBySegment('contacts'), false);
        $contents = array();
        foreach ($contentData as $content) {
            $cd = array();
            $cd['content_id'] = $content->content_id;
            $cd['description'] = $content->description;
            $contents[$content->type] = $cd;
        }
        return view('Admin::contacts', array('content' => $contents));
    }

    public function saveContent(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            $contents = array(
                'address' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_ADDRESS,
                    'content_id' => $request->address_content_id,
                    'description' => $request->address),
                'business' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_BUSINESS_HOURS,
                    'content_id' => $request->business_content_id,
                    'description' => $request->business_hours),
                'contact' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_CONTACT_NUMBERS,
                    'content_id' => $request->contact_content_id,
                    'description' => $request->contact_nos),
                'primary_email' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_PRIMARY_EMAIL,
                    'content_id' => $request->pri_email_content_id,
                    'description' => $request->pri_email),
                'support_email' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_SUPPORT_EMAIL,
                    'content_id' => $request->sup_email_content_id,
                    'description' => $request->sup_email),
                'twitter' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_TWITTER,
                    'content_id' => $request->twitter_content_id,
                    'description' => preg_match("~^(?:f|ht)tps?://~i", $request->twitter) ?
                        $request->twitter : (
                                $request->twitter ?
                                    'http://' . $request->twitter :
                                        $request->twitter)),
                'facebook' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_FACEBOOK,
                    'content_id' => $request->facebook_content_id,
                    'description' => preg_match("~^(?:f|ht)tps?://~i", $request->facebook) ?
                        $request->facebook : (
                                $request->facebook ?
                                    'http://' . $request->facebook :
                                        $request->facebook)),
                'youtube' => array(
                    'page_id' => $request->page_id,
                    'type' => Content::TYPE_YOUTUBE,
                    'content_id' => $request->youtube_content_id,
                    'description' => preg_match("~^(?:f|ht)tps?://~i", $request->youtube) ?
                        $request->youtube : (
                                $request->youtube ?
                                    'http://' . $request->youtube :
                                        $request->youtube)),
            );

            if (Content::insertUpdateMultiple($contents)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}