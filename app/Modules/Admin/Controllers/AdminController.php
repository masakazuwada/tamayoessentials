<?php
use Illuminate\Http\Request;
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\App\Models\Person;
use Session;
use Auth;
use Redirect;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::guard('admin')->check() || Auth::guard('admin')->viaRemember()) {
            return redirect::to('/administration/transactions/purchase-orders');
        }
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'email' => 'required',
                'password' => 'required',
                ]);

            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,
                );

            if (Auth::guard('admin')->validate($userdata)) {
                if (Auth::guard('admin')->attempt($userdata, $request->remember)) {
                    Person::setSession(Auth::guard('admin')->user()->person_id);
                    Session::put('guard', 'admin');
                    return redirect()->intended('administration/transactions/purchase-orders');
                }
            }  else {
                Session::flash('error', 'Incorrect Email or Password'); 
            }
            return redirect::to('/administration');
        }
        return view('Admin::login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        Session::flush();
        return redirect::to('/administration');
    }
}