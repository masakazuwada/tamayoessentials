<?php 
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Html;
use App\Modules\Admin\Models\Member;
use App\Modules\Admin\Models\User;
use App\Modules\Member\Models\Earning;
use App\Modules\App\Models\Person;
use App\Modules\App\Models\App;
use App\Modules\App\Models\Sequence;
use Session;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function viewList()
    {
        $data = [
            'data_bind' => 'member_add_edit',
        ];
        return view('Admin::members', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Member::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->member_id) && $request->ajax()) {
            $data = Member::get($id);
        }
        return json_encode($data);
    }

    public function getForDropdown(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Member::getForDropdown($request->all());
        }
        return json_encode($data);
    }

    public function save(Request $request)
    {

        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/member/images/members/',
                    'dir' => '/assets/member/images/members/'));
            }
            $sponsorOld = App::getFieldValue(array('table' => 'te_members', 'pkey' => 'member_id', 'field' => 'sponsor', 'val' => $request->member_id));
            if ($applicant = Member::insertUpdate($ajaxData)) {
                if ($request->sponsor && $request->sponsor != $sponsorOld) {
                    Earning::removeByApplicant($applicant);
                    Earning::insert(array(
                        'member_id' => $request->member_id,
                        'applicant' => $applicant,
                        'sponsor' => $request->sponsor));
                }
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->member_id) && $request->ajax()) {
            if (Member::remove($id) && Person::remove($id)) {
                Earning::removeByApplicant($id);
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function resetPassword(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->member_id) && $request->ajax()) {
            if (Member::resetPassword($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}
