<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\PurchaseOrder;


class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function manage(Request $request)
    {
        $data = [
            'data_bind' => 'purchase_order_add_edit',
        ];
        return view('Admin::purchase_orders', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = PurchaseOrder::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getLines(Request $request)
    {
        $data = array();
        if (($id = $request->id) && $request->ajax()) {
            $data = PurchaseOrder::getLines($id);
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->purchase_order_id) && $request->ajax()) {
            $data = PurchaseOrder::get($id);
            $data->purchase_order_lines = PurchaseOrder::getLinesForDetails($id);
        }
        return json_encode($data);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (PurchaseOrder::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->purchase_order_id) && $request->ajax()) {
            if (PurchaseOrder::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

}
