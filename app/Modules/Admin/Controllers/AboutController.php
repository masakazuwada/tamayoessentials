<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;
use App\Modules\Admin\Models\Slider;
use App\Modules\Admin\Models\Content;
use App\Modules\Admin\Models\Team;


class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function manage(Request $request)
    {
        $sliderData = array(
            'slider' => $slider = Slider::get(Site::getIdBySegment('about-us')),
            'slider_data' => (isset($slider) ? Slider::getData($slider->slider_id) : ''));
        $teams = Team::get(Site::getIdBySegment('about-us'));
        return view('Admin::about_us', array('slider' => $sliderData, 'teams' => $teams));
    }

    public function saveSlider(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            
            $slider = array(
                'slider_id' => $request->slider_id,
                'page_id' => $request->page_id,
                'details' => $request->details,
            );

            $sliderDatas = array();
            
            foreach ($request->slider_data_id as $key => $sdi) {
                $sliderData = array();
                $sliderData['slider_data_id'] = $sdi;
                if (isset($request->file('image')[$key])) {
                    $sliderData['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/about-us/slider/',
                        'dir' => '/assets/front/images/about-us/slider/'));
                }
                $sliderData['description']= $request->description[$key];
                $sliderDatas[] = $sliderData;
            }

            $ajaxData = array(
                'slider' => $slider,
                'slider_data' => $sliderDatas
            );

            if (Slider::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function saveTeam(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($request->all()) && $request->ajax()) {
            $teams = array();
            
            foreach ($request->team_id as $key => $ti) {
                $team = array();
                $team['page_id'] = $request->page_id;
                $team['team_id'] = $ti;
                if (isset($request->file('image')[$key])) {
                    $team['image'] = App::processImageUpload($request->file('image')[$key], array(
                        'name' => 'image',
                        'rdir' => '/public/assets/front/images/about-us/team/',
                        'dir' => '/assets/front/images/about-us/team/'));
                }
                $team['fullname']= $request->fullname[$key];
                $team['position']= $request->position[$key];
                $team['twitter']= preg_match("~^(?:f|ht)tps?://~i", $request->twitter[$key]) ?
                        $request->twitter[$key] : (
                                $request->twitter[$key] ?
                                    'http://' . $request->twitter[$key] :
                                        $request->twitter[$key]);
                $team['facebook']= preg_match("~^(?:f|ht)tps?://~i", $request->facebook[$key]) ?
                        $request->facebook[$key] : (
                                $request->facebook[$key] ?
                                    'http://' . $request->facebook[$key] :
                                        $request->facebook[$key]);
                $team['google_plus']= preg_match("~^(?:f|ht)tps?://~i", $request->google_plus[$key]) ?
                        $request->google_plus[$key] : (
                                $request->google_plus[$key] ?
                                    'http://' . $request->google_plus[$key] :
                                        $request->google_plus[$key]);
                $teams[] = $team;
            }

            if (Team::insertUpdateMultiple($teams)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeSlider(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->slider_data_id) && $request->ajax()) {

            if (Slider::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function removeTeam(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->team_id) && $request->ajax()) {

            if (Team::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}