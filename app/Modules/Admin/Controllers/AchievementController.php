<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Achievement;


class AchievementController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getDetails']]);
    }

    public function manage(Request $request)
    {
        $data = [
            'data_bind' => 'achievement_add_edit',
        ];
        return view('Admin::achievements', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Achievement::get();
        }
        return json_encode(['data' => $data]);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['image'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/front/images/achievements/',
                    'dir' => '/assets/front/images/achievements/'));
            }
            if (Achievement::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->achievement_id) && $request->ajax()) {
            $data = Achievement::get($id);
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->achievement_id) && $request->ajax()) {
            if (Achievement::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}