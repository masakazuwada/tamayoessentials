<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\App\Models\App;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Gallery;


class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function manage(Request $request)
    {
        $data = [
            'data_bind' => 'gallery_add_edit',
        ];
        return view('Admin::gallery', $data);
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Gallery::get();
        }
        return json_encode(['data' => $data]);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if($request->file('image')) {
                $ajaxData['content'] = App::processImageUpload($request->file('image'), array(
                    'name' => 'image',
                    'rdir' => '/public/assets/front/images/Gallerys/',
                    'dir' => '/assets/front/images/Gallerys/'));
                unset($ajaxData['image']);
            }
            if (Gallery::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->gallery_id) && $request->ajax()) {
            $data = Gallery::get($id);
        }
        return json_encode($data);
    }

    public function remove(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($id = $request->gallery_id) && $request->ajax()) {
            if (Gallery::remove($id)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }
}