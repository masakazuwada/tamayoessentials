<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Site;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function viewList()
    {
        return view('Admin::pages');
    }

    public function get(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Site::get();
        }
        return json_encode(['data' => $data]);
    }

    public function getDetails(Request $request)
    {
        $data = array();
        if (($id = $request->page_id) && $request->ajax()) {
            $data = Site::get($id);
        }
        return json_encode($data);
    }

    public function save(Request $request)
    {
        $data = array();
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Site::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function reorder(Request $request)
    {
        $data['status'] = false;
        if (($ajaxData = $request->all()) && $request->ajax()) {
            if (Site::insertUpdate($ajaxData)) {
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    public function getLevelsForDropdown(Request $request)
    {
        $data = array();
        if ($request->ajax()) {
            $data = Site::getLevelsForDropdown($request->level);
        }

        return json_encode($data);
    }

}