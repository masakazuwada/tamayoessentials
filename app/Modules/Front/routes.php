<?php

use Illuminate\Http\Request;

Route::group(array('namespace' => 'App\Modules\Front\Controllers'), function() {
    Route::group(['middleware' => ['web']], function() {

        Route::get('/', 'FrontController@home');
        Route::get('/home', 'FrontController@home');
        Route::get('/achievements', 'FrontController@achievements');
        Route::get('/gallery', 'FrontController@gallery');
        Route::get('/products', 'FrontController@products');
        Route::get('/about-us', 'FrontController@aboutUs');
        Route::get('/contacts', 'FrontController@contacts');
        Route::get('/services', 'FrontController@services');
        Route::get('/marketing-plan', 'FrontController@marketingPlan');
        
        Route::post('/contacts/send-message', 'FrontController@sendMessage');
        Route::get('/registration', 'FrontController@registration');
    });
});
