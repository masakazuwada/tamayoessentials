<div class="modal-content">
  <div class="modal-body">
    <div class="text-center" id="loader">
      <img src="{{ URL::asset('assets/app/images/loading.gif') }}"/>
    </div>
    <div>
      <span id="image"></span>
    </div>
  </div>
</div>
