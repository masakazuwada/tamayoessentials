<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Registration</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <form method="POST">
          {!! csrf_field() !!}
          {{-- <h5>Profile Picture</h5> --}}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <div class="photo-upload text-center">
                  <label for="photo">
                    <div class="image rou">
                      <img class="round" src="{{ URL::asset('assets/app/images/user-male.png') }}" title="Upload Image" id="photo_preview">
                      <div class="after">
                        <i class="fa fa-plus-circle" id="upload_icon" title="Upload Product Image"></i>
                      </div>
                    </div>
                  </label>
                  <input type="hidden" name="image" id="image" value="/assets/app/images/user-male.png">
                  <input id="photo" type="file" name="image">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <h5>Invitation Code</h5>
                <input type="text" class="form-control" name="code" placeholder="Code">
              </div>
              <div class="form-group">
                <h5>Sponsor <small><i>(Optional)</i></small></h5>
                <input type="text" class="form-control" name="sponsor" placeholder="Sponsor">
              </div>
            </div>
          </div>
          <h5>Account Details</h5>
          <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email">
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password" id="password">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
              </div>
            </div>
          </div>
          <h5>Personal Information</h5>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" name="first_name" placeholder="First Name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" name="last_name" placeholder="Last Name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="date" class="form-control" name="birthdate" placeholder="Birthdate">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name="contact_no" placeholder="Contact No.">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select name="gender" class="form-control">
                  <option value="">Select Gender</option>
                  <option value="1">Male</option>
                  <option value="2">Female</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="hidden" name="address_id" value="{{ Sequence::getNextSequence() }}">
                <select name="country_id" class="form-control">
                  <option value="">Select Country</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select name="region_id" class="form-control">
                  <option value="">Select Region</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select name="city_id" class="form-control">
                  <option value="">Select City</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="line1" placeholder="Address">
          </div>
          <div class="modal-foot">
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
