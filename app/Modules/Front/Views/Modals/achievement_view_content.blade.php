<div class="modal-content">
{{--   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Product Details</h4>
  </div> --}}
  <div class="modal-body">
    <div class="text-center" id="loader">
      <img src="{{ URL::asset('assets/app/images/loading.gif') }}"/>
    </div>
    <div class="">
      <div class="blog-img">
        <span id="image"></span>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="blog-two-info">
            <h3><span id="achievement"></span></h3>
          </div>
        </div>
      </div>
      <div class="blog-content">
        <p id="details"></p>
      </div>
    </div>
    <div class="modal-foot">
      <button type="button" class="btn pull-right btn-danger" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
