@extends('Front::layout')

@section('content')
  @include('Front::breadcrumbs')
  <div class="container">
    <div class="row">
      @foreach (Product::getForFront() as $product)
        <div class="col-md-4">
          <div class="blog-left wow fadeInLeft" style="height: 460px; min-height: 460px;">
            <div class="blog-img">
              <img src="{{ $product->image }}" alt="{{ $product->product }}" style="width: 358px; height: 221px" class="img-responsive"/>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="blog-two-info">
                  <a href="javascript:void(0)" data-bind="product_view_content" data-id="{{ $product->product_id }}" data-method="view" title="View Details">
                    <h3>{{ ucwords($product->product) }}</h3>
                  </a>
                  <small style="position: relative; top: -13px"><i>{{ $product->code }}</i></small>
                  <div>
                    &#8369;{{ number_format($product->selling_price ,2) }}
                  </div>
                </div>
{{--                 <a href="javascript:void(0)" data-bind="image_view" data-image="{{ $product->price_list }}" data-method="view_image" class="btn btn-success btn-xs" title="See Price List"><i class="fa fa-money"></i> Price List</a> --}}
              </div>
            </div>
            <div class="blog-content" style="padding-top: 0;">
              <p>
                {!! mb_strimwidth($product->description, 0, 130, "") !!}</a>
                @if (strlen($product->description) > 130)
                    <span>... <a href="javascript:void(0)" style="color: #3399cc" data-bind="product_view_content" data-id="{{ $product->product_id }}" data-method="view" title="Read More">Read More</a></span>
                @endif
              </p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/front/js/products.js') }}"></script>
@stop