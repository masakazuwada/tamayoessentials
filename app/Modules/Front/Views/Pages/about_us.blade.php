@extends('Front::layout')

@section('content')
  @include('Front::breadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="about-carousel wow fadeInLeft">
          <div id="myCarousel" class="carousel slide">
            <!-- Carousel items -->
            <div class="carousel-inner">
              @foreach (Slider::getData(App::getThis(Site::getIdBySegment('about-us'), Slider::TABLE, Slider::PRIMARY_KEY)) as $key => $sliderData)
                <div class="item {{ $key < 1 ? 'active' : '' }}">
                  <img src="{{ $sliderData->image }}" alt="" style="width: 458px; height: 400px" class="img-responsive">
                  <div class="carousel-caption">
                    {!! $sliderData->description !!}
                  </div>
                </div>
              @endforeach
            </div>
            <!-- Carousel nav -->
            <a class="mp carousel-control left" href="#myCarousel" data-slide="prev">
              <i class="fa fa-angle-left">
              </i>
            </a>
            <a class="mp carousel-control right" href="#myCarousel" data-slide="next">
              <i class="fa fa-angle-right">
              </i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-12 about wow fadeInRight carousel-details">
        {!! App::getThis(Site::getIdBySegment('about-us'), Slider::TABLE, 'details') !!}
      </div>
    </div>
  </div>
  <div class="container" id="tourpackages-carousel">
    <div class="row">
      <div class="profile">
        <h2>
          OUR TEAM
        </h2>
        @foreach (App::getData(Team::TABLE, 'page_id', Site::getIdBySegment('about-us')) as $key => $data)
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="thumbnail wow fadeInUp" data-wow-delay=".1s">
              <img src="{{ $data->image }}" alt="" style="width: 261px; height: 173px" class="img-responsive">
              <div class="caption">
                <h4>{{ $data->fullname }}</h4>
                <i>{{ $data->position }}</i>
                <div class="team-social-link">
                  <a target="_blank" href="{{ $data->twitter }}">
                    <i class="fa fa-twitter">
                    </i>
                  </a>
                  <a target="_blank" href="{{ $data->facebook }}">
                    <i class="fa fa-facebook">
                    </i>
                  </a>
                  <a target="_blank" href="{{ $data->google_plus }}">
                    <i class="fa fa-google-plus">
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>
    </div>
  </div>
@stop