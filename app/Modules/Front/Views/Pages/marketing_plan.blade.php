@extends('Front::layout')

@section('content')
  @include('Front::breadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="about-carousel wow fadeInLeft">
          <div id="myCarousel" class="carousel slide">
            <!-- Carousel items -->
            <div class="carousel-inner">
              @foreach (Slider::getData(App::getThis(Site::getIdBySegment('marketing-plan'), Slider::TABLE, Slider::PRIMARY_KEY)) as $key => $sliderData)
                <div class="item {{ $key < 1 ? 'active' : '' }}">
                  <img src="{{ $sliderData->image }}" alt="" style="width: 100%; height: 400px" class="img-responsive">
                  <div class="carousel-caption">
                    {!! $sliderData->description !!}
                  </div>
                </div>
              @endforeach
            </div>
            <!-- Carousel nav -->
            <a class="mp carousel-control left" href="#myCarousel" data-slide="prev">
              <i class="fa fa-angle-left">
              </i>
            </a>
            <a class="mp carousel-control right" href="#myCarousel" data-slide="next">
              <i class="fa fa-angle-right">
              </i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-12 about wow fadeInRight carousel-details">
        {!! App::getThis(Site::getIdBySegment('marketing-plan'), Slider::TABLE, 'details') !!}
      </div>
    </div>
  </div>
  <br/>
  <br/>
@stop