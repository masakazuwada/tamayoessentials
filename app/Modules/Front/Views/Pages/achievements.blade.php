@extends('Front::layout')

@section('content')
  @include('Front::breadcrumbs')
  <div class="container">
    <div class="row">
      @foreach (Achievement::get() as $achievement)
        <div class="col-md-4">
          <div class="blog-left wow fadeInLeft" style="height: 400px; min-height: 400px;">
            <div class="blog-img">
              <img src="{{ $achievement->image }}" alt="{{ $achievement->achievement }}" style="width: 358px; height: 221px" class="img-responsive" />
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="blog-two-info">
                  <a href="javascript:void(0)" data-bind="achievement_view_content" data-id="{{ $achievement->achievement_id }}" data-method="view" title="View Details">
                    <h3>{{ ucwords($achievement->achievement) }}</h3>
                  </a>
                  {{-- <p>
                    <small><i>{{ date('F d, Y', strtotime($achievement->date_achieve)) }}</i></small>
                  </p> --}}
                </div>
              </div>
            </div>
            <div class="blog-content" style="padding-top: 0;">
              <p>
                {!! mb_strimwidth($achievement->details, 0, 110, "") !!}
                @if (strlen($achievement->details) > 110)
                    <span>... <a href="javascript:void(0)" style="color: #3399cc" data-bind="achievement_view_content" data-id="{{ $achievement->achievement_id }}" data-method="view" title="Read More">Read More</a></span>
                @endif
              </p>
            </div>
            <!--<button class="btn btn-info btn-sm">
              Read More
            </button>-->
          </div>
        </div>
      @endforeach
    </div>
  </div>
@stop

@section('scripts')
  <script src="{{ URL::asset('assets/front/js/achievements.js') }}"></script>
@stop