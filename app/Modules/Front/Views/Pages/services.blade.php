@extends('Front::layout')

@section('content')
  @include('Front::breadcrumbs')
    <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="about-carousel wow fadeInLeft">
          <div id="myCarousel" class="carousel slide">
            <!-- Carousel items -->
            <div class="carousel-inner">
              @foreach (Slider::getData(App::getThis(Site::getIdBySegment('services'), Slider::TABLE, Slider::PRIMARY_KEY)) as $key => $sliderData)
                <div class="item {{ $key < 1 ? 'active' : '' }}">
                  <img src="{{ $sliderData->image }}" alt="" style="width: 458px; height: 400px" class="img-responsive">
                  <div class="carousel-caption">
                    {!! $sliderData->description !!}
                  </div>
                </div>
              @endforeach
            </div>
            <!-- Carousel nav -->
            <a class="mp carousel-control left" href="#myCarousel" data-slide="prev">
              <i class="fa fa-angle-left">
              </i>
            </a>
            <a class="mp carousel-control right" href="#myCarousel" data-slide="next">
              <i class="fa fa-angle-right">
              </i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-12 about wow fadeInRight carousel-details">
        {!! App::getThis(Site::getIdBySegment('services'), Slider::TABLE, 'details') !!}
      </div>
    </div>
  </div>
  <div class="">
    <div class="container">
      <div class="row">
        <div class="text-center feature-head">
          <h1>{{ App::getThis(Site::getIdBySegment('services'), Site::TABLE, 'name') }}</h1>
          <p>{{ App::getThis(Site::getIdBySegment('services'), Site::TABLE, 'summary') }}</p>
          <p>
        </div>
        @foreach (App::getData(Content::TABLE, 'page_id', Site::getIdBySegment('services')) as $key => $data)
          @if ($key % 2 == 0)
            @if ($key >= 2)
              </div>
            @endif
            <div class="services">
              <div class="col-lg-6 col-sm-6">
                <div class="icon-wrap round-five wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".1s">
                  <img src="{{ $data->image }}" alt="" width="70px" height="70px" class="img-responsive">
                </div>
                <div class="content">{!! $data->description !!}</div>
              </div>
          @else
            <div class="col-lg-6 col-sm-6">
              <div class="icon-wrap round-five wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".1s">
                <img src="{{ $data->image }}" alt="" width="70px" height="70px" class="img-responsive">
              </div>
              <div class="content">{!! $data->description !!}</div>
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
@stop