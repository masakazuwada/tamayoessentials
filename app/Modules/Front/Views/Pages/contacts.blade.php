@extends('Front::layout')

@section('styles')
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/toastr/toastr.min.css') }}">
@stop

@section('content')
  @include('Front::breadcrumbs')
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-sm-5 address hidden-xs">
        <?php $title = array('ADDRESS', 'BUSINESS HOURS', 'CONTACT NUMBER'); ?>
        @foreach (App::getData(Content::TABLE, 'page_id', Site::getIdBySegment('contacts')) as $key => $data)
          @if ($data->type == Content::TYPE_ADDRESS || $data->type == Content::TYPE_BUSINESS_HOURS || $data->type == Content::TYPE_CONTACT_NUMBERS)
            <section class="contact-infos">
              <h4 class="title custom-font text-black">
                {{ str_replace(["_", " "], ' ', str_replace(["TYPE_", ""], '', $data->type)) }}
              </h4>
              {!! $data->description !!}
            </section>
          @endif
        @endforeach
      </div>
      <div class="col-lg-7 col-sm-7 address">
        <h4>
          Drop us a line so that we can hear from you
        </h4>
        <div class="contact-form">
          <form role="form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
              <label for="name">
                Name
              </label>
              <input type="text" placeholder="Full Name" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label for="email">
                Email
              </label>
              <input type="text" placeholder="E-mail Address" name="email" class="form-control">
            </div>
            <div class="form-group">
              <label for="subject">
                Subject
              </label>
              <input type="text" placeholder="Subject" name="subject" class="form-control">
            </div>
            <div class="form-group">
              <label for="phone">
                Message
              </label>
              <textarea rows="5" class="form-control" name="message" placeholder="Message"></textarea>
            </div>
            <button class="btn btn-info pull-right" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="contact-map">
    <div id="map-canvas" style="width: 100%; height: 400px"></div>
  </div>
@stop

@section('scripts')
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyCQ1cXvh0E5zraFrMTVud7OXO0hXswGNzY"></script>

  <script>
    $(document).ready(function() {
        //Set the carousel options
        $('#quote-carousel').carousel({
          pause: true,
          interval: 4000,
        }
      );
    });

    //google map
    function initialize() {
      var myLatlng = new google.maps.LatLng(14.348675, 120.957869);
      var mapOptions = {
        zoom: 5,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel:false,
        zoom: 16,
        zoomControl : false,
        panControl : false,
        streetViewControl : false,
        mapTypeControl: false,
        overviewMapControl: false,
        clickable: false
      }
      var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Contact',
        animation: google.maps.Animation.DROP,
        verticalAlign: 'bottom',
        horizontalAlign: 'center',
        backgroundColor: '#3e8bff',
      }
      );
    }
    google.maps.event.addDomListener(window, 'load', initialize);
  </script>
  <script src="{{ URL::asset('assets/plugins/js/validation/jquery.validate.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/toastr/toastr.min.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/js/toastr/toastr-options.js') }}"></script>
  <script src="{{ URL::asset('assets/front/js/contacts.js') }}"></script>
@stop