@extends('Front::layout')

@section('content')
  <!-- Sequence Modern Slider -->
  <div id="da-slider" class="da-slider" style="background: transparent url({{ App::getThis(Site::getIdBySegment('home'), Slider::TABLE, 'background_image') }}) repeat 0% 0%;">
    @foreach (Slider::getData(App::getThis(Site::getIdBySegment('home'), Slider::TABLE, Slider::PRIMARY_KEY)) as $sliderData)
      <div class="da-slide">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="description">
                <h2><i>{!! nl2br($sliderData->title) !!}</i></h2>
                <p><i>{!! nl2br($sliderData->description) !!}</i></p>
              </div>
              <div class="da-img">
                <img src="{{ $sliderData->image }}" class="img-responsive"/>
              </div>
            </div>
          </div>
         </div>
      </div>
    @endforeach
  </div>
    <div class="pull-right" style="position: relative; top: -32px; z-index: 9999">
      <audio controls autoplay controlsList="nodownload" class="hidden-xs">
       <source src="{{ URL::asset('assets/front/audio/te.mp3') }}" type="audio/mpeg">
       <p>Browser does not support the audio element.</p>
      </audio>
    </div>
  <div class="container">
    <div class="row mar-b-50">
      <div class="col-md-12">
        <div class="text-center feature-head wow fadeInDown">
          <audio controls controlsList="nodownload" class="visible-xs">
           <source src="{{ URL::asset('assets/front/audio/te.mp3') }}" type="audio/mpeg">
           <p>Browser does not support the audio element.</p>
          </audio>
          <h1 class="">
            {!! App::getThis(Site::getIdBySegment('home'), Company::TABLE, 'header') !!}
          </h1>
        </div>

        <div class="feature-box">
          <div class="col-md-4 col-sm-4 text-center wow fadeInUp">
            <div class="feature-box-heading">
              <em>
                <img src="{{ URL::asset('assets/front/images/leaf.png') }}" alt="" width="100" height="100">
              </em>
              <h4>
                <b>Mission</b>
              </h4>
            </div>
            <p class="text-center">
              {!! App::getThis(Site::getIdBySegment('home'), Company::TABLE, 'mission') !!}
            </p>
          </div>
          <div class="col-md-4 col-sm-4 text-center wow fadeInUp">
            <div class="feature-box-heading">
              <em>
                <img src="{{ URL::asset('assets/front/images/leaf.png') }}" alt="" width="100" height="100">
              </em>
              <h4>
                <b>Vision</b>
              </h4>
            </div>
            <p class="text-center">
              {!! App::getThis(Site::getIdBySegment('home'), Company::TABLE, 'vision') !!}
            </p>
          </div>
          <div class="col-md-4 col-sm-4 text-center wow fadeInUp">
            <div class="feature-box-heading">
              <em>
                <img src="{{ URL::asset('assets/front/images/leaf.png') }}" alt="" width="100" height="100">
              </em>
              <h4>
                <b>Core Values</b>
              </h4>
            </div>
            <p class="text-center">
              {!! App::getThis(Site::getIdBySegment('home'), Company::TABLE, 'core_values') !!}
            </p>
          </div>
        </div>
        <!--feature end-->
      </div>
    </div>
  </div>

  <!--property start-->
      @foreach (App::getDataByType(Content::TABLE, 'page_id', Site::getIdBySegment('home')) as $key => $data)
        <div class="{{ ($key % 2 == 0) ? 'property gray-bg' : '' }}">
          <div class="container">
              <div class="row">
                <div class="col-lg-6 col-sm-6 text-center wow fadeInLeft">
                    <img src="{{ $data->image }}" class="img-responsive" style="height: 350px; width: 350px">
                </div>
                <div class="col-lg-6 col-sm-6 wow fadeInRight">
                  <p>{!! $data->description !!}</p>
                </div>
              </div>
          </div>
        </div>
      @endforeach
  <!--property end-->

  <div class="hr">
    <span class="hr-inner"></span>
  </div>

    <!--property start-->
    <div class="container">
      <h2>Passup Marketing Plan</h2><br>
      <div class="row">
        @foreach (App::getDataByType(Content::TABLE, 'page_id', Site::getIdBySegment('home'), Content::TYPE_PASSUP) as $key => $data)
          <div class="col-lg-4 fadeInLeft">
              <img src="{{ $data->image }}" class="img-responsive" style="height: 350px; width: 350px">
          </div>
        @endforeach
      </div>
    </div>
  <!--property end-->

  <div class="hr">
    <span class="hr-inner"></span>
  </div>

  <div class="container">
    <div class="row mar-b-50 our-clients">
      <div class="col-md-3">
        
        <h2>{{ App::getThis(Site::getIdBySegment('achievements'), Site::TABLE, 'name') }}</h2>
        <p>{{ App::getThis(Site::getIdBySegment('achievements'), Site::TABLE, 'summary') }}</p>
      </div>
      <div class="col-md-9">
        <div class="outside">
          <p>
            <span id="slider-prev"></span>
            |
            <span id="slider-next"></span>
          </p>
        </div>

        <ul class="bxslider1 clients-list">
          @foreach (Achievement::get() as $achievement)
            <li>
              <a href="javascript:void(0)">
                <img src="{{ $achievement->image }}" style="height: 111px; width: 168px" class="img-responsive"/>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('assets/plugins/js/parallax-slider/jquery.cslider.js') }}"></script>
    <script type="text/javascript">
      $(function() {
        $('#da-slider').cslider({
          autoplay    : true,
          bgincrement : 0
        });

      });
    </script>

    <!--common script for all pages-->
    <script src="{{ URL::asset('assets/front/js/common-scripts.js') }}"></script>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        $('.bxslider1').bxSlider({
          minSlides: 5,
          maxSlides: 6,
          slideWidth: 360,
          slideMargin: 2,
          moveSlides: 1,
          responsive: true,
          nextSelector: '#slider-next',
          prevSelector: '#slider-prev',
          nextText: 'Next →',
          prevText: '← Prev'
        });

      });
    </script>
    <script>
      $('a.info').tooltip();
      $(window).load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          start: function(slider) {
            $('body').removeClass('loading');
          }
        });
      });
      $(document).ready(function() {
        $("#owl-demo").owlCarousel({
          items : 4
        });
      });
      jQuery(document).ready(function(){
        jQuery('ul.superfish').superfish();
      });
      new WOW().init();
    </script>
@stop