@extends('Front::layout')
@section('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/vendors/unitegallery/dist/css/unite-gallery.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/vendors/unitegallery/dist/themes/default/ug-theme-default.css') }}" />

</style>
@stop
@section('content')
  @include('Front::breadcrumbs')
  <div id="gallery" style="display:none;">
    @foreach (Gallery::get() as $key => $data)
      @if ($data->type == 1)
        <img
          src="{{ $data->content }}"
          data-image="{{ $data->content }}"
          data-description="{{ $data->details }}"
          class="img-responsive">
      @else
        <img
          data-type="youtube"
          data-videoid="{{ App::getYoutubeId($data->content) }}"
          data-description="{{ $data->details }}"
          class="img-responsive">
      @endif
    @endforeach
  </div>
  <div style="padding-top: 30px;"></div>
@stop

@section('scripts')
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="{{ URL::asset('assets/plugins/vendors/unitegallery/dist/themes/default/ug-theme-default.js') }}"></script>
  <script src="{{ URL::asset('assets/plugins/vendors/unitegallery/dist/js/unitegallery.min.js') }}"></script>
  <script>
    $("#gallery").unitegallery({
      //gallery width
      gallery_width:900,  

      //gallery height
      gallery_height:500, 

      //gallery minimal width when resizing
      gallery_min_width: 400,

      //gallery minimal height when resizing
      gallery_min_height: 300,

      //default,compact,<a href="http://www.jqueryscript.net/tags.php?/grid/">grid</a>,slider
      //select your desired theme from the list of themes.
      gallery_theme:"default",

      //default, alexis etc... - the global skin of the gallery.
      //Will change all gallery items by default.
      gallery_skin:"default",

      //all , minimal , visible - preload type of the images.
      //minimal - only image nabours will be loaded each time.
      //visible - visible thumbs images will be loaded each time.
      //all - load all the images first time.
      gallery_images_preload_type:"minimal",

      //true / false - begin slideshow autoplay on start
      gallery_autoplay:false,

      //play interval of the slideshow
      gallery_play_interval: 3000,

      //true,false - pause on mouseover when playing slideshow true/false
      gallery_pause_on_mouseover: true,

      //true,false - change this option, add more mousewheel choices
      gallery_control_thumbs_mousewheel:false,  

      //true,false - enable / disble keyboard controls
      gallery_control_keyboard: true, 

      //true,false - next button on last image goes to first image.
      gallery_carousel:true,

      //true, false - preserver ratio when on window resize
      gallery_preserve_ratio: true, 

      //show error message when there is some error on the gallery area.
      gallery_debug_errors:false,

      //set custom background color. 
      //If not set it will be taken from css.
      gallery_background_color: "",

      slider_control_zoom:false,

      slider_enable_zoom_panel: false, 

      theme_enable_play_button: false,
    }); 
  </script>
@stop

