@extends('Front::layout')

@section('content')
  <div class="">
      <div class="container">
        <div class="form-wrapper">
          <form class="form-signin wow fadeInUp" method="POST" action="{{ Request::root() }}/login" onsubmit="return checkForm(this);"  style="border: 1px solid #eaeaea">
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">sign in now</h2>
            <div class="login-wrap">
              @if (Session::has('error'))
                  <div class="alert alert-danger" style="padding: 10px">
                    {{ Session::get('error') }}
                  </div>
              @endif
              <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="E-mail Address" value="{{ old('email') }}" autofocus>
                <div class="errors">{{ $errors->first('email') }}</div>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <div class="errors">{{ $errors->first('password') }}</div>
              </div>
              <label class="remember-me">
                <input type="checkbox" name="remember">
                Remember me
              </label>
              <div class="pull-right">
                  <a href="javascript:void(0)"> Forgot Password?</a>
              </div>
              <br><br>
              <button class="btn btn-sm btn-login btn-block" name="submitBtn" type="submit">Sign in</button>
              <div class="registration">
                  Don't have an account yet?
                  <a class="" href="javascript:void(0)">
                      Create an account
                  </a>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
@stop

@section('scripts')
  <script>
    function checkForm(form) 
    {
      form.submitBtn.disabled = true;
      form.submitBtn.value = "Please wait...";

      return true;
    }
  </script>
@stop