<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-4">
        <h1>{{ (str_replace('-', ' ', Request::segment(1))) }}</h1>
      </div>
      <div class="col-lg-8 col-sm-8">
        <ol class="breadcrumb pull-right">
          <li>
            <a href="{{ Request::root() }}">
              Home
            </a>
          </li>
          <li class="active">{{ (str_replace('-', ' ', ucfirst(Request::segment(1)))) }}</li>
        </ol>
      </div>
    </div>
  </div>
</div>