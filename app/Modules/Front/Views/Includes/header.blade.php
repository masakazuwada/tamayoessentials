<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tamayo Essentials Website">
    <meta name="keywords" content="Tamayo-Essentials, Tamayo Essentials, Tamayo, Essentials">
    <meta name="author" content="Edmar & Masakazu">

    <title>Tamayo Essentials</title>

    <link rel="shortcut icon" href="{{ URL::asset('assets/app/images/logo/logo2.png') }}">
    
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/bootstrap/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/theme.css') }}" />
    {{-- <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/bootstrap/bootstrap-reset.css') }}" /> --}}

    <!--external css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/flexslider.css') }}"/>
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/bxslider/jquery.bxslider.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/owlcarousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/owlcarousel/owl.theme.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/superfish.css') }}" media="screen">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato'>
    {{-- <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans'> --}}

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/component.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/css/style-responsive.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/parallax-slider/parallax-slider.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/css/toastr/toastr.min.css') }}">

    @yield('styles')

  </head>

  <body>
    @include('Front::navbar')