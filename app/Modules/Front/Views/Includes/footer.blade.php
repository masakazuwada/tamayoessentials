    <!--footer start-->
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-3">
            <h1>
              contact info
            </h1>
            <address>
              <p>
                <i class="fa fa-home pr-10"></i>Address:<br>
                {!! isset(Content::getContentByType('TYPE_ADDRESS')[0]) ? Content::getContentByType('TYPE_ADDRESS')[0]->description : '' !!}
              </p>
              <p>
                <i class="fa fa-phone pr-10"></i>Contact Nos.:<br>
                {!! isset(Content::getContentByType('TYPE_CONTACT_NUMBERS')[0]) ? Content::getContentByType('TYPE_CONTACT_NUMBERS')[0]->description : '' !!}
              </p>
              <p>
                <i class="fa fa-envelope pr-10"></i>E-mails:<br>
                <a href="javascript:void(0)">
                  {!! isset(Content::getContentByType('TYPE_PRIMARY_EMAIL')[0]) ? Content::getContentByType('TYPE_PRIMARY_EMAIL')[0]->description : '' !!}
                </a>
                <a href="javascript:void(0)">
                  {!! isset(Content::getContentByType('TYPE_SUPPORT_EMAIL')[0]) ? Content::getContentByType('TYPE_SUPPORT_EMAIL')[0]->description : '' !!}
                </a>
              </p>
            </address>
          </div>
          <div class="col-lg-3 col-sm-3">
            <h1>SOCIAL MEDIA</h1>
            <address>
              <p>
                <i class="fa fa-twitter pr-10"></i>Twitter:<br>
                <a targe="_blank" href="{!! isset(Content::getContentByType('TYPE_TWITTER')[0]) ? Content::getContentByType('TYPE_TWITTER')[0]->description : '' !!}">
                  {!! isset(Content::getContentByType('TYPE_TWITTER')[0]) ? Content::getContentByType('TYPE_TWITTER')[0]->description : '' !!}
                </a>
              </p>
              <p>
                <i class="fa fa-facebook pr-10"></i>Facebook:<br>
                <a targe="_blank" href="{!! isset(Content::getContentByType('TYPE_FACEBOOK')[0]) ? Content::getContentByType('TYPE_FACEBOOK')[0]->description : '' !!}">
                  {!! isset(Content::getContentByType('TYPE_FACEBOOK')[0]) ? Content::getContentByType('TYPE_FACEBOOK')[0]->description : '' !!}
                </a>
              </p>
              <p>
                <i class="fa fa-youtube pr-10"></i>Youtube:<br>
                <a targe="_blank" href="{!! isset(Content::getContentByType('TYPE_YOUTUBE')[0]) ? Content::getContentByType('TYPE_YOUTUBE')[0]->description : '' !!}">
                  {!! isset(Content::getContentByType('TYPE_YOUTUBE')[0]) ? Content::getContentByType('TYPE_YOUTUBE')[0]->description : '' !!}
                </a>
              </p>
            </address>
          </div>
          <div class="col-lg-3 col-sm-3">
            <h1>Our Company</h1>
            <ul class="page-footer-list">
              @foreach (Site::buildNavigation(array('parent' => Site::MAIN)) as $page)
                @if ($page->segment != 'home')
                  <li>
                    <a href="{{ Request::root() }}/{{ $page->segment }}">{{ $page->name }}</a>
                  </li>
                @endif
              @endforeach
            </ul>
          </div>
          <div class="col-lg-3 col-sm-3">
            <h1>Tamayo Essentials</h1>
            {!! App::getThis(Site::getIdBySegment('home'), Site::TABLE, 'summary') !!}
          </div>
        </div>
      </div>
    </footer>
    <!-- footer end -->
    <!--small footer start -->
    <footer class="footer-small">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-sm-6 pull-right">
            <ul class="social-link-footer list-unstyled">
              <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".1s"><a href="{{ isset(Content::getContentByType('TYPE_FACEBOOK')[0]) ? Content::getContentByType('TYPE_FACEBOOK')[0]->description : '' }}"><i class="fa fa-facebook"></i></a></li>
              <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".5s"><a href="{{ isset(Content::getContentByType('TYPE_TWITTER')[0]) ? Content::getContentByType('TYPE_TWITTER')[0]->description : '' }}"><i class="fa fa-twitter"></i></a></li>
              <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".8s"><a href="{{ isset(Content::getContentByType('TYPE_YOUTUBE')[0]) ? Content::getContentByType('TYPE_YOUTUBE')[0]->description : '' }}"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <div class="copyright">
              <p>&copy; Copyright - Tamayo Essentials</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!--small footer end-->

    <!-- Large Modal Format -->
    <div class="modal fade" id="large" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Ajax Modal Content Here -->
        </div>
      </div>
    </div>

    <!-- Medium Modal Format -->

    <div class="modal fade" id="medium" tabindex="-1"  role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <!-- Ajax Modal Content Here -->
        </div>
      </div>
    </div>

    <!-- Medium_XS Modal Format -->
    <div class="modal fade" id="medium-xs" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Ajax Modal Content Here -->
        </div>
      </div>
    </div>

    <!-- Small Modal Format -->
    <div class="modal fade" id="small" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <!-- Ajax Modal Content Here -->
        </div>
      </div>
    </div>

    <!-- Small Modal Format -->
    <div class="modal fade" id="confirmation" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <!-- Ajax Modal Content Here -->
        </div>
      </div>
    </div>

    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!}
    </script>

    <script src="{{ URL::asset('assets/plugins/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/hover-dropdown.js') }}"></script>
    <script defer src="{{ URL::asset('assets/front/js/jquery.flexslider.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/parallax-slider/modernizr.custom.28468.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/bxslider/jquery.bxslider.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/wow.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/jquery.easing.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/link-hover.js') }}"></script>
    <script src="{{ URL::asset('assets/front/js/superfish.js') }}"></script>

    <script src="{{ URL::asset('assets/plugins/js/validation/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/js/toastr/toastr-options.js') }}"></script>

    <script src="{{ URL::asset('assets/app/js/helper.js') }}"></script>
    @yield('scripts')
  </body>
</html>