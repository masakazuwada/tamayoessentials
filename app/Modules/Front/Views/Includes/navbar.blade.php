    <header class="head-section">
      <div class="navbar navbar-default navbar-static-top container">
          <div class="navbar-header">
              <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ Request::root() }}">
                Tamayo <span>Essentials</span>
              </a>
              <div class="contact-us-logo">
                {!! isset(Content::getContentByType('TYPE_PRIMARY_EMAIL')[0]) ? Content::getContentByType('TYPE_PRIMARY_EMAIL')[0]->description : '' !!}
                {!! isset(Content::getContentByType('TYPE_CONTACT_NUMBERS')[0]) ? Content::getContentByType('TYPE_CONTACT_NUMBERS')[0]->description : '' !!}
              </div>
          </div>
          <div class="navbar-collapse collapse" style="padding-top: 12px;">
              <ul class="nav navbar-nav">
                @foreach (Site::buildNavigation(array('parent' => Site::MAIN)) as $page)
                  @if (count(Site::buildNavigation(array('parent' => $page->page_id))) == 0)
                    <li>
                      <a href="{{ Request::root() }}/{{ $page->segment }}">{{ $page->name }}</a>
                    </li>
                  @else
                    <li class="dropdown">
                      <a href="{{ Request::root() }}/{{ $page->segment }}" class="dropdown-toggle disabled" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown">
                        {{ $page->name }} <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                        @foreach (Site::buildNavigation(array('parent' => $page->page_id)) as $subPage)
                          <li>
                            <a href="{{ Request::root() }}/{{ $subPage->segment }}">{{ $subPage->name }}</a>
                          </li>
                        @endforeach
                      </ul>
                    </li>
                  @endif
                @endforeach
                <li>
                    <a style="border: 1px solid #eaeaea" href="{{ Request::root() }}/member">{{ Auth::check() ? 'Dashboard' : 'Login' }}</a>
                </li>
                {{-- <li><input class="form-control search" placeholder=" Search" type="text"></li> --}}
              </ul>
          </div>
      </div>
    </header>