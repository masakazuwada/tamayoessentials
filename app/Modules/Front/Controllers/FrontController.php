<?php
namespace App\Modules\Front\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Content;
use Mail;

class FrontController extends Controller
{
    public function sendMessage(Request $request)
    {
        $data = array();
        $data['status'] = false;

        if (isset(Content::getContentByType('TYPE_SUPPORT_EMAIL')[0])) {
            if (($ajaxData = $request->all()) && $request->ajax()) {
                Mail::send('contact-us.message_content', ['data' => $ajaxData], function ($message) use($request) {
                    $supportEmail = Content::getContentByType('TYPE_SUPPORT_EMAIL')[0]->description;
                    $message->from($_ENV['MAIL_USERNAME'], 'Tamayo Essentials');
                    $message->to($supportEmail)->subject('Tamayo Essentials Contact us Form');

                });
                $data['status'] = true;
            }
        }
        return json_encode($data);
    }

    // Load Website pages
    public function registration()
    {
        return view('Front::registration');
    }

    public function home()
    {
        return view('Front::index');
    }

    public function products()
    {
        return view('Front::products');
    }

    public function aboutUs()
    {
        return view('Front::about_us');
    }

    public function services()
    {
        return view('Front::services');
    }
    public function contacts()
    {
        return view('Front::contacts');
    }
    public function achievements()
    {
        return view('Front::achievements');
    }
    public function gallery()
    {
        return view('Front::gallery');
    }
    public function marketingPlan()
    {
        return view('Front::marketing_plan');
    }

}