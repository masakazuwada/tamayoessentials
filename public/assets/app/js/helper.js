// phpjs.org
function nl2br(str, is_xhtml)
{
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function getCountries(element, dataId = false)
{
    var $country = $(element);

    return $.ajax({
        url : APP_URL + '/address/get-countries',
        type : 'get',
        dataType : 'json',
        beforeSend: function() {
            var $loader = $('<option />');

            $loader.attr('value', '').text('Fetching...').prop('selected', true);

            $country.append( $loader );
        },
        success: function( result ) {
            if( result instanceof Array ) {
                var $select = $('<option />');
                    $select.attr('value', '').text('Select Country');

                $country.empty();
                $country.append($select);

                $.each(result, function(index, value) {
                    var $option = $( '<option />' );
                    $option.attr('value', result[ index ].id).text(result[index].name);
                    if (dataId) { if (dataId == result[ index ].id) { $option.attr('selected', true) }}
                    $country.append( $option );
                });
            }
        },
        error : function( xhr, status ) {
            alert( xhr.responseText );
        }
    });
}

function getRegions(element, countryId, dataId = false)
{
    var $regions = $(element);

    return $.ajax({
        url : APP_URL + '/address/get-regions-by-country',
        type : 'get',
        dataType : 'json',
        data : { country_id : countryId },
        beforeSend: function() {
            var $loader = $('<option />');
            $loader.attr('value', '').text('Fetching...').prop('selected', true);
            $regions.append( $loader );
        },
        success: function( result ) {
            if( result instanceof Array ) {
                var $select = $('<option />');
                    $select.attr('value', '').text('Select Region');

                $regions.empty();
                $regions.append($select);

                $.each(result, function(index, value) {
                    var $option = $( '<option />' );
                    $option.attr('value', result[ index ].id).text(result[index].name);
                    if (dataId) { if (dataId == result[ index ].id) { $option.attr('selected', true) }}
                    $regions.append( $option );
                });
            }
        },
        error : function( xhr, status ) {
            alert( xhr.responseText );
        }
    });
}

function getCities(element, regionId, dataId = false)
{
    var $cities = $(element);

    if (regionId) {
        $(element).prop('disabled', false).empty();
    } else {
        $(element).prop('disabled', true).empty();
    }

    return $.ajax({
        url : APP_URL + '/address/get-cities-by-region',
        type : 'get',
        dataType : 'json',
        data : { region_id : regionId },
        beforeSend: function() {
            var $loader = $('<option />');

            $loader.attr('value', '').text('Fetching...').prop('selected', true);

            $cities.append( $loader );
        },
        success: function( result ) {
            if( result instanceof Array ) {
                var $select = $('<option />');
                    $select.attr('value', '').text('Select City');

                $cities.empty();
                $cities.append($select);

                $.each(result, function(index, value) {
                    var $option = $( '<option />' );
                    $option.attr('value', result[ index ].id).text(result[index].name);
                    if (dataId) { if (dataId == result[ index ].id) { $option.attr('selected', true) }}
                    $cities.append( $option );
                });
            }
        },
        error : function( xhr, status ) {
            alert( xhr.responseText );
        }
    });
}