$( function ($) {
    var obj = {};

    $(document).on('click', '[data-bind]', function (e) {
        e.preventDefault();
        obj.id = $(this).data('id');
        obj.id_up = $(this).data('id_up');
        
        $('#back').empty();
        if (obj.id_up) {
            $('#back').append('<a href="javascript:void(0)" data-bind="get" data-id="'+ obj.id_up +'" class="btn btn-primary btn-xs" title="Go Back"><i class="fa fa-arrow-circle-left"></i> Back</a>');
        }
        obj.chart();
    });

    obj.chart = function() {
        $('#chart').html(' \
            <center><div class="processing-wrapper"> \
            <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
            </div></center>');
        return $.ajax({
            url : APP_URL + '/member/pass-up/create-chart-by-member',
            type : 'get',
            dataType : 'json',
            data : { member_id: obj.id },
            success: function(result) {
                $('#chart').empty();
                var totalCount = 0;
                var html = '<div class="tree">';
                html += '<ul><li class="level-one">';
                $.each(result, function(i, data) {
                    html += '<a href="javascript:void(0)"> \
                        <img src="'+ APP_URL + data.chart.level_one.image +'" alt="'+ data.chart.level_one.member_id +'"> \
                        <div>'+ data.chart.level_one.member_id +'</div> \
                        <div>'+ data.chart.level_one.full_name +'</div> \
                    </a>';
                    if (data.chart.level_one.members instanceof Array && data.chart.level_one.members.length) {
                        html += '<ul>';
                        $.each(data.chart.level_one.members, function(j, data2) {
                            var count = 0;
                            html += '<li class="level-two"> \
                                <a href="javascript:void(0)" data-bind="get" data-id="'+ data2.member_id +'" data-id_up="'+ data.chart.level_one.member_id +'"> \
                                    <img src="'+ APP_URL + data2.image +'" alt="'+ data2.member_id +'"> \
                                    <div>'+ data2.member_id +'</div> \
                                    <div>'+ data2.full_name +'</div> \
                                </a>';
                            if (data2.members instanceof Array && data2.members.length) {
                                html += '<ul>';
                                $.each(data2.members, function(k, data3) {
                                    count++;
                                    html += '<li class="level-three"> \
                                        <a href="javascript:void(0)" data-bind="get" data-id="'+ data3.member_id +'" data-id_up="'+ data.chart.level_one.member_id +'"> \
                                            <img src="'+ APP_URL + data3.image +'" alt="'+ data3.member_id +'"> \
                                            <div>'+ data3.member_id +'</div> \
                                            <div>'+ data3.full_name +'</div> \
                                        </a> \
                                    </li>';
                                });
                                html += '</ul>';
                            }
                            html += '</li>';
                            totalCount = (count ? (totalCount + count) :  (totalCount + 1));
                        });
                        html += '</ul>';
                    }
                });
                html += '</li></ul></div>';
                $('#chart').append(html);
                totalwidth = ($('.scoll-tree').width() > (totalCount * 97.2) ? $('.scoll-tree').width() : (97.2 * totalCount));
                $('.scoll-tree').css('width', totalwidth);
            },
            'error': function () {
                toastr.error("Failed to load data. Please contact your administrator.", "Tamayo Essentials");
            }
        });
    }


    obj.chart();
});