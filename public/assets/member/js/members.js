$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    obj.members = $('#members').DataTable({
        'processing': true,
        'responsive': true,
        'order': [[0, 'DESC']],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/member/pass-up/get'
        },
        'columns': [
            { 'data': 'member_id', 'sClass': 'text-center' },
            { 'data': 'email', 'sClass': 'text-center' },
            { 'data': 'full_name', 'sClass': 'text-center' },
            { 
                'data': 'status',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data) {
                        return 'Active';
                    }
                    return 'Inactive';
                }
            },
            { 'data': 'date_registered', 'sClass': 'text-center' },
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/member/pass-up/save',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.members.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Members");
                }
                else {
                    toastr.error('Error', "Members");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');

        return $.ajax({
            url : APP_URL + '/member/pass-up/get-details',
            type : 'get',
            dataType : 'json',
            data : { member_id: id },
            success: function (result) {
                $.each( result, function (index, value) {
                    switch (index) {
                        case 'image' :
                            $form.find('#image').val(value);
                            $form.find('[id=photo_preview]').prop('src', APP_URL + value);
                            break;
                        default :
                            $form.find('[name=' + index + ']').val(value);
                            break;
                    }
                });
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/member/pass-up/remove',
            type : 'get',
            dataType : 'json',
            data : { member_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Members");
                }
                else {
                    toastr.error('Error', "Members");
                }
                obj.members.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email : {
                    required: true,
                    email : true,
                },
                type : {
                    required: true
                },
                status : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $fullscreenModal = $('#fullscreen'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/member/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/member/pass-up/app/' + obj.url;

        switch(obj.method) {
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    getDetailsById($mediumModal, obj.id).done(function () {
                        $('[name=password]').prop('placeholder', "Leave blank if you don't want to change");
                        $('[name=email]').attr('readonly', 'true');
                        showPreview($mediumModal);
                        obj.toggleShowPassword({
                            field: '#password_input',
                            control: '#show_password'
                        });
                        validateUpdate($mediumModal.find('form'), $mediumModal);
                        $mediumModal.modal({
                            show : true,
                            backdrop: 'static',
                        });
                    });
                })
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Members");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }
});
