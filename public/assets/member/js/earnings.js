$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.payout_available = $(this).data('payout_available');
        obj.type = $(this).data('type');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    obj.earnings = $('#earnings').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/member/earning/get'
        },
        'columns': [
            { 'data': 'date_earned', 'sClass': 'text-center' },
            { 'data': 'invited_by', 'sClass': 'text-center' },
            { 'data': 'amount', 'sClass': 'text-right' },
            { 'data': 'points', 'sClass': 'text-right' },
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.payouts = $('#payouts').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/member/payout/get'
        },
        'columns': [
            { 'data': 'request_date', 'sClass': 'text-center' },
            { 
                'data': 'type',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data == 1) {
                        return '<span style="color: #1ABB9C">QSR</span>'
                    }
                    return '<span style="color: #ff7a00">QSP</span>'
                }
            },
            { 'data': 'amount', 'sClass': 'text-right' },
            { 'data': 'remarks', 'sClass': 'text-left none' },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    if (row.status == 1) {
                        return '<i class="fa fa-check" title="Approved"></i>';
                    } else if (row.status == 2) {
                        return '<i class="fa fa-close" title="Rejected"></i>';
                    } else {
                        return  '<a href="javacript:void(0)" data-bind="confirmation" data-method="delete" data-id="'+ row.payout_id +'" class="btn btn-danger btn-xs" title="Discard"> \
                                        <i class="fa fa-close"></i> Discard \
                                    </a>';
                    }
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
            <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/member/payout/save',
            type : 'post',
            data : $form.serialize(),
            dataType : 'json',
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.payouts.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Payouts");
                }
                else {
                    toastr.error('Error', "Payouts");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
        $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/member/payout/remove',
            type : 'get',
            dataType : 'json',
            data : { payout_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Payouts");
                }
                else {
                    toastr.error('Error', "Payouts");
                }
                obj.payouts.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }  

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateSave($form, $modal) {
        var msg = 'Requested';
        $form.validate({
            ignore: [],
            rules: {
                type : {
                    required: true,
                },
                amount : {
                    required: true,
                    min: 1,
                    remote : {
                        url : APP_URL + '/member/earning/check-if-has-balance',
                        type : 'get',
                        data : { field : (obj.type == 1 ? 'amount_balance' : 'points_balance') },
                    },
                },
            },
            messages: {
                amount: {
                    min : 'Payout out amount must be greater than 0.',
                    remote : "You don't have enough balance available."
                }
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $fullscreenModal = $('#fullscreen'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/member/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'payout' :
                $.ajax({
                    url : APP_URL + '/member/payout/get-total-pending-by-member',
                    type : 'get',
                    dataType : 'json',
                    data : { type : obj.type },
                    success : function (result) {
                        if(result.status) {
                            obj.getModalContent(ajaxURL, $smallModal).done(function () {
                                $('[name=amount]').val(obj.payout_available);
                                if (obj.type == 2) {
                                    $('[name=type]').val(2);
                                    $('#type_label').text('Points');
                                    $('[type=submit]').removeClass('btn-success').addClass('btn-warning');
                                } else {
                                    $('[name=type]').val(1);
                                    $('#type_label').text('Amount');
                                }
                                validateSave($smallModal.find('form'), $smallModal);
                                $smallModal.modal({
                                    show : true,
                                });
                            });
                        }
                        else {
                            toastr.error('You still have pending '+ (obj.type == 1 ? "amount" : "points") +' payouts. Wait for it to be completed and try again.', "Payouts");
                        }
                    },
                });
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text('Discard Request');
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }
});
