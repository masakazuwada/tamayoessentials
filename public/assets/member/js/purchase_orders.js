$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    obj.purchase_orders = $('#purchase_orders').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/member/purchase-order/get',
            'error': function () {
                toastr.error("Failed to load data, please try again!", "Purchase Orders");
            }
        },
        'columns': [
            { 'data': 'date_purchased', 'sClass': 'text-center' },
            { 'data': 'products', 'sClass': 'text-left' },
            { 'data': 'customer', 'sClass': 'text-left' },
            { 'data': 'total', 'sClass': 'text-right' },
            {
                'data': 'details',
                'render': function (data, type, row) {
                    if (data != null) {
                        return data.replace(/\n/g, '<br>'); 
                    }
                    return data;
                }
            },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void(0)" data-bind="purchase_order_lines_view" data-method="view" data-id="'+ row.purchase_order_id +'" data-order_line_id="'+ row.order_line_id +'" class="btn btn-success btn-xs" title="View Lines"> \
                        <i class="fa fa-eye"></i> View \
                        </a>';
                    return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        },
        'createdRow': function(row, data, index) {
            var row = $(row);
            row.attr('data-toggle', 'tooltip');
            row.attr('title', 'Encoded by: '+ data['encoder'] + "\n" + 'Timestamp: '+ (data['timestamp'] != null ? data['timestamp'] : data['date_collected']));
        }
    }).on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function ajaxGetLines($modal)
    {
        obj.purchase_order_lines = $('#purchase_order_lines').DataTable({
            'processing': true,
            'responsive': true,
            'filter': false,
            'order': [],
            'ajax': {
                'url' : APP_URL + '/member/purchase-order/get-lines',
                'data': function(d) {
                    d.id = obj.id;
                },
                'error': function () {
                    toastr.error("Failed to load data, please try again!", "Orders");
                }
            },
            'columns': [
                { 
                    'data': 'image',
                    'sClass': 'text-center',
                    'render': function (data, type, row) {
                        return '<a href="'+ (APP_URL + data) +'" target="_blank"> \
                        <img src="'+ (APP_URL + data) +'" height="20px" width="20px" title="See full image" alt="'+ row.product +'"/></a>';
                    }
                },
                { 'data': 'product' },
                { 'data': 'quantity', 'sClass': 'text-center' },
                { 
                    'data': 'discount',
                    'sClass': 'text-center',
                    'render' : function (data, type, row) {
                        return  data + '%';
                    }
                },
                { 'data': 'price', 'sClass': 'text-right' },
                { 'data': 'sub_total', 'sClass': 'text-right' },
                ]
            });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            // beforeSend: function () {
            //     $.spin('true');
            // },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    function loadModal()
    {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/member/' + obj.url;

        switch(obj.method) {
            case 'view' : 
                obj.getModalContent(ajaxURL, $modal).done(function () {
                    ajaxGetLines($modal);
                    $modal.on('shown.bs.modal', function (e) {
                      obj.purchase_order_lines.columns.adjust().responsive.recalc();
                  });
                    $modal.modal('show');
                });
                break;
        }
    }
    
    obj.select2 = function(selector)
    {
        $(selector).select2({
            width: '100%',
            dropdownParent: obj.modal,
        });
    }
});
