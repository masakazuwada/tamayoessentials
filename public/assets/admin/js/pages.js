$( function ($) {
    var obj = {};
    
    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');
        obj.level = $(this).data('level');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    obj.pages = $('#pages').DataTable({
        'processing': true,
        // 'responsive': true,
        'rowReorder': true,
        'order': [],
        'ajax': {
            "url": APP_URL + '/administration/site-pages/get'
        },
        'columns': [
            { 'data': 'drag', 'sClass': 'text-center' },
            { 'data': 'name' },
            { 'data': 'summary' },
            {
                'data': 'status',
                'sClass': 'text-center',
                'sortable': false,
                'render': function(data, row, type) {
                    if (data) {
                        return '<i class="fa fa-check"></i>';
                    }
                    return '<i class="fa fa-close"></i>';
                }
            },
            { 'data': 'actions', 'sClass': 'text-center', 'width': 200 }
        ],
        'columnDefs': [
            {
                'data': 'drag',
                'targets': 0,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<i class="fa fa-bars"></i>';
                return html;
                }
            },
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a target="_blank" href="'+ APP_URL +'/'+ row.segment +'" class="btn btn-success btn-xs" title="View"> \
                                    <i class="fa fa-search"></i> View \
                                </a> \
                                <a href="javascript:void(0)" class="btn btn-info btn-xs" data-bind="page_edit" data-method="update" data-id="'+ row.page_id +'" data-level="'+ row.level +'" title="Edit"> \
                                    <i class="fa fa-edit"></i> Edit \
                                </a> \
                                <a href="'+ (row.manageable ? (APP_URL +'/administration/site-pages/'+ row.segment) : 'javascript:void(0)') +'" class="btn btn-warning btn-xs '+ (row.manageable ? '' : 'disabled') +'" title="Manage"> \
                                    <i class="fa fa-wrench"></i> Manage \
                                </a> \
                                ';
                return html;
                }
            },
            { orderable: false, targets: [ 1,2,3 ] },
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        },
        'createdRow': function(row, data, index) {
            var row = $(row);
            row.attr('id', data.page_id);
            td = row.find('td:eq(0)');
            td.attr('style', 'cursor: move');
        }
    });

    obj.pages.on( 'row-reorder', function (e, details, changes) {
        for (var i = 0; i < details.length; i++) {
            var pageId = $(details[i].node).attr('id'),
                oldPos = details[i].oldPosition,
                newPos = details[i].newPosition;
            $.ajax({
                url : APP_URL + '/administration/site-pages/reorder',                
                dataType: 'json',
                data : { page_id: pageId, order_no: newPos },
                type: 'get',
                success : function (result) {
                    if(result.status) {
                        obj.pages.ajax.reload();
                    }
                    else {
                        toastr.error('Error', "pages");
                    }
                },
                error : function (xhr, status) {
                    alert(xhr.responseText);
                }
            });

        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/save',
            type : 'post',
            dataType : 'json',
            data: $form.serialize(),
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.pages.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "pages");
                }
                else {
                    toastr.error('Error', "pages");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');

        return $.ajax({
            url : APP_URL + '/administration/site-pages/get-details',
            type : 'get',
            dataType : 'json',
            data : { page_id: id },
            success: function (result) {
                $.each( result, function (index, value) {
                    $form.find('[name=' + index + ']').val(value);
                });
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getLevelsForDropdown($modal, level)
    {
        var $parents = $('#parents');

        return $.ajax({
            url : APP_URL + '/administration/site-pages/get-levels-for-dropdown',
            type : 'get',
            dataType : 'json',
            data: { level : level },
            beforeSend: function() {
                var $loader = $('<option />');
                $loader.attr('value', '').text('Fetching...').prop('selected', true);
                $parents.append($loader);
            },
            success: function(result) {
                if(result instanceof Array) {
                    var $select = $('<option />');
                    $select.attr('value', '0').text('Main');
                    
                    $parents.empty();
                    $parents.append($select);
                    
                    $.each(result, function(index, value) {
                        var $option = $('<option />');
                        $option.attr('value', result[index].page_id).text(result[index].name);
                        $parents.append($option);
                    });
                }
            },
            error : function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            rules: {
                name : {
                    required: true,
                },
                parent : {
                    required: true,
                },
                status : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    getLevelsForDropdown($mediumModal, obj.level).done(function() {
                        getDetailsById($mediumModal, obj.id).done(function () {
                            validateUpdate($mediumModal.find('form'), $mediumModal);
                            $mediumModal.modal({
                                show : true,
                                backdrop: 'static',
                            });
                        });
                    });
                });
                break;
        }
    }
    
});
