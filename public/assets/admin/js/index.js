$( function( $ ) {
    var obj = {};
    var index= -1000,
        indexContent = 0;

    showPreview();

    $(document).on("click", ":submit", function(e) {
        var btn = $(this).val();
        if (btn === 'slider_submit_btn') {
            validateSlider();
        }
        else if (btn === 'company_submit_btn') {
            validateCompany();
        }
        else if (btn === 'content_submit_btn') {
            validateContent();
        }
        else if (btn === 'passup_submit_btn') {
            validatePassup();
        }
    });

    $('#slider_form')
        .on('click', '#add_more_data', function() {
            index++;
            var html = '<div class="form-group slider-data"> \
                           <div class="row"> \
                              <div class="pull-right"> \
                                <a href="javascript:void(0)" class="btn btn-danger delete_btn" style="top: 0"><i class="fa fa-close"></i></a> \
                              </div> \
                          </div> \
                          <div class="row"> \
                            <input type="hidden" name="slider_data_id[]" value=""> \
                            <div class="col-md-4"> \
                              <div class="form-group"> \
                                <label for="">Image</label> \
                                <div class="photo-upload text-center"> \
                                  <input type="hidden" name="slider_data_image[]" value=""> \
                                  <input id="photo_'+ index +'" type="file" name="slider_data_image[]" class="photo"> \
                                  <label for="photo_'+ index +'"> \
                                    <div class="image rou"> \
                                      <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" > \
                                      <div class="after"> \
                                      <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                                      </div> \
                                    </div> \
                                  </label> \
                                </div> \
                              </div> \
                            </div> \
                            <div class="col-md-4"> \
                              <label for="">Title</label> \
                              <textarea name="slider_data_title[]" class="form-control" cols="30" rows="5" placeholder="Title"></textarea> \
                            </div> \
                            <div class="col-md-4"> \
                              <label for="">Description</label> \
                              <textarea name="slider_data_desc[]" id="" cols="30" rows="5" class="form-control" placeholder="Description"></textarea> \
                            </div> \
                          </div> \
                        </div>';

            $('#slider_data_last_row').before(html);
            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.removeSlider($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    $('#content_form')

        .on('click', '#add_more_content', function() {
            indexContent++;
            var html = ' <div class="form-group"> \
              <div class="row"> \
                  <div class="pull-right"> \
                    <a href="javascript:void(0)" class="btn btn-danger delete_btn" style="top: 0"><i class="fa fa-close"></i></a> \
                  </div> \
              </div> \
              <div class="row"> \
                <input type="hidden" name="content_id[]" value=""> \
                <div class="col-md-4"> \
                  <label for="">Image</label> \
                  <div class="photo-upload text-center"> \
                    <input type="hidden" name="image[]" id="image" value=""> \
                    <input id="content_photo_'+ indexContent +'" type="file" name="image[]" class="photo"> \
                    <label for="content_photo_'+ indexContent +'"> \
                      <div class="image rou"> \
                        <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" > \
                        <div class="after"> \
                        <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                        </div> \
                      </div> \
                    </label> \
                  </div> \
                </div> \
                <div class="col-md-8"> \
                  <label for="">Description</label> \
                  <textarea name="description[]" id="textarea_'+ indexContent +'" cols="30" rows="5" class="form-control" placeholder="Description"></textarea> \
                </div> \
              </div> \
            </div>';

            $('#content_last_row').before(html);
            $('#textarea_'+ indexContent +'').wysihtml5({
              toolbar: {
                "font-styles": true,
                "emphasis": true,
                "lists": true,
                "html": true,
                "link": false,
                "image": false,
                "color": false,
                "blockquote": false,
                "size": 'sm'
              },
            });
            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.removeContent($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    obj.saveSlider = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/home/save-slider',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Home Page");
                }
                else {
                    toastr.error('Error', "Home Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.saveCompany = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/home/save-company',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Home Page");
                }
                else {
                    toastr.error('Error', "Home Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.saveContent = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/home/save-content',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Home Page");
                }
                else {
                    toastr.error('Error', "Home Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.savePassup = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/home/save-passup',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Home Page");
                }
                else {
                    toastr.error('Error', "Home Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.removeSlider = function (sliderDataId) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/home/remove-slider',
            type : 'get',
            dataType : 'json',
            data : { slider_data_id : sliderDataId },
        });
    }

    obj.removeContent = function (contentId) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/home/remove-content',
            type : 'get',
            dataType : 'json',
            data : { content_id : contentId },
        });
    }

    function validateSlider()
    {
        $form = $('#slider_form');

        $form.validate({
            rules: {
                slider_bg_image : {
                    required: true,
                },
                'slider_data_title[]' : {
                    required: true,
                },
                // 'slider_data_image[]' : {
                //     required: true,
                // },
                'slider_data_desc[]' : {
                    required: true,
                },
            },
            submitHandler: function () {
                obj.saveSlider($form);
            },
        });
    }

    function validateCompany()
    {
        $form = $('#company_form');

        $form.validate({
            rules: {
                header : {
                    required: true,
                },
                mission : {
                    required: true,
                },
                vision : {
                    required: true,
                },
                core_values : {
                    required: true,
                },
            },
            submitHandler: function () {
                obj.saveCompany($form);
            },
        });
    }

    function validateContent()
    {
        $form = $('#content_form');

        $form.validate({
            rules: {
                // image: {
                //     required: true;
                // }
            },
            submitHandler: function () {
                obj.saveContent($form);
            },
        });
    }

    function validatePassup()
    {
        $form = $('#passup_form');

        $form.validate({
            rules: {
                // image: {
                //     required: true;
                // }
            },
            submitHandler: function () {
                obj.savePassup($form);
            },
        });
    }

    function showPreview()
    {
        $form = $('#content_form');

        $(".photo").on('change', function() {
            var photo = $(this).next().find('img');
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $(photo).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Image");
            }
        });
    }
});