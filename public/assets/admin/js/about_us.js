$( function( $ ) {
    var obj = {};

    var index= -1000,
        teamIndex= -1000;

    showPreview();

    $(document).on("click", ":submit", function(e) {
        var btn = $(this).val();
        if (btn === 'slider_submit_btn') {
            validateSlider();
        }
        else if (btn === 'team_submit_btn') {
            validateTeam();
        }
    });

    $('#slider_form')
        .on('click', '#add_more_content', function() {
            index++;
            var html = '<div class="form-group"> \
                            <input type="hidden" name="slider_data_id[]" value=""> \
                            <div class="row"> \
                              <div class="pull-right"> \
                                <a href="javascript:void(0)" class="btn btn-danger delete_btn"><i class="fa fa-close"></i></a> \
                              </div> \
                            </div> \
                            <div class="row"> \
                              <div class="col-md-4"> \
                                <label for="">Image</label> \
                                <div class="photo-upload text-center">\
                                  <input type="hidden" name="image[]" value=""> \
                                  <input id="content_photo_'+ index +'" type="file" name="image[]" class="photo"> \
                                  <label for="content_photo_'+ index +'"> \
                                    <div class="image rou"> \
                                      <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" > \
                                      <div class="after"> \
                                      <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                                      </div> \
                                    </div> \
                                  </label>\
                                </div>\
                              </div>\
                              <div class="col-md-8"> \
                                <label for="">Description</label> \
                                <textarea name="description[]" id="textarea_'+ index +'" cols="30" rows="5" class="form-control" placeholder="Description"></textarea> \
                              </div> \
                            </div> \
                          </div>';

            $('#slider_file_last_row').before(html);
            $('#textarea_'+ index +'').wysihtml5({
              toolbar: {
                "font-styles": true,
                "emphasis": true,
                "lists": true,
                "html": true,
                "link": false,
                "image": false,
                "color": false,
                "blockquote": false,
                "size": 'sm'
              },
            });
            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.removeSlider($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    $('#team_form')
        .on('click', '#add_more_content', function() {
            teamIndex++;
            var html = '<div class="form-group"> \
                  <input type="hidden" name="team_id[]" value=""> \
                  <div class="row"> \
                    <div class="pull-right"> \
                      <a href="javascript:void(0)" class="btn btn-danger delete_btn"><i class="fa fa-close"></i></a> \
                    </div> \
                  </div> \
                  <div class="row"> \
                    <div class="col-md-4"> \
                      <label for="">Image</label> \
                      <div class="photo-upload text-center"> \
                        <input type="hidden" name="image[]" id="image" value="'+APP_URL+'/assets/app/images/no_image.jpg"> \
                        <input id="team_photo_'+ teamIndex +'" type="file" name="image[]" class="photo"> \
                        <label for="team_photo_'+ teamIndex +'"> \
                          <div class="image rou"> \
                            <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" id="photo_preview"> \
                            <div class="after"> \
                            <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                            </div> \
                          </div> \
                        </label> \
                      </div> \
                    </div> \
                    <div class="col-md-7"> \
                      <label for="">Basic Information</label> \
                      <div class="input-group"> \
                        <span class="input-group-addon"> \
                            <i class="fa fa-user"></i> \
                        </span> \
                        <input type="text" class="form-control input-sm" placeholder="Full Name" name="fullname[]"> \
                      </div> \
                      <div class="input-group"> \
                        <span class="input-group-addon"> \
                          <i class="fa fa-universal-access"></i> \
                        </span> \
                        <input type="text" class="form-control input-sm" placeholder="Position" name="position[]""> \
                      </div> \
                      <label for="">Social Media</label> \
                      <div class="input-group"> \
                        <span class="input-group-addon"> \
                            <i class="fa fa-twitter-square"></i> \
                        </span> \
                        <input type="text" class="form-control input-sm" name="twitter[]" placeholder="e.g. www.twitter.com/tamayo" /> \
                      </div> \
                      <div class="input-group"> \
                        <span class="input-group-addon"> \
                            <i class="fa fa-facebook-square"></i> \
                        </span> \
                        <input type="text" class="form-control input-sm" name="facebook[]" placeholder="e.g. www.facebook.com/tamayo" /> \
                      </div> \
                      <div class="input-group"> \
                        <span class="input-group-addon"> \
                            <i class="fa fa-google-plus-square"></i> \
                        </span> \
                        <input type="text" class="form-control input-sm" name="google_plus[]" placeholder="e.g. www.plus.google.com/tamayo" /> \
                      </div> \
                    </div> \
                  </div> \
                </div> \
            </div>';

            $('#team_last_row').before(html);

            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.removeTeam($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    obj.saveSlider = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/about-us/save-slider',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "About us Page");
                }
                else {
                    toastr.error('Error', "About us Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.saveTeam = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/about-us/save-team',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "About us Page");
                }
                else {
                    toastr.error('Error', "About us Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.removeSlider = function (sliderDataId) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/about-us/remove-slider',
            type : 'get',
            dataType : 'json',
            data : { slider_data_id : sliderDataId },
        });
    }

    obj.removeTeam = function (teamId) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/about-us/remove-team',
            type : 'get',
            dataType : 'json',
            data : { team_id : teamId },
        });
    }

    function validateSlider()
    {
        $form = $('#slider_form');

        $form.validate({
            submitHandler: function () {
                obj.saveSlider($form);
            },
        });
    }

    function validateTeam()
    {
        $form = $('#team_form');

        $form.validate({
            submitHandler: function () {
                obj.saveTeam($form);
            },
        });
    }

    function showPreview()
    {
        $form = $('#slider_form');

        $(".photo").on('change', function() {
            var photo = $(this).next().find('img');
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $(photo).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Image");
            }
        });
    }
});