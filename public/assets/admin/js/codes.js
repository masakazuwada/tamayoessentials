$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'void' :
                obj.void();
                break;
        }
    });

    obj.codes = $('#codes').DataTable({
        'processing': true,
        'responsive': true,
        'order': false,
        'ajax': {
            "url": APP_URL + '/administration/code/get'
        },
        'columns': [
            { 'data': 'code', 'sClass': 'text-center' },
            { 'data': 'used_by', 'sClass': 'text-center' },
            {
                'data': 'status',
                'sClass': 'text-center',
                'render': function(data, type, row) {
                    switch(data) {
                        case 'void' : return 'Void'; break;
                        case 'not_available' : return 'Not Available'; break;
                        case 'expired' : return 'Expired'; break;
                        default : return data; break;
                    }
                }
            },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void()" data-bind="confirmation" data-method="void" data-id="'+ row.code_id +'" class="btn btn-danger btn-xs '+ ((row.status == 'void' || row.status == 'not_available' || row.status == 'expired') ? 'disabled' : '')+'" title="Void"> \
                                    <i class="fa fa-close"></i> Void \
                                </a>';
                return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/code/save',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.codes.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Codes");
                }
                else {
                    toastr.error('Error', "Codes");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.void = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/administration/code/void',
            type : 'get',
            dataType : 'json',
            data : { code_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Voided!', "Codes");
                }
                else {
                    toastr.error('Error', "Codes");
                }
                obj.codes.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateGenerate($form, $modal) {
        var msg = 'Generated';
        $form.validate({
            rules: {
                no_to_generate : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'generate' :
                obj.getModalContent(ajaxURL, $smallModal).done(function () {
                    validateGenerate($smallModal.find('form'), $smallModal);
                    $smallModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
           case 'void' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Delete Code");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }
});
