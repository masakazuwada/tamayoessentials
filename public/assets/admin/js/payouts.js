$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.type = $(this).data('type');
        obj.value = $(this).data('value');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    obj.payouts = $('#payouts').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/administration/transactions/payout/get'
        },
        'columns': [
            { 'data': 'request_date', 'sClass': 'text-center' },
            { 'data': 'member', 'sClass': 'text-center' },
            { 
                'data': 'type',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data == 1) {
                        return '<span style="color: #1ABB9C">QSR</span>'
                    }
                    return '<span style="color: #ff7a00">QSP</span>'
                }
            },
            { 'data': 'amount', 'sClass': 'text-right' },
            { 'data': 'reviewed_by', 'sClass': 'text-left none' },
            { 'data': 'reviewed_on', 'sClass': 'text-left none' },
            { 'data': 'remarks', 'sClass': 'text-left none' },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    if (row.status == 1) {
                        return '<i class="fa fa-check" title="Approved"></i>';
                    } else if (row.status == 2) {
                        return '<i class="fa fa-close" title="Rejected"></i>';
                    } else {
                        return  '<a href="javacript:void(0)" data-bind="payout_confirmation" data-method="confirmation" data-id="'+ row.payout_id +'" data-value="2" class="btn btn-danger btn-xs" title="Discard"> \
                                        <i class="fa fa-close"></i> Reject \
                                    </a> \
                                    <a href="javacript:void(0)" data-bind="payout_confirmation" data-method="confirmation" data-id="'+ row.payout_id +'" data-value="1" class="btn btn-success btn-xs" title="Approve"> \
                                        <i class="fa fa-check"></i> Approve \
                                    </a>';
                        
                    }
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
            <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/transactions/payout/save',
            type : 'post',
            data : $form.serialize(),
            dataType : 'json',
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.payouts.ajax.reload();
                    toastr.success('Success', "Payouts");
                }
                else {
                    toastr.error('Error', "Payouts");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateConfirmation($form, $modal) {
        var msg = '';
        $form.validate({
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $fullscreenModal = $('#fullscreen'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/member/pass-up/app/' + obj.url;

        switch(obj.method) {
            case 'confirmation' :
                obj.getModalContent(ajaxURL, $smallModal).done( function () {
                    $('[name=payout_id]').val(obj.id);
                    $('[name=status]').val(obj.value);
                    if (obj.value == 2) {
                        $('#title').text('Reject Payout');
                        $('[type=submit]').text('Reject');
                        $('[type=submit]').removeClass('btn-success').addClass('btn-danger');
                    }
                    validateConfirmation($smallModal.find('form'), $smallModal);
                    $smallModal.modal({
                        show : true,
                    });
                });
                break;
        }
    }
});
