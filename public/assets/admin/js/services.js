$( function( $ ) {
    var obj = {};

    var index= -1000;

    validateContent();
    showPreview();

    $(document).on("click", ":submit", function(e) {
        var btn = $(this).val();
        if (btn === 'slider_submit_btn') {
            validateSlider();
        }
        else if (btn === 'content_submit_btn') {
            validateContent();
        }
    });

    $('#slider_form')
        .on('click', '#add_more_content', function() {
            index++;
            var html = '<div class="form-group"> \
                            <input type="hidden" name="slider_data_id[]" value=""> \
                            <div class="row"> \
                              <div class="pull-right"> \
                                <a href="javascript:void(0)" class="btn btn-danger delete_btn"><i class="fa fa-close"></i></a> \
                              </div> \
                            </div> \
                            <div class="row"> \
                              <div class="col-md-4"> \
                                <label for="">Image</label> \
                                <div class="photo-upload text-center">\
                                  <input type="hidden" name="image[]" value=""> \
                                  <input id="slider_photo_'+ index +'" type="file" name="image[]" class="photo"> \
                                  <label for="slider_photo_'+ index +'"> \
                                    <div class="image rou"> \
                                      <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" > \
                                      <div class="after"> \
                                      <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                                      </div> \
                                    </div> \
                                  </label>\
                                </div>\
                              </div>\
                              <div class="col-md-8"> \
                                <label for="">Description</label> \
                                <textarea name="description[]" id="textarea_'+ index +'" cols="30" rows="5" class="form-control" placeholder="Description"></textarea> \
                              </div> \
                            </div> \
                          </div>';

            $('#slider_file_last_row').before(html);
            $('#textarea_'+ index +'').wysihtml5({
              toolbar: {
                "font-styles": true,
                "emphasis": true,
                "lists": true,
                "html": true,
                "link": false,
                "image": false,
                "color": false,
                "blockquote": false,
                "size": 'sm'
              },
            });
            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.removeSlider($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    $('#content_form')
        .on('click', '#add_more_content', function() {
            index++;

            var html = '<div class="form-group"> \
                            <input type="hidden" name="content_id[]"> \
                            <div class="row"> \
                              <div class="pull-right"> \
                                <a href="javascript:void(0)" class="btn btn-danger delete_btn"><i class="fa fa-close"></i></a> \
                              </div> \
                            </div> \
                            <div class="row"> \
                              <div class="col-md-4"> \
                                <label for="">Image</label> \
                                <div class="photo-upload text-center">\
                                  <input type="hidden" name="image[]" value=""> \
                                  <input id="content_photo_'+ index +'" type="file" name="image[]" class="photo"> \
                                  <label for="content_photo_'+ index +'"> \
                                    <div class="image rou"> \
                                      <img class="round" src="'+APP_URL+'/assets/app/images/no_image.jpg" title="Upload Image" > \
                                      <div class="after"> \
                                      <i class="fa fa-plus-circle" id="upload_icon" title="Upload Image"></i> \
                                      </div> \
                                    </div> \
                                  </label>\
                                </div>\
                              </div>\
                              <div class="col-md-8"> \
                                <label for="">Description</label> \
                                <textarea name="description[]" id="textarea_'+ index +'" cols="30" rows="5" class="form-control" placeholder="Description"></textarea> \
                              </div> \
                            </div> \
                          </div>';

            $('#content_file_last_row').before(html);
            $('#textarea_'+ index +'').wysihtml5({
              toolbar: {
                "font-styles": true,
                "emphasis": true,
                "lists": true,
                "html": true,
                "link": false,
                "image": false,
                "color": false,
                "blockquote": false,
                "size": 'sm'
              },
            });
            showPreview();
        })

        .on('click', '.delete_btn', '[data-id]', function() {
            var $row  = $(this).closest('.form-group'),
                id = $(this).data('id');
            if (id != null) {
                obj.remove($(this).data('id')).done(function() {
                    $row.remove();
                });
            }
            $row.remove();
        });

    obj.saveContent = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/services/save-content',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Services Page");
                }
                else {
                    toastr.error('Error', "Services Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function (content_id) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/services/remove-line',
            type : 'get',
            dataType : 'json',
            data : { content_id : content_id },
        });
    }

    function validateContent()
    {
        $form = $('#content_form');

        $form.validate({
            rules: {
                // image: {
                //     required: true;
                // }
            },
            submitHandler: function () {
                obj.saveContent($form);
            },
        });
    }

        obj.saveSlider = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/services/save-slider',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Marketing Plan Page");
                }
                else {
                    toastr.error('Error', "Marketing Plan Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.removeSlider = function (sliderDataId) {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/marketing-plan/remove-slider',
            type : 'get',
            dataType : 'json',
            data : { slider_data_id : sliderDataId },
        });
    }

    function validateSlider()
    {
        $form = $('#slider_form');

        $form.validate({
            submitHandler: function () {
                obj.saveSlider($form);
            },
        });
    }

    function showPreview()
    {
        $form = $('#content_form');

        $(".photo").on('change', function() {
            var photo = $(this).next().find('img');
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $(photo).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Image");
            }
        });
    }
});