$( function( $ ) {
    var obj = {};

    validateContent();

    obj.saveContent = function ($form)
    {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/site-pages/contacts/save-content',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully saved changes!', "Contact Page");
                }
                else {
                    toastr.error('Error', "Contact Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateContent()
    {
        $form = $('form');

        $form.validate({
            rules : {
                pri_email : {
                    required: true,
                    email: true
                },
                sup_email : {
                    required: true,
                    email: true
                }
            },
            submitHandler: function () {
                obj.saveContent($form);
            },
        });
    }
});