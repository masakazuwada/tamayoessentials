$( function ($) {
    var obj = {};

    showPreview();

    validateUpdateDetails();

    $(document).on( 'click', '[data-toggle]', function (e) {
        e.preventDefault();

        var tab = $(this).data('tab');
        if (tab == 'password') {
            validateUpdatePassword();
        } else {
            validateUpdateDetails();
        }
    });

    obj.updateDetails = function ($form) {
        var $submit = $form.find('[type="submit"]');
        console.log($submit);
        $.ajax({
            url : APP_URL + '/administration/account/update-details',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Updated!', "Account");
                }
                else {
                    toastr.error('Error', "Account");
                }
                $submit.text('Submit').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.updatePassword = function ($form, action) {
        var $submit = $form.find('[type="submit"]');
        console.log($submit);
        $.ajax({
            url : APP_URL + '/administration/account/update-password',
            type : 'post',
            data : $form.serialize(),
            dataType : 'json',
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Updated!', "Password");
                }
                else {
                    toastr.error('Error', "Password");
                }
                $('[name=current_password]').val('');
                $('[name=password]').val('');
                $('[name=confirm_password]').val('');
                $submit.text('Submit').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateUpdateDetails() {
        var $form = $('#account_form');
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
            },
            submitHandler: function () {
                obj.updateDetails($form);
            },
        });
    }

    function validateUpdatePassword() {
        var $form = $('#password_form');
        $form.validate({
            rules: {
                current_password: {
                    required : true,
                    remote : {
                        url : APP_URL + "/app/ajax-check-hash-if-equal",
                        type : "post",
                        data: {
                            _token: function() {
                                return $( '[name="_token"]' ).val();
                            }
                        }
                    }
                },
                password: {
                    minlength : 6,
                    required: true,
                    notEqualTo : '#current_password'
                },
                confirm_password: {
                    required: true,
                    equalTo : '#password'
                }
            },
            messages : {
                current_password : {
                    remote : "Incorrect current password."
                },
                password : {
                    notEqualTo : "Please choose different password."
                },
                confirm_password : {
                    equalTo : "Password does not match." 
                }
            },
            submitHandler: function(){
                obj.updatePassword($form);
            }
        });
    }

    function showPreview()
    {
        $(".photo").on('change', function() {
            var photo = $(this).next().find('img');
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $(photo).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Image");
            }
        });
    }
});
