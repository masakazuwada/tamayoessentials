$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    obj.products = $('#products').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'ajax': {
            "url": APP_URL + '/administration/transactions/product/get'
        },
        'columns': [
            { 'data': 'code', 'sClass': 'text-center' },
            { 
                'data': 'image',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    return '<a href="'+ APP_URL + data +'" target="_blank"> \
                        <img src="'+ APP_URL + data +'" height="20px" width="20px" title="See full image"/></a>';
                }
            },
            { 'data': 'product' },
            { 'data': 'original_price', 'sClass': 'text-right' },
            { 'data': 'selling_price', 'sClass': 'text-right' },
            {
                'data': 'description',
                'render': function (data, type, row) {
                    if (data != null) {
                        return (data.replace(/\n/g, "")).substring(0, 100); 
                    }
                    return data;
                }
            },
            { 'data': 'actions', 'sClass': 'text-center' }
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void(0)" data-bind="product_add_edit" data-method="update" data-id="'+ row.product_id +'" class="btn btn-info btn-xs" title="Edit"> \
                                    <i class="fa fa-edit"></i> Edit \
                                </a> \
                                <a href="javacript:void(0)" data-bind="confirmation" data-method="delete" data-id="'+ row.product_id +'" class="btn btn-danger btn-xs" title="Delete"> \
                                    <i class="fa fa-trash-o"></i> Delete \
                                </a>';
                return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/transactions/product/save',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.products.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Products Page");
                }
                else {
                    toastr.error('Error', "Products Page");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');

        return $.ajax({
            url : APP_URL + '/administration/transactions/product/get-details',
            type : 'get',
            dataType : 'json',
            data : { product_id: id },
            success: function (result) {
                $.each( result, function (index, value) {
                    switch (index) {
                        case 'image' :
                            $form.find('#image').val(value);
                            $form.find('[id=photo_preview]').prop('src', (APP_URL + value));
                            break;
                        case 'advertised':
                            $form.find('#advertised').prop('checked', value);
                            break;
                        default :
                            $form.find('[name=' + index + ']').val(value);
                            break;
                    }
                });
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/administration/transactions/product/remove',
            type : 'get',
            dataType : 'json',
            data : { product_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Products Page");
                }
                else {
                    toastr.error('Error', "Products Page");
                }
                obj.products.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }  

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateCreate($form, $modal) {
        var msg = 'Added';
        $form.validate({
            rules: {
                code: {
                    required: true,
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability',
                        type : 'get',
                        data : { table: 'te_products', key_name : 'code' },
                    }
                },
                product : {
                    required: true,
                },
                original_price : {
                    required: true,
                },
                selling_price : {
                    required: true,
                }
            }, 
            messages : {
                code : {
                    remote : "Please choose different product code."
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            }
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            ignore : [],
            rules: {
                code: {
                    required: true,
                },
                product : {
                    required: true,
                },
                original_price : {
                    required: true,
                },
                selling_price : {
                    required: true,
                }
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            }
        });
    }

    function showPreview($form)
    {
        $("#photo").on('change', function() {
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $($form.find('img')).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Products Page");
            }
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'insert' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    showPreview($mediumModal);
                    validateCreate($mediumModal.find('form'), $mediumModal);
                    $mediumModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    showPreview($mediumModal);
                    getDetailsById($mediumModal, obj.id).done(function () {
                        $("[name=code]").prop("readonly", true);
                        validateUpdate($mediumModal.find('form'), $mediumModal);
                        $mediumModal.modal({
                            show : true,
                            backdrop: 'static',
                        });
                    });
                })
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text('Delete Product');
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }
});
