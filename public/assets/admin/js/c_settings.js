$( function ($) {
    var obj = {};

    getCompanyItemForDropdown();
    validate();

    $('#items').on( 'change', function() {
        var key = $(this).val();
        if (key) {
            getSettingValueByKey(key);
        } else {
            $('[name=setting_id]').val(null);
            $('[name=value]').val(null);
            $('[name=description]').val(null);
        }
    });

    obj.c_settings = $('#c_settings').DataTable({
        'processing': true,
        'responsive': true,
        'filter' : false,
        'paging' : false,
        'ajax': {
            "url": APP_URL + '/administration/settings/get-company'
        },
        'columns': [
            { 'data': 'name' },
            { 'data': 'value', 'sClass': 'text-center' },
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        },
        'createdRow': function(row, data, index) {
            var row = $(row);
            row.attr('data-toggle', 'tooltip');
            row.attr('title', data['description']);
        }
    }).on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/settings/save',
            type : 'post',
            dataType : 'json',
            data: $form.serialize(),
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.c_settings.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Settings");
                }
                else {
                    toastr.error('Error', "Settings");
                }
                $submit.text('Set').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getCompanyItemForDropdown()
    {
        var $items = $('#items');

        return $.ajax({
            url : APP_URL + '/administration/settings/get-company-items-for-dropdown',
            type : 'get',
            dataType : 'json',
            beforeSend: function() {
                var $loader = $('<option />');
                $loader.attr('value', '').text('Fetching...').prop('selected', true);
                $items.append($loader);
            },
            success: function(result) {
                if(result instanceof Array) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('Select Item to Set');
                    
                    $items.empty();
                    $items.append($select);

                    $.each(result, function(index, value) {
                        var $option = $('<option />');
                        $option.attr('value', result[index].key).text(result[index].name);
                        $items.append($option);
                    });
                }
            },
            error : function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getSettingValueByKey(key)
    {
        var $form = $('form'),
            flag = false;

        return $.ajax({
            url : APP_URL + '/app/get-data',
            type : 'get',
            dataType : 'json',
            data : { table : 'te_settings', pkey: 'key', id : key },
            success: function(result) {
                $.each(result, function(index, value) {
                    $.each(value, function(index2, value2) {
                        $form.find('[name=' + index2 + ']').val(value2);
                        flag = true;
                    });
                });
                if (!flag) {
                    $('[name=setting_id]').val(null);
                    $('[name=value]').val(null);
                }
            },
            error : function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validate()
    {
        var $form = $('form');

        $form.validate({
            rules : {
                key : {
                    required : true
                },
                value : {
                    required : true,
                },
            },
            submitHandler: function() {
                obj.save($form);
            }
        });
    }
    
});
