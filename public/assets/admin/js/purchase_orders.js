$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;
        }
    });

    $(document).on('change', '.product_select', function () {
        var value = $(this).val(),
            $price = $(this).parent().parent().parent().find('.selling_price');

        if (value) {
            $.ajax({
                url: APP_URL + '/administration/transactions/product/get-details-for-select',
                type: 'get',
                dataType: 'json',
                data: { product_id: value },
                success: function (result) {
                    $price.val(result.selling_price);
                    var price = result.selling_price;

                    $('.selling_price').on('input',function(e){
                        discount = $(this).parent().parent().parent().find('.discount').val();
                        if (discount > 0) {
                            $discount = $(this).parent().parent().parent().find('.discount');
                            $discount.val(-($(this).val() - result.selling_price));
                        }
                    });

                    $('.discount').on('input',function(e){
                        $price = $(this).parent().parent().parent().find('.selling_price');
                        $price.val(price - $(this).val());
                    });
                }
            })
        }
        else {
            $price.val("");
        }
    });

    function populateTemplate($modal, _productID, _quantity, _discount, _price)
    {
        return $.ajax({
            // url : APP_URL + '/product/get-for-dropdown',
            url : APP_URL + '/administration/transactions/product/get-for-dropdown',
            type : 'get',
            dataType : 'json',
            success: function (result) {
                var addButton = $('.add_button'),
                html = '<div class="sub_row"><div class="row"> \
                    <div class="col-md-5 col-xs-11"> \
                      <span class="small_50 visible-xs">Product Name (Code)</span> \
                      <div class="form-group"> \
                        <select name="product[]" class="form-control input-sm product_select"> ';
                            if(result instanceof Array) {
                                html += '<option value="">Select Product</option> ';
                                $.each(result, function (index, value) {
                                    html += '<option value="'+ result[index].product_id +'" '+ (result[index].product_id == _productID ? 'selected' : "") +'>'+ result[index].product +' ('+ result[index].code +')</option> ';
                                });
                            }
                        html += '</select> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Quantity</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm" name="quantity[]" min="0" placeholder="₱" value="'+ _quantity +'"> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Discount</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm" name="discount[]" min="0" max="100" placeholder="%" value="'+ _discount +'"> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Price</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm selling_price" name="price[]" min="0" placeholder="₱" value="'+ _price +'"> \
                      </div>  \
                    </div> \
                    <div class="col-md-1"> \
                      <button type="button" class="btn btn-danger btn-xs delete_button"> \
                        <i class="fa fa-close"></i> \
                      </button> \
                    </div> \
                </div> \
                <hr class="visible-xs" style="margin-top: 0; border: 0; border-top: 1px solid #eee;"></div>';

                $($modal.find('#purchase_order_line')).append(html);
                $($modal).find('.delete_button').first().prop('disabled', true);
                obj.select2('.product_select');
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    function duplicateTemplate($modal, status)
    {
        return $.ajax({
            url : APP_URL + '/administration/transactions/product/get-for-dropdown',
            type : 'get',
            dataType : 'json',
            success: function (result) {
                var addButton = $('.add_button'),
                html = '<div class="sub_row"><div class="row"> \
                    <div class="col-md-5 col-xs-11"> \
                      <span class="small_50 visible-xs">Product Name (Code)</span> \
                      <div class="form-group"> \
                        <select name="product[]" class="form-control input-sm product_select"> ';
                            if(result instanceof Array) {
                                    html += '<option value="">Select Product</option> ';
                                $.each(result, function (index, value) {
                                    html += '<option value="'+ result[index].product_id +'" >'+ result[index].product +' ('+ result[index].code +')</option> ';
                                });
                            }
                        html += '</select> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Quantity</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm quantity" name="quantity[]" min="0" placeholder="Qty" value="1"> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Discount</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm" name="discount[]" min="0" max="100" placeholder="%" value="0"> \
                      </div> \
                    </div> \
                    <div class="col-md-2 col-xs-4"> \
                      <span class="small_50 visible-xs">Price</span> \
                      <div class="form-group"> \
                        <input type="number" class="form-control input-sm selling_price" name="price[]" min="0" placeholder="₱"> \
                      </div> \
                    </div> \
                    <div class="col-md-1"> \
                      <button type="button" class="btn btn-danger btn-xs delete_button"> \
                        <i class="fa fa-close"></i> \
                      </button> \
                    </div> \
                </div> \
                <hr class="visible-xs" style="margin-top: 0; border: 0; border-top: 1px solid #eee;"></div>';
                
                $(addButton).click(function () {
                    $($modal.find('#purchase_order_line')).append(html);
                    obj.select2('.product_select');
                });

                $('#purchase_order_line').on('click', '.delete_button', '[data-id]', function() {
                    var $row  = $(this).closest('.sub_row');
                    // var order_line_id = $(this).data('id');
                    $row.remove();
                });

                if (status) {
                    $($modal.find('#purchase_order_line')).append(html);
                }
                // disable first element of remove button
                $($modal).find('.delete_button').first().prop('disabled', true);
                obj.select2('.product_select');
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            },
        });
    }

    obj.purchase_orders = $('#purchase_orders').DataTable({
        'processing': true,
        'responsive': true,
        'order': [],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/administration/transactions/purchase-order/get',
            'error': function () {
                toastr.error("Failed to load data, please try again!", "Purchase Orders");
            }
        },
        'columns': [
            { 'data': 'date_purchased', 'sClass': 'text-center' },
            { 'data': 'products', 'sClass': 'text-left' },
            { 'data': 'customer', 'sClass': 'text-left' },
            { 'data': 'total', 'sClass': 'text-right' },
            {
                'data': 'details',
                'render': function (data, type, row) {
                    if (data != null) {
                        return data.replace(/\n/g, '<br>'); 
                    }
                    return data;
                }
            },
            { 'data': 'actions', 'sClass': 'text-center' }
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void(0)" data-bind="purchase_order_lines_view" data-method="view" data-id="'+ row.purchase_order_id +'" data-order_line_id="'+ row.order_line_id +'" class="btn btn-success btn-xs" title="View Lines"> \
                                    <i class="fa fa-eye"></i> View \
                                </a> \
                                <a href="javacript:void(0)" data-bind="purchase_order_add_edit" data-method="update" data-id="'+ row.purchase_order_id +'" data-order_line_id="'+ row.order_line_id +'" class="btn btn-info btn-xs" title="Edit"> \
                                    <i class="fa fa-edit"></i> Edit \
                                </a> \
                                <a href="javacript:void(0)" data-bind="confirmation" data-method="delete" data-id="'+ row.purchase_order_id +'" class="btn btn-danger btn-xs" title="Delete"> \
                                    <i class="fa fa-trash-o"></i> Delete \
                                </a>';
                return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        },
        'createdRow': function(row, data, index) {
            var row = $(row);
            row.attr('data-toggle', 'tooltip');
            row.attr('title', 'Encoded by: '+ data['encoder'] + "\n" + 'Timestamp: '+ (data['timestamp'] != null ? data['timestamp'] : data['date_collected']));
        }
    }).on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function ajaxGetLines($modal)
    {
        obj.purchase_order_lines = $('#purchase_order_lines').DataTable({
            'processing': true,
            'responsive': true,
            'filter': false,
            'order': [],
            'ajax': {
                'url' : APP_URL + '/administration/transactions/purchase-order/get-lines',
                'data': function(d) {
                    d.id = obj.id;
                },
                'error': function () {
                    toastr.error("Failed to load data, please try again!", "Orders");
                }
            },
            'columns': [
                { 
                    'data': 'image',
                    'sClass': 'text-center',
                    'render': function (data, type, row) {
                        return '<a href="'+ (APP_URL + data) +'" target="_blank"> \
                        <img src="'+ (APP_URL + data) +'" height="20px" width="20px" title="See full image" alt="'+ row.product +'"/></a>';
                    }
                },
                { 'data': 'product' },
                { 'data': 'quantity', 'sClass': 'text-center' },
                { 
                    'data': 'discount',
                    'sClass': 'text-center',
                    'render' : function (data, type, row) {
                        return  data + '%';
                    }
                },
                { 'data': 'price', 'sClass': 'text-right' },
                { 'data': 'sub_total', 'sClass': 'text-right' },
                ]
            });
    }

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/transactions/purchase-order/save',
            type : 'post',
            dataType : 'json',
            data: $form.serialize(),
            beforeSend : function () {
                $submit.html('<i class="fa fa-spinner fa-spin fa-fw"></i> Please wait').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.purchase_orders.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Purchase Orders");
                }
                else {
                    toastr.error('Error', "Purchase Orders");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');

        return $.ajax({
            url : APP_URL + '/administration/transactions/purchase-order/get-details',
            type : 'get',
            dataType : 'json',
            data : { purchase_order_id: id },
            success: function (result) {
                $.each(result, function (tepo_index, tepo_value) {
                    switch (tepo_index) {
                        case 'purchase_order_lines': 
                            $.each(tepo_value, function (tepol_index, tepol_value) {
                                populateTemplate($modal,
                                                tepo_value[tepol_index].product_id,
                                                tepo_value[tepol_index].quantity,
                                                tepo_value[tepol_index].discount,
                                                tepo_value[tepol_index].price);
                            });
                            break;
                        case 'customer_type':
                            $modal.find($('#customer')).empty();
                            if (tepo_value == 1) {
                                var selectInput = '<select name="customer" class="form-control input-sm" id="pm_customer"></select>';
                                $modal.find($('#customer')).append(selectInput);
                                $form.find('#pass_up_member').prop('checked', true);
                            } else {
                                var textInput = '<input type="text" class="form-control input-sm" name="customer" placeholder="Customer" id="wi_customer">';
                                $modal.find($('#customer')).append(textInput);
                                $form.find('#walk_in').prop('checked', true);
                            }
                            break;
                        case 'customer':
                            $form.find('[name=' + tepo_index + ']').val(tepo_value);
                            obj.getMemberForDropdown($modal, tepo_value);
                            break;
                        default:
                            $form.find('[name=' + tepo_index + ']').val(tepo_value);
                            break;
                    }
                });
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    obj.getMemberForDropdown = function($modal, id = null)
    {
        var $member = $modal.find('#pm_customer');

        return $.ajax({
            url : APP_URL + '/administration/member/get-for-dropdown',
            type : 'get',
            dataType : 'json',
            beforeSend: function() {
                var $loader = $('<option />');
                $loader.attr('value', '').text('Fetching...').prop('selected', true);
                $member.append($loader);
            },
            success: function (result) {
                if (result instanceof Array) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('Select Member');
                    $member.empty();
                    $member.append($select);
                    $.each(result, function(index, value) {
                        var $option = $('<option />');
                        $option.attr('value', result[index].member_id).text(result[index].member_id + ' - ' +result[index].full_name);
                        if (result[index].member_id == id) {
                            $option.attr('selected',  true);
                        }
                        $member.append($option);
                    });
                }
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Order");
            },
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/administration/transactions/purchase-order/remove',
            type : 'get',
            dataType : 'json',
            data : { purchase_order_id : obj.id },
            beforeSend : function () {
                $yBtn.html('<i class="fa fa-spinner fa-spin fa-fw"></i> Please wait').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Purchase Orders");
                }
                else {
                    toastr.error('Error', "Purchase Orders");
                }
                obj.purchase_orders.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            // beforeSend: function () {
            //     $.spin('true');
            // },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                toastr.error("Error, please try again!", "Purchase Orders");
            }
        });
    }

    function validateCreate($form, $modal) {
        var msg = 'Added';
        $form.validate({
            rules: {
                'product[]' : {
                    required: true,
                },
                'quantity[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                'discount[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                'price[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                customer_type : {
                    required: true
                },
                customer : {
                    required: true
                },
                date_purchased : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
            errorPlacement: function (error, element) {
                if (element.hasClass("select2-hidden-accessible")) {
                    error.insertAfter(element.next('span.select2')); 
                } else {                                      
                    error.insertAfter(element);
                }
            }
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            rules: {
                'product[]' : {
                    required: true,
                },
                'quantity[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                'discount[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                'price[]' : {
                    required: true,
                    number: true,
                    min: 0,
                },
                customer_type : {
                    required: true
                },
                customer : {
                    required: true
                },
                date_purchased : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
            errorPlacement: function (error, element) {
                if (element.hasClass("select2-hidden-accessible")) {
                    error.insertAfter(element.next('span.select2')); 
                } else {                                      
                    error.insertAfter(element);
                }
            }
        });
    }

    function toogleCustomer($modal)
    {
        $modal.find('#walk_in').prop('checked', true);

        $(document).on('change', '[name=customer_type]', function () {
            $modal.find($('#customer')).empty();
            var type = $(this).val();
            if (type == 1) {
                var selectInput = '<select name="customer" class="form-control input-sm" id="pm_customer"></select>';
                $modal.find($('#customer')).append(selectInput);
                obj.getMemberForDropdown($modal);

            } else {
                var textInput = '<input type="text" class="form-control input-sm" name="customer" placeholder="Customer" id="wi_customer">';
                $modal.find($('#customer')).append(textInput);
            }
        });
    }

    function loadModal()
    {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'view' : 
                obj.getModalContent(ajaxURL, $modal).done(function () {
                    ajaxGetLines($modal);
                    $modal.on('shown.bs.modal', function (e) {
                      obj.purchase_order_lines.columns.adjust().responsive.recalc();
                  });
                    $modal.modal('show');
                });
                break;
            case 'insert' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    toogleCustomer($mediumModal);
                    duplicateTemplate($mediumModal, true).done(function () {
                        validateCreate($mediumModal.find('form'), $mediumModal);
                        $mediumModal.modal({
                            show : true,
                            backdrop: 'static',
                        });
                    });
                });
                break;
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    toogleCustomer($mediumModal);
                    getDetailsById($mediumModal, obj.id).done(function () {
                        duplicateTemplate($mediumModal, false).done(function () {
                            validateUpdate($mediumModal.find('form'), $mediumModal);
                            $mediumModal.modal({
                                show : true,
                                backdrop: 'static',
                            });
                        });
                    })
                })
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text('Purchase Orders');
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                    $.spin('false');
                });
                break;
        }
    }
    
    obj.select2 = function(selector)
    {
        $(selector).select2({
            width: '100%',
            dropdownParent: obj.modal,
        });
    }
});
