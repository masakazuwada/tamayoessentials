$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;            
            case 'reset' :
                obj.resetPassword();
                break;
        }
    });

    obj.users = $('#users').DataTable({
        'processing': true,
        'responsive': true,
        'order': [[0, 'DESC']],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/administration/user/get'
        },
        'columns': [
            { 
                'data': 'image',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    return '<a href="'+ APP_URL + data +'" target="_blank"> \
                        <img src="'+ APP_URL + data +'" height="20px" width="20px" title="See full image"/></a>';
                }
            },
            { 'data': 'email', 'sClass': 'text-center' },
            { 'data': 'full_name', 'sClass': 'text-center' },
            { 
                'data': 'type',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data == 1) {
                        return 'Adminstrator';
                    }
                    else if (data == 2) {
                        return 'User';
                    }
                    return null;
                }
            },
            { 
                'data': 'status',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data) {
                        return 'Active';
                    }
                    return 'Inactive';
                }
            },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void(0)" data-bind="confirmation" data-method="reset" data-id="'+ row.user_id +'" class="btn btn-default btn-xs" title="Reset Password"> \
                                    <i class="fa fa-refresh"></i> Reset \
                                </a> \
                                <a href="javacript:void(0)" data-bind="user_add_edit" data-method="update" data-id="'+ row.user_id +'" class="btn btn-info btn-xs" title="Edit"> \
                                    <i class="fa fa-edit"></i> Edit \
                                </a> \
                                <a href="javacript:void(1)" data-bind="confirmation" data-method="delete" data-id="'+ row.user_id +'" class="btn btn-danger btn-xs" title="Delete"> \
                                    <i class="fa fa-trash-o"></i> Delete \
                                </a>';
                return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/user/save',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.users.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Users");
                }
                else {
                    toastr.error('Error', "Users");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.resetPassword = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        return $.ajax({
            url : APP_URL + '/administration/user/reset-password',
            type : 'get',
            dataType : 'json',
            data : { user_id: obj.id },
            success: function (result) {
                if(result.status) {
                    obj.users.ajax.reload();
                    toastr.success('Password has been reset successfully.', "Users");
                }
                else {
                    toastr.error('Error', "Users");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');

        return $.ajax({
            url : APP_URL + '/administration/user/get-details',
            type : 'get',
            dataType : 'json',
            data : { user_id: id },
            success: function (result) {
                $.each( result, function (index, value) {
                    switch (index) {
                        case 'image' :
                            $form.find('#image').val(value);
                            $form.find('[id=photo_preview]').prop('src', APP_URL + value);
                            break;
                        default :
                            $form.find('[name=' + index + ']').val(value);
                            break;
                    }
                });
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/administration/user/remove',
            type : 'get',
            dataType : 'json',
            data : { user_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Users");
                }
                else {
                    toastr.error('Error', "Users");
                }
                obj.users.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateCreate($form, $modal) {
        var msg = 'Added';
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email : {
                    required: true,
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability',
                        type : 'get',
                        data : { table: 'te_users', key_name : 'email' },
                    },
                    email : true
                },
                password : {
                    required: true,
                },
                type : {
                    required: true
                },
                status : {
                    required: true
                },
            },
            messages : {
                email : {
                    remote : "Please choose different email."
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email : {
                    required: true,
                    email : true,
                },
                type : {
                    required: true
                },
                status : {
                    required: true
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function showPreview($form)
    {
        $("#photo").on('change', function() {
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $($form.find('img')).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Members");
            }
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'insert' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    showPreview($mediumModal);
                    obj.toggleShowPassword({
                        field: '#password_input',
                        control: '#show_password'
                    });
                    validateCreate($mediumModal.find('form'), $mediumModal);
                    $mediumModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    getDetailsById($mediumModal, obj.id).done(function () {
                        $('[name=password]').prop('placeholder', "Leave blank if you don't want to change");
                        $('[name=email]').attr('readonly', 'true');
                        showPreview($mediumModal);
                        obj.toggleShowPassword({
                            field: '#password_input',
                            control: '#show_password'
                        });
                        validateUpdate($mediumModal.find('form'), $mediumModal);
                        $mediumModal.modal({
                            show : true,
                            backdrop: 'static',
                        });
                    });
                })
                break;
            case 'reset' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Reset Password");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Delete User");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }

    obj.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };
});
