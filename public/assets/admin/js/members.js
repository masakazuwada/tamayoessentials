$( function ($) {
    var obj = {};

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');

        loadModal();
    });

    $(document).on('click', '#confirmation [type="submit"]', function (e) {
        e.preventDefault();
        switch (obj.method) {
            case 'delete' :
                obj.remove();
                break;            
            case 'reset' :
                obj.resetPassword();
                break;
        }
    });

    obj.members = $('#members').DataTable({
        'processing': true,
        'responsive': true,
        'order': [[0, 'DESC']],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'ajax': {
            "url": APP_URL + '/administration/member/get'
        },
        'columns': [
            { 'data': 'member_id', 'sClass': 'text-center' },
            { 
                'data': 'image',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    return '<a href="'+ APP_URL + data +'" target="_blank"> \
                        <img src="'+ APP_URL + data +'" height="20px" width="20px" title="See full image"/></a>';
                }
            },
            { 'data': 'email', 'sClass': 'text-center' },
            { 'data': 'full_name', 'sClass': 'text-center' },
            {
                'data': 'sponsor',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    var html = '<div><b>'+ data +'</b></div>';
                    if (data) {
                        html += '<div>'+ row.sponsor_full_name +'</div>';
                    }
                    return html;
                }
            },
            { 'data': 'contact_no', 'sClass': 'none' },
            { 'data': 'address', 'sClass': 'none' },
            { 
                'data': 'status',
                'sClass': 'text-center',
                'render': function (data, type, row) {
                    if (data) {
                        return 'Active';
                    }
                    return 'Inactive';
                }
            },
            { 'data': 'date_registered', 'sClass': 'text-center' },
            { 'data': 'actions', 'sClass': 'text-center' },
        ],
        'columnDefs': [
            {
                'data': 'actions',
                'targets': -1,
                'sortable': false,
                'render' : function (data, type, row) {
                    var html = '<a href="javacript:void(0)" data-bind="confirmation" data-method="reset" data-id="'+ row.member_id +'" class="btn btn-default btn-xs" title="Reset Password"> \
                                    <i class="fa fa-refresh"></i> Reset \
                                </a> \
                                <a href="javacript:void(0)" data-bind="member_add_edit" data-method="update" data-id="'+ row.member_id +'" class="btn btn-info btn-xs" title="Edit"> \
                                    <i class="fa fa-edit"></i> Edit \
                                </a> \
                                <a href="javacript:void(1)" data-bind="confirmation" data-method="delete" data-id="'+ row.member_id +'" class="btn btn-danger btn-xs" title="Delete"> \
                                    <i class="fa fa-trash-o"></i> Delete \
                                </a>';
                return html;
                }
            }
        ],
        'language': {
            "processing": '<div class="processing-wrapper"> \
                                <div><i class="fa fa-spinner fa-spin"></i> Fetching ... Please wait...</div> \
                            </div>',
            'emptyTable': 'No data available in the database.',
            'zeroRecords': 'No data found.',
            "infoFiltered": ""
        }
    });

    obj.save = function ($form, $modal, msg) {
        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/administration/member/save',
            type : 'post',
            headers: {
               'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    obj.members.ajax.reload();
                    toastr.success('Successfully '+ msg +'!', "Members");
                }
                else {
                    toastr.error('Error', "Members");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.resetPassword = function () {
        var $yBtn = $('#yconfirmation'),
        $modal = obj.modal;
        return $.ajax({
            url : APP_URL + '/administration/member/reset-password',
            type : 'get',
            dataType : 'json',
            data : { member_id: obj.id },
            success: function (result) {
                if(result.status) {
                    obj.members.ajax.reload();
                    toastr.success('Password has been reset successfully.', "Members");
                }
                else {
                    toastr.error('Error', "Members");
                }
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getMembersForDropdown = function($modal)
    {
        var $members = $('#sponsor');

        return $.ajax({
            url : APP_URL + '/administration/member/get-for-dropdown',
            type : 'get',
            dataType : 'json',
            beforeSend: function() {
                var $loader = $('<option />');
                $loader.attr('value', '').text('Fetching...').prop('selected', true);
                $members.append($loader);
            },
            success: function (result) {
                if (result instanceof Array) {
                    var $select = $('<option />');
                    $select.attr('value', '').text('N/A');

                    $members.empty();
                    $members.append($select);

                    $.each(result, function(index, value) {
                        var $option = $( '<option />' );
                        $option.attr('value', result[index].member_id).text(result[index].member_id + ' - ' +result[index].full_name);
                        $members.append( $option );
                    });
                }
            },
            error : function (xhr, status) {
                toastr.error("Error, please try again!", "Sponsor");
            },
        });
    }

    function getDetailsById($modal, id)
    {
        var $form = $modal.find('form');
        var countryId, regionId;

        return $.ajax({
            url : APP_URL + '/administration/member/get-details',
            type : 'get',
            dataType : 'json',
            data : { member_id: id },
            success: function (result) {
                $.each( result, function (index, value) {
                    switch (index) {
                        case 'image' :
                            $form.find('#image').val(value);
                            $form.find('[id=photo_preview]').prop('src', APP_URL + value);
                            break;
                        case 'country_id' :
                            countryId = value;
                            getCountries($modal.find('[name=' + index + ']'), value);
                            break;
                        case 'region_id' :
                            regionId = value;
                            getRegions($modal.find('[name=' + index + ']'), countryId, value);
                            break;
                        case 'city_id' :
                            getCities($modal.find('[name=' + index + ']'), regionId, value);
                            break;
                        default :
                            $form.find('[name=' + index + ']').val(value);
                            break;
                    }
                });
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.remove = function () {
        var $yBtn = $('#yconfirmation'),
            $modal = obj.modal;
        $.ajax({
            url : APP_URL + '/administration/member/remove',
            type : 'get',
            dataType : 'json',
            data : { member_id : obj.id },
            beforeSend : function () {
                $yBtn.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Removed!', "Members");
                }
                else {
                    toastr.error('Error', "Members");
                }
                obj.members.ajax.reload();
                $modal.modal('hide');
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function (ajaxURL, $modal) {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function () {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function (response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();

                $modal.find('.modal-content').html(content);
            },
            error: function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateCreate($form, $modal) {
        var msg = 'Added';
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email : {
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability',
                        type : 'get',
                        data : { table: 'te_members', key_name : 'email' },
                    },
                    required: true,
                    email : true,
                },
                password : {
                    required: true,
                },
                date_registered : {
                    required: true
                },
                status : {
                    required: true
                },
                country_id : {
                    required: true
                },
                region_id : {
                    required: true
                },
                city_id : {
                    required: true
                },
                line1 : {
                    required: true
                }
            },
            messages : {
                email : {
                    remote : "Please choose different email."
                },
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function validateUpdate($form, $modal) {
        var msg = 'Updated';
        $form.validate({
            rules: {
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email : {
                    required: true,
                    email : true,
                },
                date_registered : {
                    required: true
                },
                status : {
                    required: true
                },
                country_id : {
                    required: true
                },
                region_id : {
                    required: true
                },
                city_id : {
                    required: true
                },
                line1 : {
                    required: true
                }
            },
            submitHandler: function () {
                obj.save($form, $modal, msg);
            },
        });
    }

    function showPreview($form)
    {
        $("#photo").on('change', function() {
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $($form.find('img')).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Members");
            }
        });
    }

    function loadModal() {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/admin/' + obj.url;

        var $confirmationModal = $('#confirmation'),
            confirmationURL = APP_URL + '/modal/app/' + obj.url;

        switch(obj.method) {
            case 'insert' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    obj.getMembersForDropdown($mediumModal).done(function() {
                        getCountries($mediumModal.find('[name=country_id]'));
                        $($mediumModal.find('[name=country_id]')).on( 'change', function() {
                            getRegions($mediumModal.find('[name=region_id]'), $(this).val());
                        });
                        $($mediumModal.find('[name=region_id]')).on( 'change', function() {
                            getCities($mediumModal.find('[name=city_id]'), $(this).val());
                        });
                        showPreview($mediumModal);
                        obj.toggleShowPassword({
                            field: '#password_input',
                            control: '#show_password'
                        });
                        validateCreate($mediumModal.find('form'), $mediumModal);
                        $mediumModal.modal({
                            show : true,
                            backdrop: 'static',
                        });
                    });
                });
            break;
            case 'update' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function () {
                    obj.getMembersForDropdown($mediumModal).done(function() {
                        getDetailsById($mediumModal, obj.id).done(function () {
                            $($mediumModal.find('[name=country_id]')).on( 'change', function() {
                                getRegions($mediumModal.find('[name=region_id]'), $(this).val());
                            });
                            $($mediumModal.find('[name=region_id]')).on( 'change', function() {
                                getCities($mediumModal.find('[name=city_id]'), $(this).val());
                            });
                            $('[name=password]').prop('placeholder', "Leave blank if you don't want to change");
                            // $('[name=email]').attr('readonly', 'true');
                            $('.sponsor').remove();
                            showPreview($mediumModal);
                            obj.toggleShowPassword({
                                field: '#password_input',
                                control: '#show_password'
                            });
                            validateUpdate($mediumModal.find('form'), $mediumModal);
                            $mediumModal.modal({
                                show : true,
                                backdrop: 'static',
                            });
                        });
                    });
                })
                break;
            case 'reset' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Reset Password");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
            case 'delete' :
                obj.getModalContent(confirmationURL, $confirmationModal).done( function () {
                    $('#title').text("Delete Member");
                    $confirmationModal.modal({
                        show : true,
                        backdrop: 'static',
                    });
                });
                break;
        }
    }

    obj.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };
});
