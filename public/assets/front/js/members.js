$(function($) {
    var obj = {};
    obj.loader = '<img src="'+ APP_URL +'/assets/app/images/loading.gif" class="loader" />';

    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');
        loadModal();
    });

    obj.getModalContent = function(ajaxURL, $modal)
    {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function() {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function(response) {
                var html = $($.parseHTML(response)),
                content = html.filter('.modal-content').html();
                $modal.find('.modal-content').html(content);
            },
            error: function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    obj.register = function ($form, $modal) {

        var $submit = $form.find('[type="submit"]');

        $.ajax({
            url : APP_URL + '/member/register',
            type : 'post',
            headers: {
                'X-CSRF-Token': $('[name="_token"]').val()
            },
            data : new FormData($form[0]),
            dataType : 'json',
            contentType : false,
            cache : false,
            processData : false,
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Successfully Registered! Please check your email for verification.', "Tamayo Essentials");
                }
                else {
                    toastr.error('Error, try again.', "Tamayo Essentials");
                }
                $modal.modal('hide');

                sendEmailVerification(result.email_data);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function sendEmailVerification(data)
    {
        $.ajax({
            url : APP_URL + '/member/send-email-verification',
            type : 'post',
            headers: {
                'X-CSRF-Token': $('[name="_token"]').val()
            },
            dataType : 'json',
            data : { email: data['email'], token : data['token'], name : data['name'] },
            success : function (result) {
                console.log(result.status);
            },
        });
    }

    function validateRegister($form, $modal) {
        $form.validate({
            rules: {
                code : {
                    required: true,
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability-by-expiration',
                        type : 'get',
                        data : { table: 'te_codes', key_name : 'code_id' },
                    },
                },
                email : {
                    required: true,
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability',
                        type : 'get',
                        data : { table: 'te_members', key_name : 'email' },
                    },
                    email : true
                },
                sponsor : {
                    remote : {
                        url : APP_URL + '/app/ajax-check-availability-with-field',
                        type : 'get',
                        data : { table: 'te_members', key_name : 'member_id', 'field': 'sponsor' },
                    },
                },
                password : {
                    required: true,
                },
                confirm_password : {
                    required: true,
                    equalTo: '#password'
                },
                first_name : {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                birthdate : {
                    required: true,
                },
                gender : {
                    required: true,
                },
                country : {
                    required: true,
                },
                region : {
                    required: true,
                },
                city : {
                    required: true,
                },
                address : {
                    required: true,
                },
            },
            messages : {
                code : {
                    remote : "Code not available."
                },
                sponsor : {
                    remote : "Sponsor not found."
                },
                email : {
                    remote : "Please choose different email."
                },
            },
            submitHandler: function () {
                obj.register($form, $modal);
            },
        });
    }

    function showPreview($form)
    {
        $("#photo").on('change', function() {
            if (typeof (FileReader) != "undefined") {
                var files = !!this.files ? this.files : [];
                if (/^image/.test( files[0].type)) {
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);
                    reader.onloadend = function(e) {
                        $($form.find('img')).attr('src', e.target.result);
                    }
                }
            } else {
                toastr.error('This browser does not support FileReader.', "Members");
            }
        });
    }

    function loadModal()
    {
        var $modal = $( '#large'),
        $mediumModal = $('#medium'),
        $smallModal = $('#small'),
        ajaxURL = APP_URL + '/modal/front/' + obj.url;
        switch (obj.method) {
            case 'register' :
            obj.getModalContent(ajaxURL, $mediumModal).done(function() {
                getCountries($mediumModal.find('[name=country_id]'));
                $($mediumModal.find('[name=country_id]')).on( 'change', function() {
                    getRegions($mediumModal.find('[name=region_id]'), $(this).val());
                });
                $($mediumModal.find('[name=region_id]')).on( 'change', function() {
                    getCities($mediumModal.find('[name=city_id]'), $(this).val());
                });
                showPreview($mediumModal);
                validateRegister($mediumModal.find('form'), $mediumModal);
                $mediumModal.modal({
                    show : true
                });
            });
            break;
        }
    }

    toastr.options = {
        "positionClass": "toast-top-right",
    }
});