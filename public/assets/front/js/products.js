$(function($) {
    var obj = {};
    obj.loader = '<img src="'+ APP_URL +'/assets/app/images/loading.gif" class="loader" />';
    
    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');
        obj.image = $(this).data('image');
        loadModal();
    });

    function getProductDetails($modal)
    {
        return $.ajax({
            url : APP_URL + '/administration/transactions/product/get-details',
            type : 'get',
            dataType : 'json',
            data : { product_id : obj.id },
            complete: function(){
                $("#loader").hide();
            },
            success: function(result){
                $.each( result, function(index, value) {
                    switch (index) {
                        case 'image': $modal.find('#' + index).append('<img alt="Product Image" style="height: 300px; width: 100%" src="'+value+'" class="img-responsive"/>'); break;
                        case 'description': $modal.find('#' + index).html(nl2br(value)); break;
                        default: $modal.find('#' + index).text(value);
                    }
                });
            },
            error : function(xhr, status){
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function(ajaxURL, $modal)
    {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function() {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function(response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();
                $modal.find('.modal-content').html(content);
            },
            error: function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function loadModal()
    {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/front/' + obj.url;

        switch (obj.method) {
            case 'view' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function() {
                    getProductDetails($mediumModal);
                    $mediumModal.modal({
                        show : true
                    });
                });
                break;
            case 'view_image' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function() {
                    $("#loader").hide();
                    // $mediumModal.find('#image').append('<img alt="Price List" style="height: 300px" src="'+obj.image+'" class="img-responsive"/>');
                    $mediumModal.modal({
                        show : true
                    });
                });
                break;
        }
    }
});