$(function($) {
    var obj = {};
    obj.loader = '<img src="'+ APP_URL +'/assets/app/images/loading.gif" class="loader" />';
    
    $(document).on( 'click', '[data-bind]', function (e) {
        e.preventDefault();

        obj.url = $(this).data('bind');
        obj.id = $(this).data('id');
        obj.method = $(this).data('method');
        loadModal();
    });

    function getAchievementDetails($modal)
    {
        return $.ajax({
            url : APP_URL + '/administration/site-pages/achievements/get-details',
            type : 'get',
            dataType : 'json',
            data : { achievement_id : obj.id },
            complete: function(){
                $("#loader").hide();
            },
            success: function(result){
                $.each( result, function(index, value) {
                    switch (index) {
                        case 'image': $modal.find('#' + index).append('<img alt="Achievement Image" style="height: 300px" src="'+value+'" class="img-responsive"/>'); break;
                        case 'description': $modal.find('#' + index).html(nl2br(value)); break;
                        default: $modal.find('#' + index).text(value);
                    }
                });
            },
            error : function(xhr, status){
                alert(xhr.responseText);
            }
        });
    }

    obj.getModalContent = function(ajaxURL, $modal)
    {
        obj.modal = $modal;
        return $.ajax({
            url : ajaxURL,
            type : 'get',
            dataType : 'html',
            beforeSend: function() {
                $modal.find('.modal-content').html(obj.loader);
            },
            success: function(response) {
                var html = $($.parseHTML(response)),
                    content = html.filter('.modal-content').html();
                $modal.find('.modal-content').html(content);
            },
            error: function(xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function loadModal()
    {
        var $modal = $( '#large'),
            $mediumModal = $('#medium'),
            $smallModal = $('#small'),
            ajaxURL = APP_URL + '/modal/front/' + obj.url;

        switch (obj.method) {
            case 'view' :
                obj.getModalContent(ajaxURL, $mediumModal).done(function() {
                    getAchievementDetails($mediumModal);
                    $mediumModal.modal({
                        show : true
                    });
                });
                break;
        }
    }
});