$( function( $ ) {
    var obj = {};

    validateContact();

    obj.sendMessage = function ($form)
    {
        var $submit = $form.find('[type="submit"]');
        $.ajax({
            url : APP_URL + '/contacts/send-message',
            type : 'post',
            dataType : 'json',
            data: $form.serialize(),
            beforeSend : function () {
                $submit.text('Processing...').prop('disabled', true);
            },
            success : function (result) {
                if(result.status) {
                    toastr.success('Message Sent!', "Contact Page");
                }
                else {
                    toastr.error('Error', "Contact Page");
                }
                $submit.text('Save Changes').prop('disabled', false);
            },
            error : function (xhr, status) {
                alert(xhr.responseText);
            }
        });
    }

    function validateContact() {
        var $form = $('form');
        $form.validate({
            rules: {
                name : {
                    required: true,
                },
                email : {
                    required: true,
                    email: true
                },
                name : {
                    required: true,
                },
                subject : {
                    required: true,
                },
                message : {
                    required: true,
                },
            },
            submitHandler: function () {
                obj.sendMessage($form);
            },
        });
    }
});