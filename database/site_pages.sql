-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2017 at 03:33 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_pages`
--

CREATE TABLE `site_pages` (
  `page_id` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `segment` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `details` varchar(1000) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_pages`
--

INSERT INTO `site_pages` (`page_id`, `order_no`, `segment`, `name`, `summary`, `details`, `status`, `date_created`) VALUES
(1, 1, 'home', 'Home', NULL, NULL, 1, '2017-05-11 09:01:39'),
(2, 2, 'products', 'Products', NULL, NULL, 1, '2017-05-11 09:01:39'),
(3, 3, 'gallery', 'Gallery', NULL, NULL, 1, '2017-05-11 09:04:29'),
(4, 5, 'about-us', 'About us', NULL, NULL, 1, '2017-05-11 09:04:29'),
(5, 6, 'services', 'Services', NULL, NULL, 1, '2017-05-11 09:05:30'),
(6, 7, 'contacts', 'Contacts', NULL, NULL, 1, '2017-05-11 09:05:30'),
(7, 4, 'achievements', 'Achievements', NULL, NULL, 1, '2017-05-11 16:04:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_pages`
--
ALTER TABLE `site_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `url_segment` (`segment`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
