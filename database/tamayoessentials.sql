-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 15, 2017 at 02:06 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_sequences`
--

CREATE TABLE `app_sequences` (
  `sequence_id` int(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 = Global, 2 = Audit, 3 = Temporary, 4 = Constants, 1001 = Ibuild_control_no,  1002 = Ibuild_bill_no',
  `sequence` int(11) DEFAULT NULL COMMENT 'sequence number starting from 1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_sequences`
--

INSERT INTO `app_sequences` (`sequence_id`, `sequence`) VALUES
(1, 1077);

-- --------------------------------------------------------

--
-- Table structure for table `site_companies`
--

CREATE TABLE `site_companies` (
  `company_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `header` varchar(100) DEFAULT NULL,
  `mission` varchar(250) DEFAULT NULL,
  `vision` varchar(250) DEFAULT NULL,
  `core_values` varchar(250) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_companies`
--

INSERT INTO `site_companies` (`company_id`, `page_id`, `header`, `mission`, `vision`, `core_values`, `date_created`) VALUES
(1056, 1, 'Welcome', 'jkjkj', 'kkjkjk', 'bbbb', '2017-05-12 03:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE `site_contents` (
  `content_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`content_id`, `page_id`, `image`, `description`, `date_created`) VALUES
(1063, 1, 'http://localhost/tamayoessentials/public/assets/front/images/home/content/848bbfde5d4741408a0d3815b07702cf.png', '<h1>WHAT IS ALKALINE?</h1>\r\n\r\n<p></p><ul><li>&nbsp;<b>Alkaline water</b>&nbsp;is water that is slightly basic. It contains basic minerals such as calcium, magnesium, or bicarbonate. These compounds bind to hydrogen ions in solution, making the water more basic.<br></li><li>&nbsp;So, how do we get alkaline water if normal tap water is at about neutral pH? Natural alkaline water sources are usually springs or a reservoir of natural water underneath the earth\'s surface. The rock structures holding the water may have basic minerals, such as calcium or limestone that leak into the water, increasing the pH. Some companies, such as Poland Springs, have their own springs that they bottle water from. The Mammoth Hot Springs in Yellowstone National Park are examples of an alkaline springs.<br></li><li>&nbsp;Some companies add minerals to the water to make it more basic. Water can also be ionized, meaning it is broken up into hydrogen ions and hydroxide ions. The hydrogen ions are bound by minerals in the water, which makes the water more basic.<br></li></ul><p></p><h1></h1>', '2017-05-12 04:20:26'),
(1071, 5, 'http://localhost/tamayoessentials/public/assets/front/images/services/data/5654517ef45d2f5c810bc1d27d9d0702.png', 'asa', '2017-05-13 16:49:27');

-- --------------------------------------------------------

--
-- Table structure for table `site_pages`
--

CREATE TABLE `site_pages` (
  `page_id` int(11) NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  `segment` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_pages`
--

INSERT INTO `site_pages` (`page_id`, `order_no`, `segment`, `name`, `status`, `date_created`) VALUES
(1, 1, 'home', 'Home', 1, '2017-05-11 09:01:39'),
(2, 2, 'products', 'Products', 1, '2017-05-11 09:01:39'),
(3, 3, 'gallery', 'Gallery', 1, '2017-05-11 09:04:29'),
(4, 5, 'about-us', 'About us', 1, '2017-05-11 09:04:29'),
(5, 6, 'services', 'Services', 1, '2017-05-11 09:05:30'),
(6, 7, 'contact', 'Contact', 1, '2017-05-11 09:05:30'),
(7, 4, 'achievement', 'Achievements', 1, '2017-05-11 16:04:27');

-- --------------------------------------------------------

--
-- Table structure for table `site_sliders`
--

CREATE TABLE `site_sliders` (
  `slider_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `background_image` varchar(255) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_sliders`
--

INSERT INTO `site_sliders` (`slider_id`, `page_id`, `background_image`, `description`, `date_created`) VALUES
(1033, 1, 'http://localhost/tamayoessentials/public/assets/front/images/home/slider/c7ddc38b20c523747b4d7c3f19587136.png', NULL, '2017-05-11 19:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `site_slider_datas`
--

CREATE TABLE `site_slider_datas` (
  `slider_data_id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_slider_datas`
--

INSERT INTO `site_slider_datas` (`slider_data_id`, `slider_id`, `image`, `title`, `description`) VALUES
(1053, 1033, 'http://localhost/tamayoessentials/public/assets/front/images/home/slider/data/5cb4798fd5f21fb89a3a01e3b68d5cbf.png', 'Heading 1\r\nasasaff\r\nfffff', 'lskjdkjksjdfk'),
(1054, 1033, 'http://localhost/tamayoessentials/public/assets/front/images/home/slider/data/5cb4798fd5f21fb89a3a01e3b68d5cbf.png', 'Heading ss', 'asasa'),
(1055, 1033, 'http://localhost/tamayoessentials/public/assets/front/images/home/slider/data/5cb4798fd5f21fb89a3a01e3b68d5cbf.png', 'Heading 3\r\nasasasa\r\nuuuu', 'sdasa\r\nasasas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_sequences`
--
ALTER TABLE `app_sequences`
  ADD PRIMARY KEY (`sequence_id`);

--
-- Indexes for table `site_companies`
--
ALTER TABLE `site_companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `site_contents`
--
ALTER TABLE `site_contents`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `site_pages`
--
ALTER TABLE `site_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `url_segment` (`segment`);

--
-- Indexes for table `site_sliders`
--
ALTER TABLE `site_sliders`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `site_slider_datas`
--
ALTER TABLE `site_slider_datas`
  ADD PRIMARY KEY (`slider_data_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
