-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2017 at 06:19 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `te_galleries`
--

CREATE TABLE `te_galleries` (
  `gallery_id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `details` varchar(1000) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `te_galleries`
--

INSERT INTO `te_galleries` (`gallery_id`, `type`, `content`, `details`, `date_created`) VALUES
(1145, 1, 'http://localhost/tamayoessentials/public/assets/front/images/Gallerys/6baeffc792433b5b9ecef8337a5a4a21.png', 'asa', '2017-05-19 09:49:03'),
(1146, 2, 'https://www.youtube.com/watch?v=WhQ3COG18WE', 'asasaxxxxxx', '2017-05-19 09:49:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `te_galleries`
--
ALTER TABLE `te_galleries`
  ADD PRIMARY KEY (`gallery_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
