-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2017 at 02:41 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `te_achievements`
--

CREATE TABLE `te_achievements` (
  `achievement_id` int(11) NOT NULL,
  `achievement` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `details` varchar(2000) DEFAULT NULL,
  `date_achieve` date DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `te_achievements`
--

INSERT INTO `te_achievements` (`achievement_id`, `achievement`, `image`, `details`, `date_achieve`, `date_created`) VALUES
(1133, 'aaaasa', 'http://localhost/tamayoessentials/public/assets/front/images/achievements/a89d0c70ba081b915ef4fa7bbcbaa2c2.png', 'asasacc', '2017-12-31', '2017-05-18 06:01:17'),
(1136, 'sdfghjkl;,.', 'http://localhost/tamayoessentials/public/assets/front/images/achievements/a47b8a82dfe7341a138a03ff0d665a57.png', 'DFGHJKL', '2017-12-31', '2017-05-18 06:11:41'),
(1137, '121', 'http://localhost/tamayoessentials/public/assets/front/images/achievements/1f9647345f09b1a200fc4f41d80624d2.png', 'as', '2017-12-30', '2017-05-18 06:21:17'),
(1138, 'asas', 'http://localhost/tamayoessentials/public/assets/front/images/achievements/04a76d626c0e0e7da636ccbb0967ddd1.png', 'asa', '2017-12-31', '2017-05-18 06:21:24'),
(1139, 'asas', 'http://localhost/tamayoessentials/public/assets/front/images/achievements/f78bafa6e0d8557a0a3414168ad7332f.png', 'as', '2016-12-31', '2017-05-18 06:21:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `te_achievements`
--
ALTER TABLE `te_achievements`
  ADD PRIMARY KEY (`achievement_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
