-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 16, 2017 at 01:05 AM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_sliders`
--

CREATE TABLE `site_sliders` (
  `slider_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `background_image` varchar(255) DEFAULT NULL,
  `details` varchar(2000) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_sliders`
--

INSERT INTO `site_sliders` (`slider_id`, `page_id`, `background_image`, `details`, `date_created`) VALUES
(1033, 1, 'http://localhost/tamayoessentials/public/assets/front/images/home/slider/c7ddc38b20c523747b4d7c3f19587136.png', NULL, '2017-05-11 19:03:00'),
(1087, 4, NULL, 'Aenean nibh ante, lacinia non tincidunt nec, lobortis ut tellus. Sed in porta diam. Suspendisse potenti. Donec luctus ullamcorper nulla. Duis nec velit odio.Suspendisse potenti. Donec luctus ullamcorper nulla. Duis nec velit odio.<br><ul><li>Vestibulum auctor dapibus neque.<br></li><li>Ipsum<br></li><li>Lorey<br></li><li>Ipsey<br></li></ul><br>', '2017-05-15 16:12:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_sliders`
--
ALTER TABLE `site_sliders`
  ADD PRIMARY KEY (`slider_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
