-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 16, 2017 at 03:33 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamayoessentials`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE `site_contents` (
  `content_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`content_id`, `page_id`, `type`, `image`, `description`, `date_created`) VALUES
(1063, 1, NULL, 'http://localhost/tamayoessentials/public/assets/front/images/home/content/848bbfde5d4741408a0d3815b07702cf.png', '<h1>WHAT IS ALKALINE?</h1>\r\n\r\n<p></p><ul><li>&nbsp;<b>Alkaline water</b>&nbsp;is water that is slightly basic. It contains basic minerals such as calcium, magnesium, or bicarbonate. These compounds bind to hydrogen ions in solution, making the water more basic.<br></li><li>&nbsp;So, how do we get alkaline water if normal tap water is at about neutral pH? Natural alkaline water sources are usually springs or a reservoir of natural water underneath the earth\'s surface. The rock structures holding the water may have basic minerals, such as calcium or limestone that leak into the water, increasing the pH. Some companies, such as Poland Springs, have their own springs that they bottle water from. The Mammoth Hot Springs in Yellowstone National Park are examples of an alkaline springs.<br></li><li>&nbsp;Some companies add minerals to the water to make it more basic. Water can also be ionized, meaning it is broken up into hydrogen ions and hydroxide ions. The hydrogen ions are bound by minerals in the water, which makes the water more basic.<br></li></ul><p></p><h1></h1>', '2017-05-12 04:20:26'),
(1071, 5, NULL, 'http://localhost/tamayoessentials/public/assets/front/images/services/data/5654517ef45d2f5c810bc1d27d9d0702.png', 'asa', '2017-05-13 16:49:27'),
(1080, 5, NULL, NULL, 'xxxx', '2017-05-15 15:24:23'),
(1101, 6, '1', NULL, 'Revox Crossraid, 85/B Cross Street, <br>New York, USA&nbsp;<br>NA1 42SL﻿<br>', '2017-05-16 05:56:24'),
(1102, 6, '6', NULL, 'Monday - Friday 8am to 4pm <br>Saturday - 7am to 6pm&nbsp;<br>Sunday- Closed&nbsp;﻿<br>', '2017-05-16 05:56:24'),
(1103, 6, '5', NULL, '<p>+088-01234567890</p><p><i></i>+088-01234567890﻿</p>', '2017-05-16 05:56:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_contents`
--
ALTER TABLE `site_contents`
  ADD PRIMARY KEY (`content_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
